<?php

namespace editor;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Model
{
    use SoftDeletes;
    
     protected $table = 'category';
     
      protected $fillable = ['name','status'];
      
      
      public function mockup3d()
    {
        return $this->hasOne('editor\Mockup3d','id','category_id');
    }

}
