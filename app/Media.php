<?php

namespace editor;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Media extends Model implements StaplerableInterface{

  use EloquentTrait;
    protected $table = 'media';
    protected $fillable = ['file_name', 'file_location', 'media_type','image','user_id'];
    
    public function __construct(array $attributes = array()) {
    $this->hasAttachedFile('image', [
        'styles' => [
            'medium' => '300x300',
            'thumb' => '100x100'
        ],
//        'storage' => 's3',
        'url' => '/system/:attachment/:id_partition/:style/:filename', 
        
        
    ]);

    parent::__construct($attributes);
  }
  
    public function mockup3d() {
        return $this->hasOne('editor\Mockup3d', 'media_id', 'id');
    }

    public function patternimage() {
        return $this->hasOne('editor\PatternImage', 'media_id', 'id');
    }
    

}
