<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('index');
// });
Route::controllers([
   'password' => 'Auth\PasswordController',
]);

Route::get('/', 'Auth\AuthController@getLogin');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register' , 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

//Block for ACL just use ['auth','roles'] 
//Currently roles section not using anymore
$router->group(['middleware' => ['auth']], function() {
    //Users segment
    Route::resource('/user','UserController');
    Route::resource('/users','Admin\UsersController');

    //Roles segment
    Route::resource('/roles','Admin\RolesController');
    Route::get('/role','Admin\RolesController@roleArea');
    Route::post('/roles/assign','Admin\RolesController@assign');

    //ACL segment
    Route::get('/acl', 'Admin\AclController@update_resources');

    //ACL Permission segment
    Route::get('permissions', 'Admin\AclController@list_permissions');
    Route::get('permissions/{role_id}', 'Admin\AclController@list_permissions');
    Route::post('/permissions','Admin\AclController@add_permission');

    //Category segment
    Route::get('/category/trash','Admin\CategoryController@trashItems');
    Route::get('/category/restore/{c_id}','Admin\CategoryController@restoreItems');
    Route::get('/category/delete/{c_id}','Admin\CategoryController@forceDelete');
    Route::resource('/category','Admin\CategoryController');

    //Mockups
    Route::resource('/mockup3d','Admin\Mockup3dController');
    
    //Template segment
    Route::resource('/template','Admin\TemplateController');
    Route::post('gettemplate','Admin\PatternImageController@getTemplateData');
    Route::get('/edit/{id}','EcoverController@getSizes');

    //Pattrens segment
    Route::get('tabdata','Admin\PatternImageController@loadTabdata');
    Route::resource('/pattern','Admin\PatternImageController');
    Route::get('/showsession','Admin\UsersController@showsession');

    //Route::get('/home', ['as' => '/users', 'uses' => 'Admin\UsersController@index']);
});

//Block for notmal auth
$router->group(['middleware' => ['auth']], function() {
    //Canvas User profile and crop segment
    Route::get('jcrop','Admin\UsersController@jcropview');
    Route::post('/users/jcrop','Admin\UsersController@jcroppost');
    
    //Editor segment
    Route::resource('/canvas','Admin\CanvasController');
    Route::resource('/cover','EcoverController');
    Route::get('/editor/{id}','EcoverController@getSizes');
    
    Route::post('/editor/save','EcoverController@saveCanvas');
    Route::post('/editor/clonesave','EcoverController@cloneCanvas');
    Route::get('/design','EcoverController@getCanvasSizes');
    Route::get('/getCanvasAjax','EcoverController@getCanvasAjax');
    Route::get('/getCategoryAjax','EcoverController@getCategoryAjax');
    Route::get('/home', ['as' => '/editor', 'uses' => 'EcoverController@getSizes']);  
    Route::post('/editor/upload_image/{id}','EcoverController@uploadUserMedia');
    Route::post('/editor/get_user_images/{id}','EcoverController@getUserImages');
    
    //Upload Media and Images segment
    Route::post('/media/uploads','MediaController@uploads');
    Route::post('/media','MediaController@index');
    Route::resource('/media','MediaController');
    Route::post('/media','MediaController@index');
    Route::post('/media/{id}','MediaController@destroy');
    
   
});

 //Export Canvas segment
Route::post('/exports','Admin\CanvasController@exportCanvas');
 

