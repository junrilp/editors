<?php

namespace editor\Http\Controllers\Admin;

use Illuminate\Http\Request;

use editor\Http\Requests;
use editor\Http\Requests\CreateMockup3dRequest;
use editor\Http\Controllers\Controller;
use editor\Category;
use editor\Media;
use editor\Canvas;
//use editor\Mockup;
use editor\Mockup3d;
use editor\Menu;
use Input;
use DB;
use Validator;
use Image;

class Mockup3dController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //$category = DB::table('category')->get();
       $mockup3d = Mockup3d::with('category')->paginate(config('app.pagination_limit'));
       return view('admin.mockup3d.index', compact('mockup3d')); 

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $category = DB::table('category')->whereNull('deleted_at')->get();
         return view('admin.mockup3d.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input=$request->all();
        if(isset($input['is_3d']) && $input['is_3d']=='yes'){
        $validator = Validator::make($request->all(), ['category_id' => 'required','title'=>'required','price'=>'required','width'=>'required','height'=>'required']);
        }else{
            $validator = Validator::make($request->all(), ['width'=>'required','height'=>'required']);
        }
        if ($validator->fails()) {
            return redirect('/mockup3d/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        
        $input['media_id']=$input['media-id'];
        $input['status']=1;
         $mockup3d = Mockup3d::create($input);
         \Session::flash('message', 'New 3d Mock up has been Created.!'); 
         \Session::flash('message-type', 'success'); 
        return redirect('/mockup3d');
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $mockup3d = Mockup3d::with(['category','media'])->findOrFail($id);
//         echo "<pre>";
//         print_r($mockup3d->category['name']);
//         exit;
         $category = DB::table('category')->get();
         return view('admin.mockup3d.edit', compact('mockup3d','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), ['category_id' => 'required','title'=>'required','price'=>'required','width'=>'required','height'=>'required']);
        if ($validator->fails()) {
            return redirect('/mockup3d/'.$id.'/edit')
                            ->withErrors($validator)
                            ->withInput();
        }
         $input = $request->all();
         $mockup3d = Mockup3d::findorFail($id);
         $mockup3d->update($input);
         \Session::flash('message', '3D Mock up has been updated !');
         \Session::flash('message-type', 'success');
        return redirect('/mockup3d');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        die($id);
        Mockup3d::destroy($id);
        Canvas::destroy($id);
        \Session::flash('message', '3d Mock up Deleted Sucessfully.!'); 
         \Session::flash('message-type', 'success'); 
        return redirect('/mockup3d');
    }
}
