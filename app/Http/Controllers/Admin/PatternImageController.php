<?php

namespace editor\Http\Controllers\Admin;

use Illuminate\Http\Request;
use editor\Http\Requests;
//use Illuminate\Support\Facades\Request;
use editor\Http\Requests\CreatePatternImageRequest;
use editor\Http\Controllers\Controller;
use editor\Category;
use editor\Media;
use Intervention\Image\ImageManagerStatic;
//use editor\Mockup;
use editor\Mockup3d;
use editor\Menu;
use editor\Canvas;
use editor\PatternImage;
use editor\User;
use editor\Template;
use Input;
use DB;
use Validator;
use Image;
use Session;
use Auth;

class PatternImageController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $base_url;
    public function __construct() {
        $this->base_url;
    }

    public function index() {
        $pattern_image = PatternImage::with('media')->paginate(config('app.pagination_limit'));
//          echo"<pre>";
//          print_r($pattern_image);
//          die;
        return view('admin.pattern_image.index', compact('pattern_image'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $pattern_image = DB::table('pattern')->get();
        return view('admin.pattern_image.create', compact('pattern_image'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePatternImageRequest $request) {
        $input = $request->all();
        
         $media = new Media();
        if (isset($input)) {
//            $image = $request->file('media_id');
//            $filename = time().'_'.$image->getClientOriginalName();
//            $path_pro = public_path('uploads/prolix/' . $filename);
//            //Image::make($image->getRealPath())->save($path_pro);
////            $media->file_name = $filename;
////             $media->file_location = 'uploads/prolix/' .time().'_'.$image->getClientOriginalName() ;
//             $media->save();
//             $input['media_id'] = $media->id;
            
            
            $response['status'] = 'success';
            $response['image_path'] = $media->image->url('thumb');
            $response['id'] = $media->id;
            $response['message'] = '<bold><u>'.$media->image_file_name.'</u></bold> has been uploaded successfully';            
        } else {
            $response['status'] = 'error';
            $response['image_path'] = $media->image->url('thumb');
            $response['id'] = $media->id;
            $response['message'] = '<bold>Error while uploading file: '.$media->image_file_name.'</bold>';  
        }
        echo json_encode($response);
        $pattern_image = PatternImage::create($input);
        \Session::flash('message', 'New Pattern OR Image has been Created.!');
        \Session::flash('message-type', 'success');
        return redirect('/pattern');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $pattern_image = PatternImage::with(['media'])->findOrFail($id);
        return view('admin.pattern_image.edit', compact('pattern_image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), ['media_id' => 'required', 'type' => 'required', 'title' => 'required']);
        if ($validator->fails()) {
            return redirect('/pattern/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }
        $input = $request->all();
        $pattern_image = PatternImage::findorFail($id);
        $pattern_image->update($input);
        \Session::flash('message', 'Pattern or Image has been updated !');
        \Session::flash('message-type', 'success');
        return redirect('/pattern');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        PatternImage::destroy($id);
        \Session::flash('message', 'Pattern or Image deleted successfully !');
        \Session::flash('message-type', 'success');
        return redirect('/pattern');
    }

    /**
     * Load all Pattern, Image, Template Data on frontend editor.
     *
     * @return \Illuminate\Http\Response
     */
    public function loadTabdata(Request $request) {
         $tabType = isset($_GET['tog']) ? $_GET['tog'] : 1;
        $offset = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $limit = 9;
        $response = array();
        
        /* tog=2 defines template
         * tog=3 define Text
         * tog=4 define pattern
         * tog=5 define uploads
         * tog=6 define images
         */

        switch ($tabType) {
            case '2':
                
                $width = $_GET['width'];
                $height = $_GET['height'];
                $id = $_GET['id'];
                
                
//                $response = DB::select("SELECT oe_canvas.* , oe_users.id as uid , oe_users.first_name , oe_users.last_name  
//                                        FROM oe_canvas 
//                                        LEFT JOIN oe_users
//                                        ON oe_canvas.user_id=oe_users.id
//                                        WHERE oe_canvas.width='$width' and oe_canvas.height='$height' and oe_canvas.user_id=1 ");
                if(Auth::user()->id == 1){
                    $response = Canvas::with('users')
                            ->where('id', '!=',$id)
                            ->where('width','=',$width)->where('height','=',$height)
                            ->where('status','=', 1)
                            ->get();
                }else{
                    $response = Canvas::with('users')
                        ->whereIn('user_id',[Auth::user()->id,1])
                        ->where('id', '!=',$id)
                        ->where('width','=',$width)->where('height','=',$height)
                        ->where('status','=', 1)
                        ->get();
                }
                
                
                foreach ($response as $key => $value) {
                    $can_frame_id = explode(',', $value->frame_id);
                    $frame_data[$key] = DB::table('frames')->where("id", $can_frame_id[0])->first();
                }
               if(!empty($frame_data)){
                foreach ($frame_data as $key => $value) {
                    $response[$key]->frame_image = $value->frame_image;
                }
               }
//                echo "<pre>";
//                print_r($response);
//                die;
                echo json_encode($response);
                die;
            case '3':
                //currently not using at all. text table also not exist. This is future case if extended
                $response = Template::with('text')->get();
                break;
            case '4':
//                $response = DB::select('SELECT oe_pattern.* , oe_media.*
//                                        FROM oe_pattern LEFT JOIN oe_media
//                                        ON oe_pattern.media_id=oe_media.id  
//                                        where oe_pattern.type="pattern" limit ' . $offset . ',' . $limit);

                $response = PatternImage::with('media')->where('type', '=', 'pattern')->offset($offset)->limit($limit)->get();
                break;
            case '5':
//                $response = Media::orderBy('id', 'desc')->get();
                break;
            case '6':
//                $response = DB::select('SELECT oe_pattern.* , oe_media.* 
//                                        FROM oe_pattern LEFT JOIN oe_media
//                                        ON oe_pattern.media_id=oe_media.id  
//                                        where oe_pattern.type="image" limit ' . $offset . ',' . $limit);
                $response = PatternImage::with('media')->where('type', '=', 'image')->offset($offset)->limit($limit)->get();
                break;
             case '7':
//                $response = DB::select('SELECT oe_pattern.* , oe_media.*
//                                        FROM oe_pattern LEFT JOIN oe_media
//                                        ON oe_pattern.media_id=oe_media.id  
//                                        where oe_pattern.type="pattern" limit ' . $offset . ',' . $limit);

                $response = PatternImage::with('media')->where('type', '=', 'pattern')->offset($offset)->limit($limit)->get();
                break;
            
        }
//        echo "<pre>";print_r($response);die();
        foreach ($response as $i => $details) {
            $response[$i]['original_src'] = url() . $details->media->image->url();
            $response[$i]['thumb_src'] = url()  . $details->media->image->url('thumb');
            $response[$i]['medium_src'] = url()  . $details->media->image->url('medium');
        }
        echo json_encode($response);
        die();
    }

    /**
     * Get Template Data By using template ID
     * @return \Illuminate\Http\Response
     */
    public function getTemplateData() {
        $id = $_POST['tid'];
        $response = DB::table('canvas')->where("id",'=',$id)->first();

        $can_frame_id = explode(',', $response->frame_id);
        if(!empty($can_frame_id)){
            foreach ($can_frame_id as $i=> $value) {
                $response->frames[$i]  = DB::table('frames')->where("id", $value)->first();
            }
        }

        if (!empty($response)) {
            echo $response= json_encode($response);
        }
        die;
    }

}
