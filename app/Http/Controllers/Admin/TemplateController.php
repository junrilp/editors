<?php

namespace editor\Http\Controllers\Admin;

use Illuminate\Http\Request;

use editor\Http\Requests;
use editor\Http\Controllers\Controller;
use editor\Category;
use editor\Media;
//use editor\Mockup3d;
use editor\Template;
use Input;
use DB;
use Validator;
use Image;
use Session;
use Auth;
use Carbon\Carbon;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $template = Template::with('users')->paginate(config('app.pagination_limit'));
        return view('admin.template.index', compact('template')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = DB::table('category')->get();
        return view('admin.template.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dimention = explode('__', $_POST['dimention']);
        $store = array(
            'user_id'=>$_POST['_m_user_id'],
            'title'=>$_POST['title'],
            'template_data'=>$_POST['canvas_data'],
            'img_data'=>$_POST['img_data_source'],
            'width'=>$dimention[0],
            'height'=>$dimention[1],
            'price'=>0,
            'status'=>'1',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        );
        $rid = DB::table('template')->insertGetId($store);
        
        $res = array(
           'status'=>'success',
           'id'  => $rid
        );
        
        echo json_encode($res);
        die;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Template::destroy($id);
        return redirect('/template');
    }
}
