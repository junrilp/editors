<?php

namespace editor\Http\Controllers\Admin;

use Illuminate\Http\Request;
use editor\Http\Requests\CreateCategoryRequest;
use editor\Http\Requests;
use editor\Http\Controllers\Controller;
use Auth;
use Route;
use editor\Category;
use DB;
use Input;
use Validator;

class CategoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $category = Category::paginate(config('app.pagination_limit'));
        return view('admin.category.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request) {
        $input = $request->all();
        $input['status'] = 1;
        $category = Category::create($input);
        \Session::flash('message', 'New Category has been Created.!');
        \Session::flash('message-type', 'success');
        return redirect('/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $category = Category::findOrFail($id);
        return view('admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), ['name' => 'required']);
        if ($validator->fails()) {
            return redirect('/category/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
        }

        $input = $request->all();
        $category = Category::findorFail($id);
        $category->update($input);
        \Session::flash('message', 'Category has been updated !');
        \Session::flash('message-type', 'success');
        return redirect('/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
       
        Category::destroy($id);
        \Session::flash('message', 'Category has been Moved to Trash Folder.!');
        \Session::flash('message-type', 'success');
        return redirect('/category');
    }

    /**
     * Indexing the Soft Deleted items in Trash Folder.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function trashItems() {

        $category = Category::onlyTrashed()->paginate(config('app.pagination_limit'));
        return view('admin.category.trash', compact('category'));
    }

    /**
     * Restore the items from trash folder to Main Category Area.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restoreItems($c_id = null) {

        $category = Category::withTrashed()->find($c_id)->restore();
        \Session::flash('message', 'Category has been Restored !');
        \Session::flash('message-type', 'success');
        return redirect('/category');
    }

    /**
     * Delete the Items Permanently from Trash Folder.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($c_id = null) {

        $category = Category::withTrashed()->find($c_id)->forceDelete();
        \Session::flash('message', 'Category has been Deleted Permanently.!');
        \Session::flash('message-type', 'success');
        return redirect('/category/trash');
    }

}
