<?php

namespace editor\Http\Controllers\Admin;

use Illuminate\Http\Request;

use editor\Http\Requests;
use editor\Http\Controllers\Controller;
use Auth;
use Route;
use DB;
use editor\Resource;
use editor\Permission;
use editor\Role;
use Input;
use Validator;
use Carbon\Carbon;


class AclController extends Controller
{
    private $guest_controllers = array("authcontroller", "passwordcontroller");
    
    /**
     * Update all actions in controller and save to thre database.
     *
     * 
     * @return Response
     */
    
    public function update_resources() {
        $arr = [];
        $new_entries = array();
        foreach (Route::getRoutes()->getRoutes() as $route) {
            $resource = $this->getResources($route);
            $controller = strtolower($resource['controller']);
            $action = strtolower($resource['action']);
            if (in_array($controller, $this->guest_controllers))
                continue;
            $resourceFlag = DB::table('resources')
                            ->where('controller', $controller)->where('action', $action)
                            ->where('deleted', 0)->first();
            if (!$resourceFlag) {
                $new_entries[] = DB::table('resources')->insertGetId(
                        array('controller' => $controller, 'action' => $action, 'deleted' => 0)
                );
            }
        }
        //echo count($new_entries) . " new resources added";
        return view ('resource', compact('new_entries'));
        exit;
    }

    /**
     * To get All controller and action from the App.
     *
     * 
     * @return Controller and Action
     */
   
    private function getResources($route) {
        $resources = $route->getAction();
        $currentAction = $resources['controller'];
        list($controller, $method) = explode('@', $currentAction);
        $controller = preg_replace('/.*\\\/', '', $controller);
        return ['controller' => $controller, 'action' => $method];
    }
    
    /**
     * Display the all controller and action in Permission area view.
     *
     * @param  int  $role_id
     * @return Response
     */

    public function list_permissions( $role_id = null  ) {
        $permissions = [];
        $type = "";
        if( $role_id ){
            $permissions = DB::table('permissions')->where('role_id', $role_id)->lists('status','resource_id');
            $type = $role_id;
//            print_r($permissions);
//            exit;
        }
        $resources = DB::table('resources')->get();
        $roles = DB::table('roles')->get();
        $resourceData = [];
        foreach ($resources as $resource) {
            $resource = (array) $resource;
            $resourceData[$resource['controller']][$resource['id']] = $resource['action'];
        }
        
       // echo "<pre>";
       // print_r($resourceData);
        //die();
        return view('admin.permissions', compact('roles', 'resourceData', 'permissions', 'type'));
    }
    
    /**
     * To Assign the Permissions to different Role types.
     *
     * @param  int  $id
     * @return Response
     */

    public function add_permission(Request $request) {
        
        $validator = Validator::make($request->all(), ['role_id' => 'required']);
        if ($validator->fails()) {
            return redirect('/permissions')
                            ->withErrors($validator)
                            ->withInput();
        }

        $input = $request->all();
        $role_id = $input['role_id'];
        $resource = $input['resource_id'];
        if ($resource) {
            foreach ($resource as $resource_id => $status) {
                $status = ( $status == "on" ) ? 1 :0;
                $id = DB::table('permissions')->where('resource_id', $resource_id)->where('role_id', $role_id)->pluck('id');
                $data = ['role_id' => $role_id, 'resource_id' => $resource_id, 'status' => $status,'created_at'=>Carbon::now(),'updated_at'=>Carbon::now()];
                if( $id ) DB::table('permissions')->where('id', $id)->update($data);
                else DB::table('permissions')->insertGetId($data);    
            }
        }
        \Session::flash('message', 'Your Permission has been successfully changed.!');
        \Session::flash('message-type', 'success');
        return redirect('/permissions');
    }

}