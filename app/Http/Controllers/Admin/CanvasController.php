<?php

namespace editor\Http\Controllers\Admin;

use Illuminate\Http\Request;
use editor\Http\Requests;
use editor\Http\Controllers\Controller;
use editor\Category;
use editor\Media;
//use editor\Mockup;
use editor\Mockup3d;
use editor\Menu;
use editor\Canvas;
use editor\Frames;
use Input;
use DB;
use ZipArchive;
use Validator;
use Image;
use Session;
use Auth;

class CanvasController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $role = Auth::user()->role_id;
        $userId = Auth::user()->id;
        if ($role == 1) {
            $canvas = Canvas::orderBy('id', 'desc')->with('users')->paginate(config('app.pagination_limit'));
        } else {
            $canvas = Canvas::orderBy('id', 'desc')->where('user_id', $userId)->with('users')->paginate(config('app.pagination_limit'));
        }
        return view('admin.canvas.index', compact('canvas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Responsee
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $role = Auth::user()->role_id;
        $user_id = Auth::user()->id;
        $canvas = Canvas::with(['users'])->findOrFail($id);
        Session::set('_canvas_data', json_encode($canvas));
        return Redirect('/editor');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $canvasFrameId = DB::table('canvas')->where('id', $id)->get();
        $can_frame_id = explode(',', $canvasFrameId[0]->frame_id);
        $frames = array();
        foreach ($can_frame_id as $i => $value) {
            $frames[$i] = DB::table('frames')->where("id", $value)->first();
            Frames::destroy($frames[$i]->id);
        }
        
        Canvas::destroy($id);
        \Session::flash('message', 'Canvas has been deleted Sucessfully.!');
        \Session::flash('message-type', 'success');
        return redirect('/canvas');
    }
    /**
     * Make the zip file for images while exporting.
     *
     * @param  $stack,$extension
     * 
     */
    function archiveImagesZIP($stack, $extension) {

        $userId = Auth::user()->id;
        $archiveName = "banners.zip";
        $archivePath = 'export/' . $userId . '/' . $archiveName;
        //Check if zip exist otherwise create one
        if (!file_exists($archivePath)) {
            $buildArchive = fopen($archivePath, "w");
            fclose($buildArchive);
        }

        $zip = new ZipArchive;
        $res = array();
        if ($zip->open($archivePath) === TRUE) {
            foreach ($stack as $i => $stackImg) {
                $fileName = ($i + 1) . '.' . $extension;
                if ($extension == 'png') {
                    $zip->addFile($stackImg, 'png/' . $fileName);
                } else if ($extension == 'jpg') {
                    $zip->addFile($stackImg, 'jpg/' . $fileName);
                }
            }
            $zip->close();
            $res['archive_path'] = $archivePath;
            $res['archive_status'] = true;
        } else {
            $res['archive_path'] = 'export/not-found';
            $res['archive_status'] = false;
        }
        return json_encode($res);
    }

    /**
     * Exporting the script to gif file.
     *
     * @param $frameDataSet
     * @return \Illuminate\Http\Response
     */
    public function exportToGIF($frameDataSet) {
        $userId = Auth::user()->id;
        $shellScript = "convert";
        $frameString = array();
        $response = array();
//        echo "<pre>";
//        print_r($frameDataSet['frames']);
//        die;
        try {

            $gifFileName = "banner.gif";
            foreach ($frameDataSet['frames'] as $index => $frame) {
//                print_r($index);
                $filename = strtotime(date("Y-m-d")) . '_' . str_replace(" ", "_", $index) . '.png';
                $path = 'export/base64img/' . $filename;
               
                //$data = 'data:image/gif;base64,R0lGODlhEAAOALMAAOazToeHh0tLS/7LZv/0jvb29t/f3//Ub//ge8WSLf/rhf/3kdbW1mxsbP//mf///yH5BAAAAAAALAAAAAAQAA4AAARe8L1Ekyky67QZ1hLnjM5UUde0ECwLJoExKcppV0aCcGCmTIHEIUEqjgaORCMxIC6e0CcguWw6aFjsVMkkIr7g77ZKPJjPZqIyd7sJAgVGoEGv2xsBxqNgYPj/gAwXEQA7';
                if (substr($frame, 0, 5) == 'data:') {
                     $source = fopen($frame, 'r');
                }else{
                    $image_parts = explode("/large/", $frame);
                    $type = pathinfo($image_parts[1], PATHINFO_EXTENSION);
                    $image = file_get_contents(url().$frame);
                    $baseuri = 'data:image/' . $type . ';base64,' . base64_encode($image);
                    $source = fopen($baseuri, 'r');
                }
                $destination = fopen($path, 'w');
                stream_copy_to_stream($source, $destination);
                fclose($source);
                fclose($destination);
                //Store delay and image name into an array
                $frameString[$index]['delay'] = $frameDataSet['frameTime'][$index] * 100;
                $frameString[$index]['img'] = $filename;
            }

            //Store script 
            foreach ($frameString as $index => $data) {
               $shellScript.= ' -delay ' . $data['delay'] . ' ' . 'export/base64img/' . $data['img']; 
            }
            // If not exist then create folder
            if (!file_exists('export/' . $userId)) {
                mkdir('export/' . $userId);
            }
            //Convert script to gif
            exec($shellScript . " export/" . $userId . "/" . $gifFileName);
//            exec('convert' .'export/base64img/1514419200_0.png'.' -crop miff:- | convert - -loop 0 '.$gifFileName);

            //Delete each frame generated earlier
//            foreach ($frameString as $index => $data) {
//                if (file_exists('export/base64img/' . $data['img'])) {
//                    $this->removeDirectory('export/base64img/' . $data['img']);
//                }
//            }
            $response['status'] = true;
            $response['path'] = url() . "/export/" . $userId . "/" . $gifFileName;
            $response['imgtype'] = 'gif';
        } catch (Exception $ex) {
            $response['status'] = false;
            $response['imgtype'] = 'gif';
            $response['message'] = "Something went wrong" . $ex;
        }
        return json_encode($response);
    }

    public function exportToOther($frameDataSet, $format) {
        $userId = Auth::user()->id;
        $response = array();
        $imgStore = array();
        
        //First remove the images which was stored
        if (is_dir('export/' . $userId)) {
            $this->removeDirectory('export/' . $userId);
        }
        // If not exist then create folder
        if (!file_exists('export/' . $userId)) {
            mkdir('export/' . $userId);
        }

        foreach ($frameDataSet['frames'] as $index => $frame) {
            $filename = 'banner' . '_' . str_replace(" ", "_", $index) . '.' . $format;
            $path = "export/" . $userId . '/' . $filename;
            // $data = $frameDataSet['frames'][0];
            if (substr($frame, 0, 5) == 'data:') {
                $source = fopen($frame, 'r');
            } else {
                $image_parts = explode("/large/", $frame);
                $type = pathinfo($image_parts[1], PATHINFO_EXTENSION);
                $image = file_get_contents(url() . $frame);
                $baseuri = 'data:image/' . $type . ';base64,' . base64_encode($image);
                $source = fopen($baseuri, 'r');
            }
            $destination = fopen($path, 'w');
            stream_copy_to_stream($source, $destination);
            fclose($source);
            fclose($destination);
            $frameString[$index]['img'] = $filename;
        }

        foreach ($frameString as $index => $data) {
            $imgStore[] = 'export/' . $userId . '/' . $data['img'];
        }
        //Call archiveImagesZIP to make a zip file of images
        $imagesResponse = $this->archiveImagesZIP($imgStore, $format);
        return $imagesResponse;
    }

    //Remove the directory
    public function removeDirectory($path) {

        $files = glob($path . '/*');
        foreach ($files as $file) {
            is_dir($file) ? $this->removeDirectory($file) : unlink($file);
        }
        rmdir($path);
        return;
    }

    public function exportCanvas() {

        if (!empty($_POST['frames'])) {
            $result = "";
            switch ($_POST['imagetype']) {
                case 'gif':
                    $result = $this->exportToGIF($_POST);
                    break;
                default :
                    $result = $this->exportToOther($_POST, $_POST['imagetype']);
                    break;
            }
        }
        echo $result;
        die();
    }

    //Remove the png images 
    public function deleteImages($imagesPath) {
        unlink($imagesPath);
        return;
    }

}
