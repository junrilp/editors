<?php

namespace editor\Http\Controllers\Admin;

use Illuminate\Http\Request;

use editor\Http\Requests;
use editor\Http\Requests\CreateRoleRequest;
use editor\Http\Controllers\Controller;
use Auth;
use editor\User;
use Route;
use editor\Report;
use editor\Role;
use editor\WorkType;
use editor\Attendence;
use DB;
use Input;
use Validator;




class RolesController extends Controller
{
   /**
     * Display a listing of the resource.
     * Index for assigning role area
     * @return Response
     */
    public function index() {
        $users_list = User::all();
        $roles = Role::all();
        return view('admin.roles.index', compact('users_list', 'roles'));
    }

    /**
     * Show the Role Index.
     *
     * @return Response
     */
    public function roleArea(){
    
        $roles = Role::all();
        return view('admin.roles.role', compact('roles')); 
        
   }
    
    
    
    
    
    public function create() {
         return view('admin.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(CreateRoleRequest $request) {
         $input=$request->all();
         $role = Role::create($input);
         \Session::flash('message', 'New Role has been Created.!'); 
         \Session::flash('message-type', 'success'); 
        return redirect('/role');
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
         if ($id) {
            $user =  User::find($id);
            return response()->json(['role_id' => $user->role_id]);
         }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        
            $roles = Role::findOrFail($id);
            return view('admin.roles.edit', compact('roles'));
        
            }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        
         $validator = Validator::make($request->all(), ['name' => 'required']);
        if ($validator->fails()) {
            return redirect('/roles/'.$id.'/edit')
                            ->withErrors($validator)
                            ->withInput();
        }
        
         $input = $request->all();
         $roles = Role::findorFail($id);
         $roles->update($input);
         \Session::flash('message', 'Role has been updated!');
         \Session::flash('message-type', 'success');
        return redirect('/role');
        
    }
    
     /**
     * Assign roles to users (admin,user etc).
     *
     * @param  Request  $request
     * @return Response
     */

    public function assign(Request $request) {
        
        $validator = Validator::make($request->all(),['user_id' => 'required','role_id' => 'required',]);
        if ($validator->fails()) {
            return redirect('/roles')
                        ->withErrors($validator)
                        ->withInput();
        }
        extract($request->all());
        DB::table('users')
                ->where('id', $user_id)
                ->update(['role_id' => $role_id]);
        
         \Session::flash('message', 'User Role has been changed by Super admin.!'); 
         \Session::flash('message-type', 'success'); 
        return redirect('/role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        Role::destroy($id);
        return redirect('/role');
    }

}
