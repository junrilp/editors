<?php

namespace editor\Http\Controllers\Admin;

use Illuminate\Http\Request;
use editor\Http\Requests\CreateCategoryRequest;
use editor\Http\Requests\EditUserRequest;
use editor\Http\Requests;
use editor\Http\Controllers\Controller;
use Auth;
use Route;
use editor\Category;
use editor\User;
use DB;
use Input;
use Validator;
use Session;

class UsersController extends Controller {

    public function __construct() {

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $users = User::paginate(config('app.pagination_limit'));
       
        return view('admin.users.index', compact('users'));
      
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $role = Auth::user()->role_id;
        $user_id = Auth::user()->id;
        if ($role != 1 && $user_id != $id) {
            \Session::flash('message', 'Not Authorized to edit other users profile');
            \Session::flash('message-type', 'danger');
            return redirect('/users/' . $user_id.'/edit');
        }
        $user_profile = User::findOrFail($id);
        return view('admin.users.edit', compact('user_profile', 'data'));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update($id, EditUserRequest $request) {
       
     $role = Auth::user()->role_id;
        $user_id = Auth::user()->id;
        if ($role !=1 && $user_id != $id) {
            \Session::flash('message', 'Not Authorized to edit other users profile');
            \Session::flash('message-type', 'danger');
            return redirect('/user/' . $user_id);
        }
        $input = $request->all();
        $users = User::findorFail($id);
        if (isset($input['password'])) {
            $input['password'] = bcrypt($input['password']);
        }
        if (isset($input['profileImage'])) {
            $imageName = time() . '_' . Auth::user()->id . '.' .
                    $request->file('profileImage')->getClientOriginalExtension();
            $ext = $request->file('profileImage')->getClientOriginalExtension();

            if ($request->file('profileImage')->move(base_path() . '/public/users_images/', $imageName)) {
                $input['profile_pic'] = $imageName;
                $users->update($input);

                return Redirect('/jcrop')->with('image', $imageName)->with('user_id', $user_id)->with('ext', $ext);
            }
        }
        $users->update($input);
        \Session::flash('message', 'User has been updated!');
        \Session::flash('message-type', 'success');
        return redirect('/users/' . $id.'/edit');
    }

       
        public function jcropview() {
        
         return view('jcrop')->with('image', 'users_images/'. Session::get('image'))->with('user_id', Session::get('user_id'))->with('ext', Session::get('ext'));
        
    }

    /**
     * Upload the Profile Pic in user profile.
     *
     * Switch case for upload both JPG,PNG format
     * 
     */
    public function jcroppost() {
        $quality = 90;
        $src = Input::get('image');
        $ext = Input::get('ext');
        $id = Input::get('user_id');
        $dest = ImageCreateTrueColor(Input::get('w'), Input::get('h'));

        switch ($ext) {
            case "jpg":
                $img = imagecreatefromjpeg($src);
                imagecopyresampled($dest, $img, 0, 0, Input::get('x'), Input::get('y'), Input::get('w'), Input::get('h'), Input::get('w'), Input::get('h'));
                imagejpeg($dest, $src, $quality);
                break;
            case "png":
                $img = imagecreatefrompng($src);
                imagecopyresampled($dest, $img, 0, 0, Input::get('x'), Input::get('y'), Input::get('w'), Input::get('h'), Input::get('w'), Input::get('h'));
                imagepng($dest, $src);
                break;
            case "gif":
                $img = imagecreatefromgif($src);
                imagecopyresampled($dest, $img, 0, 0, Input::get('x'), Input::get('y'), Input::get('w'), Input::get('h'), Input::get('w'), Input::get('h'));
                imagegif($dest, $src);
                break;
        }

        \Session::flash('message', 'User has been updated!');
        \Session::flash('message-type', 'success');
        return redirect('/users/' . $id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
         User::destroy($id);
        \Session::flash('message', 'User deleted successfully!');
        \Session::flash('message-type', 'success');
        return redirect('/users');
    }
    
    /**
     * Display all list of session
     * @return Response
     */
    public function showsession() {
       echo "<pre>";
       $data = Session::all();
       print_r($data);
       echo "</pre>";
       die;
       
    }
    
}


