<?php

namespace editor\Http\Controllers;

use Illuminate\Http\Request;
use editor\Http\Requests;
use editor\Http\Controllers\Controller;
use editor\Category;
use editor\Media;
use editor\UserUploadMedia;
use editor\Mockup;
use editor\Menu;
use editor\Canvas;
// use Request;
use Input;
use DB;
use Auth;
use Session;

class EcoverController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $details = DB::select('SELECT oe_mockup.*,oe_category.name,oe_media.file_name,oe_media.file_location,oe_media.media_type
                                FROM (oe_category
                                LEFT JOIN oe_mockup
                                ON oe_mockup.category_id=oe_category.id)
                                LEFT JOIN oe_media
                                ON oe_mockup.media_id=oe_media.id where oe_mockup.is_3d="yes" AND oe_mockup.status=1');


        $detailsData = array();
        foreach ($details as $detail) {
            $detail = (array) $detail;
            $detailsData[$detail['name']][] = $detail;
        }
//        return view('cover.ecover')->with('detailsData',$detailsData);
        return view('cover.ecover', compact('detailsData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function getSizes($tid) {
        $tempid = $tid;
        $dimensions = DB::select('SELECT oe_mockup.title,oe_mockup.height,oe_mockup.width from oe_mockup group by `height`,`width` ');
        $editor_tool = DB::table('users')->where("id", Auth::user()->id)->first();
        $canvasData = DB::table('canvas')->where('id', $tempid)->get();
        if(!empty($canvasData)){
            $can_frame_id = explode(',', $canvasData[0]->frame_id);
        }
        $frames = array();
        if (Auth::user()->id == 1) {
            $designFromTemplateData = DB::table('canvas')->where("id", $tempid)->first();
        } else {
            $designFromTemplateData = DB::table('canvas')->where("id", $tempid)->where("user_id", Auth::user()->id)->first();
        }
       
        if(!empty($can_frame_id)){
            foreach ($can_frame_id as $i => $value) {
                $designFromTemplateData->frames[$i] = DB::table('frames')->where("id", $value)->first();
            }
        }
//        echo "<pre>";
//        print_r($designFromTemplateData);
//        die;
        return view('index')->with(compact('dimensions', 'designFromTemplateData', 'tempid', 'editor_tool'));
    }

    public function getCanvasSizes() {
        $dimensions = DB::select('SELECT oe_mockup.title,oe_mockup.height,oe_mockup.width from oe_mockup group by `height`,`width` ');
        $layouts = DB::select("SELECT oe_canvas.* , oe_users.id as uid , oe_users.first_name , oe_users.last_name ,oe_users.editor_tool
                                        FROM oe_canvas 
                                        LEFT JOIN oe_users
                                        ON oe_canvas.user_id=oe_users.id
                                        ");

        $categories = DB::table('category')->lists('name', 'id');
        $templateSizeLayouts = array();
        $i = 0;
        $j = 0;
        foreach ($dimensions as $key => $value) {
            $templateSizeLayouts['dimensions'][$i] = $value;
            $i++;
        }
        foreach ($layouts as $key => $value) {
            $templateSizeLayouts['layouts'][$j] = $value;
            $j++;
        }
        return view('canvas-options-step', compact('templateSizeLayouts', 'categories'));
    }

    public function uploadUserMedia(Request $request) {

        $temp_id =  $request->id;
        $media = new Media();
        $response = array();
        $media->temp_id = $temp_id;
        $media->user_id = Auth::user()->id;
        $media = $media->fill($request->all());
        $media->save();

        $response['success'] = "Your file has been uploaded.";
        $response['url'] = $media->image->url();
        $response['status'] = true;

        echo json_encode($response);
    }

    public function getUserImages($id) {
        $temp_id =  $id;
        $user_id =  Auth::user()->id;
        if ($user_id == 1) {
            $response = Media::orderBy('id', 'desc')->get();
        }else{
            $response = Media::orderBy('id', 'desc')->where('user_id',$user_id)->get();
        }
        
        foreach ($response as $i => $details) {
            $response[$i]['original_src'] = url() . $details->image->url();
            $response[$i]['thumb_src'] = url() . $details->image->url('thumb');
            $response[$i]['medium_src'] = url() . $details->image->url('medium');
        }
        return json_encode($response);
    }
    public function base64ToImage($dataUrl,$dest,$index) {
        if (substr($dataUrl, 0, 5) == 'data:') {
            $uploadDir = ('banners/uploads/' . $dest . '/');
            $image_parts = explode(";base64,", $dataUrl);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file =  $uploadDir . time() . '_' . Auth::user()->id . '_' .$index. '.png';

            if (file_put_contents($file, $image_base64)) {
                return '/'.$file;
            } else {
                return "";
            }
        }else{
            return $dataUrl;
        }
    }

    public function cloneCanvas() {
        
        $data = array();
        $dimention = explode('_', $_POST['dimention']);
        //For design from template area 
        // If frame exists then insert frame for new user else default frame is added
        foreach ($_POST["canvas_data"] as $key => $value) {
            $data = array(
                'frame_data' => $_POST["canvas_data"][$key]["frame_data"],
                'frame_image' => $_POST["canvas_data"][$key]["frame_image"]
            );
            $frameid[$key] = DB::table('frames')->insertGetId($data);
            $frame_id = implode(',', $frameid);
        }

        // Insert the data into canvas table
        $data = array(
            'user_id' => Auth::user()->id,
            'frame_id' => $frame_id,
            'project_title' => @$_POST['title'],
//                'data' => @$_POST['canvas_data'],
            'category_id' => (isset($_POST['category_id']) ? $_POST['category_id'] : 1),
//                'img_data' => @$_POST['img_data_source'],
            'open' => 1,
            'status' => 0,
            'width' => $dimention[0],
            'height' => $dimention[1],
            'modified_by' => Auth::user()->id,
            'parent_id' => $_POST['canvas_id'],
            'created_at' => date("Y-m-d h:i:s"),
            'updated_at' => date("Y-m-d h:i:s")
        );
        $id = DB::table('canvas')->insertGetId($data);

        $res = array();
        if ($id) {
            $res['status'] = 'success';
            $res['message'] = 'Canvas Saved Successfully';
            $res['tmpid'] = $id;
        }

        echo json_encode($res);
    }

    public function saveCanvas() {
        //check if canvas id exists
        if (isset($_POST['_m_canvas_id']) && $_POST['_m_canvas_id'] > 0) {
            $id = $_POST['_m_canvas_id'];
            $frames = "";
            //check if frames are added or not ,if added then first delete the existing frames and add new frames
            if (!empty($_POST['framedata'])) {
                $canvasFrameId = DB::table('canvas')->where('id', $id)->get();
                $canvas_frame_id = $canvasFrameId[0]->frame_id;
                $can_frame_id = explode(',', $canvas_frame_id);
                //delete all the frames
                foreach ($can_frame_id as $value) {
                    DB::table('frames')->where('id', $value)->delete();
                }
                //Insert the new frames
                foreach ($_POST['framedata'] as $i => $value) {
                    $frame_data = array(
                        'frame_data' => $_POST['framedata'][$i],
                        'frame_image' => $this->base64ToImage($_POST['frames'][$i],'large',$i),
                    );
                    $frameid[$i] = DB::table('frames')->insertGetId($frame_data);
                    $frames = implode(',', $frameid);
                }
            }
            //update data into canvas table
            $data = array(
                'frame_id' => $frames,
                'project_title' => $_POST['title'],
                'updated_at' => date("Y-m-d h:i:s"),
                'status' => 1,
                'modified_by' => Auth::user()->id,
            );

            DB::table('canvas')->where('id', $id)->update($data);
        } else {
            $data = array();
            $dimention = explode('_', $_POST['dimention']);
            $data = array(
                'frame_data' => @$_POST['canvas_data'],
                'frame_image' => $this->base64ToImage($_POST['img_data_source'],'large',0),
            );
            $frame_id = DB::table('frames')->insertGetId($data);
            // Insert the data into canvas table
            $data = array(
                'user_id' => Auth::user()->id,
                'frame_id' => $frame_id,
                'project_title' => @$_POST['title'],
                'category_id' => (isset($_POST['category_id']) ? $_POST['category_id'] : 1),
                'open' => 1,
                'status' => 0,
                'width' => $dimention[0],
                'height' => $dimention[1],
                'modified_by' => Auth::user()->id,
                'created_at' => date("Y-m-d h:i:s"),
                'updated_at' => date("Y-m-d h:i:s")
            );
            $id = DB::table('canvas')->insertGetId($data);
        }
        $res = array();
        if ($id) {
            $res['status'] = 'success';
            $res['message'] = 'Canvas Saved Successfully';
            $res['tmpid'] = $id;
        }

        echo json_encode($res);
    }

    /**
     * get data according to category of canvas.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getCategoryAjax(Request $request) {
        $res = array();
        $canvas_params = $request->get('canvas_params');
        $cat_id = json_decode($canvas_params);
        $frame_data = array();
        if (Auth::user()->id == 1) {
            $layouts = DB::select("SELECT oe_canvas.* , oe_users.id as uid , oe_users.first_name , oe_users.last_name  
                                        FROM oe_canvas 
                                        LEFT JOIN oe_users
                                        ON oe_canvas.user_id=oe_users.id WHERE oe_canvas.category_id = " . $cat_id . "
                                        ");
        } else {
            $layouts = DB::select("SELECT oe_canvas.* , oe_users.id as uid , oe_users.first_name , oe_users.last_name  
                                        FROM oe_canvas 
                                        LEFT JOIN oe_users
                                        ON oe_canvas.user_id=oe_users.id
                                        WHERE oe_canvas.category_id=" . $cat_id . " AND
                                        (oe_canvas.user_id= " . Auth::user()->id . " OR oe_canvas.user_id=1)  
                                        ");
        }
        // get the frames fronm the canvas table
        foreach ($layouts as $key => $value) {
            $can_frame_id = explode(',', $value->frame_id);
            foreach ($can_frame_id as $i => $v) {
                $frame_data[$key][$i] = DB::table('frames')->where("id", $v)->first();
            }
        }
        $templateSizeLayouts = array();
        $i = 0;
        $j = 0;

        foreach ($layouts as $key => $value) {
            $templateSizeLayouts['layouts'][$j] = $value;
            $j++;
        }
        //Insert into into templatesizelayout array as a frame image key ,This frame image is used as a template thumbnail
        foreach ($frame_data as $key => $value) {
            foreach ($frame_data[$key] as $i => $v) {
                $templateSizeLayouts['layouts'][$key]->frame_image[$i] = $v->frame_image;
            }
        }
       
        if (count($layouts) > 0) {
            $res['success'] = true;
            $res['data'] = $templateSizeLayouts;
        } else {
            $res['success'] = false;
            $res['msg'] = 'No Category Data Found..!';
        }

        return json_encode($res);
    }

    /**
     * Get data according to the canvas sizes.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getCanvasAjax(Request $request) {

        $res = array();
        $canvas_params = $request->get('canvas_params');
        $canvas_params = json_decode($canvas_params);
        $canvas_params = explode("|", $canvas_params);
        $frame_id = array();
        $frame_data = array();
        if (Auth::user()->id == 1) {
            $layouts = DB::select("SELECT oe_canvas.* , oe_users.id as uid , oe_users.first_name , oe_users.last_name  
                                        FROM oe_canvas 
                                        LEFT JOIN oe_users
                                        ON oe_canvas.user_id=oe_users.id WHERE oe_canvas.width = " . $canvas_params[0] . " AND oe_canvas.height = " . $canvas_params[1] . "   
                                        ");
        } else {
            $layouts = DB::select("SELECT oe_canvas.* , oe_users.id as uid , oe_users.first_name , oe_users.last_name  
                                        FROM oe_canvas 
                                        LEFT JOIN oe_users
                                        ON oe_canvas.user_id=oe_users.id WHERE oe_canvas.width = " . $canvas_params[0] . " AND oe_canvas.height = " . $canvas_params[1] . " 
                                        AND (oe_canvas.user_id= " . Auth::user()->id . " OR oe_canvas.user_id=1)     
                                        ");
        }
        // get the frames fronm the canvas table
        foreach ($layouts as $key => $value) {
            $can_frame_id = explode(',', $value->frame_id);
            foreach ($can_frame_id as $i => $v) {
                $frame_data[$key][$i] = DB::table('frames')->where("id", $v)->first();
            }
        }

        $templateSizeLayouts = array();
        $frames = array();

        $i = 0;
        $j = 0;

        foreach ($layouts as $key => $value) {
            $templateSizeLayouts['layouts'][$j] = $value;
            $j++;
        }
        //Insert into into templatesizelayout array as a frame image key ,This frame image is used as a template thumbnail
        foreach ($frame_data as $key => $value) {
            foreach ($frame_data[$key] as $i => $v) {
                $templateSizeLayouts['layouts'][$key]->frame_image[$i] = $v->frame_image;
            }
        }

        if (count($layouts) > 0) {
            $res['success'] = true;
            $res['data'] = $templateSizeLayouts;
        } else {
            $res['success'] = false;
            $res['msg'] = 'No Canvas layouts Found..!';
        }

        return json_encode($res);
    }

}
