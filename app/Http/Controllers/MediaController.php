<?php

namespace editor\Http\Controllers;

use Illuminate\Http\Request;
//require 'vendor/autoload.php';
use editor\Http\Requests;
use editor\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic;
use editor\Category;
use editor\Media;
use editor\Mockup3d;
use editor\PatternImage;
use editor\Menu;
use Input;
use DB;
use Validator;
use Image;

class MediaController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $limitFrom = $_POST['limit'];
        $limitTo = 10;
        $mediaSql = Media::orderBy('id', 'desc')->skip($limitFrom)->take($limitTo)->toSql();
        $media =  Media::orderBy('id', 'desc')->skip($limitFrom)->take($limitTo)->get();
        $totalMedia =  Media::orderBy('id', 'desc')->get();
        $response = array();
        $length = 0;
        foreach ($media as $i => $details) {
            $response[$i]['src'] = url() . $details->image->url('thumb');
            $response[$i]['id'] = $details->id;
            $length = $i+1;
        }
        $response['limit'] = ($limitFrom + $limitTo);
        $response['count'] = ($length);
        $response['query'] = $mediaSql;
        $response['totalMedia'] = count($totalMedia);
        //return view('media.index', compact(['media']));
        echo json_encode($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    

    public function uploads(Request $request) {
//        echo "<pre>";
//        print_r($request->all());
//        echo "</pre>";
//        die;

        $input = $request->all();
        $media = new Media();
        $response = array();
        if (isset($input)) {
            $image = $request->file('image');
            $media = $media->fill($request->all());
            $media->save();

//            $filename = time().'_'.$image->getClientOriginalName();
//            $filename_thumb = time().'_'.$image->getClientOriginalName();
//            $filename_prolix = time().'_'.$image->getClientOriginalName();
//            $filename_thumb_200 = time().'_'.$image->getClientOriginalName();
//            $path_pro = public_path('uploads/prolix/' . $filename_thumb);
//            $path_thumb = public_path('uploads/thumbnails/' . $filename_prolix);
//            $path_thumb_200 = public_path('uploads/thumbnails_200/' . $filename_thumb_200);
//            $image_path = url() . '/uploads/thumbnails_200/' .time().'_'.$image->getClientOriginalName() ;
//            Image::make($image->getRealPath())->save($path_pro);
//            Image::make($image->getRealPath())->fit(200, 200)->save($path_thumb_200);
//            $thumbW = 200;
//            $thumbH = 200;
//            switch($input['mediaSource']){
//                case 'pattern':
//                    $thumbW = 145;
//                    $thumbH = 110;
//                    break;
//                case 'mockup3d':
//                    $thumbW = 200;
//                    $thumbH = 200;
//                    break;
//            }
//            Image::make($image->getRealPath())->fit($thumbW, $thumbH)->save($path_thumb);
//            $input['file_name'] = $filename;
//            $input['file_location'] = 'uploads/thumbnails_200/' .time().'_'.$image->getClientOriginalName() ;
//            $media = Media::create(Input::all());
            $response['status'] = 'success';
            $response['image_path'] = $media->image->url('thumb');
            $response['id'] = $media->id;
            $response['message'] = '<bold><u>'.$media->image_file_name.'</u></bold> has been uploaded successfully';            
        } else {
            $response['status'] = 'error';
            $response['image_path'] = $media->image->url('thumb');
            $response['id'] = $media->id;
            $response['message'] = '<bold>Error while uploading file: '.$media->image_file_name.'</bold>';  
        }
        echo json_encode($response);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = null) {

        $response = array();
        if ($id != '') {
            $media = Media::find($id);
            Media::destroy($id);
            PatternImage::where('media_id', '=', $id)->delete();
//            $patternImage = PatternImage::where('media_id','=',$id);
//            echo"<pre>";
//            print_r($patternImage);
//            die;
//            PatternImage::destroy($id);
            $mediaName = $media->file_name;
            $mediaFileLocation = $media->file_location;
            //  Delete file name from folder
            if (file_exists($mediaFileLocation)) {
                unlink($mediaFileLocation);
            }

            $response['id'] = $id;
            $response['success'] = "Media has been deleted successfully";
        } else {
            $response['id'] = $id;
            $response['error'] = "Error While Deleting Media";
        }

        echo json_encode($response);
    }

}
