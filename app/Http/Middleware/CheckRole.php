<?php

namespace editor\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Route;
use DB;
use editor\Permission;
use editor\Resource;
class CheckRole
{
   protected $auth;

    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $role_id = $this->auth->user()->role_id;
       if($role_id == 1) return $next($request);
        //if(1) return $next($request);
        $resources = $this->getRequestedResources($request->route());
        $authorized = $this->checkPermission(['resources' => $resources, 'role_id' => $role_id]);
        if( $authorized ){
            return $next($request);
        } else {
//            return response([
//                'error' => [
//                    'code' => 'INSUFFICIENT',
//                    'description' => 'You are not authorized to access this resource.'
//                ]
//           ], 401);
            
            return view('access');
        }
    }
    
    private function checkPermission( $arr = array() ){
        $controller = strtolower($arr['resources']['controller']);
        $action = strtolower($arr['resources']['action']);
        $role_id = $arr['role_id'];
        $id = DB::table('resources')
                ->where('controller','=', $controller)
                ->where('action','=',$action)
                ->pluck('id');
        if( $id ){
            $status = DB::table('permissions')
                ->where('role_id','=', $role_id)
                ->where('resource_id','=', $id)
                ->pluck('status');
            return $status;
        } else {
            return false;
        }
    }
    private function getRequestedResources($route) {
        $resources = $route->getAction();
        $currentAction = $resources['controller'];
        list($controller, $method) = explode('@', $currentAction);
        $controller = preg_replace('/.*\\\/', '', $controller);
        return ['controller' => $controller, 'action' => $method];
    }

}
