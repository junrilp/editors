<?php

namespace editor\Http\Requests;

use editor\Http\Requests\Request;
use Auth;

class EditUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
   public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user=Auth::user()->id;
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,id,'.$user,
            'profileImage'=>'image|mimes:jpeg,png,gif|between:10,3000'          
          
        ];
    }
}
