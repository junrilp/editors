<?php

namespace editor\Http\Requests;

use editor\Http\Requests\Request;

class CreatePatternImageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'media_id'=>'required',
//            'color'=>'required',
            'type'=>'required',
//          'price'=>'required',
            'title'=>'required',
            
        ];
    }
}
