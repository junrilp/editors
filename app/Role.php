<?php

namespace editor;

use Illuminate\Database\Eloquent\Model;



class Role extends Model {

    protected $table = 'roles';
    protected $fillable = ['name'];

    public function users() {
        return $this->hasMany('editor\User', 'role_id', 'id');
    }

    public function permission() {
        return $this->hasMany('editor\Permission', 'resource_id', 'id');
    }

}
