<?php

namespace editor;

use Illuminate\Database\Eloquent\Model;


class PatternImage extends Model
{
    
    protected $table = 'pattern';
    protected $fillable = ['id', 'media_id', 'color', 'type', 'title', 'price','status'];
    
    public function media() {
        return $this->belongsTo('editor\Media', 'media_id', 'id');
    }
}
