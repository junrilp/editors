<?php

namespace editor;

use Illuminate\Database\Eloquent\Model;



class Permission extends Model
{
 protected $table = 'permissions';
    
    
     public function resource()
	{
		return $this->belongsTo('editor\Resource', 'resource_id', 'id');
	}
         public function role()
	{
		return $this->belongsTo('editor\Role', 'role_id', 'id');
	}
        
    public function user(){
        return $this->belongsTo('editor\User');
    }
    
    
}
