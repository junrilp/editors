<?php

namespace editor;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model {

    protected $table = 'resources';

    public function permission() {
        return $this->hasOne('editor\Permission', 'resource_id', 'id');
    }

    public function getResource() {
        return DB::table('resources')->get();
    }

}
