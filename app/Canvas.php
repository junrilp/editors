<?php

namespace editor;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Canvas extends Model
{
    
    
    protected $table = 'canvas';
    
    protected $fillable = ['user_id', 'project_title', 'data', 'open', 'status'];
    
    public function users() {

        return $this->belongsTo('editor\User' , 'user_id', 'id');
    }
    public function frames() {

        return $this->hasOne('editor\Frames' , 'id', 'frame_id');
    }
}
