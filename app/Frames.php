<?php

namespace editor;

use Illuminate\Database\Eloquent\Model;

class Frames extends Model
{
     protected $table = 'frames';
    
    protected $fillable = ['id', 'frame_data', 'frame_image'];
    
    
}
