<?php

namespace editor;

use Illuminate\Database\Eloquent\Model;



class Mockup3d extends Model {

    protected $table = 'mockup';
    protected $fillable = ['category_id', 'media_id', 'title', 'is_3d', 'price', 'height', 'width', 'status'];

    public function category() {

        return $this->belongsTo('editor\Category', 'category_id', 'id');
    }

    public function media() {
        return $this->belongsTo('editor\Media', 'media_id', 'id');
    }

}
