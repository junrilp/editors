<?php

namespace editor;

use Illuminate\Database\Eloquent\Model;



class Template extends Model
{
    protected $table = 'template';
    //protected $fillable = ['template_data'];

    public function users() {
        return $this->hasOne('editor\User', 'id', 'user_id');
    }
}
