<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('media', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_name');
            $table->string('file_location');
			 $table->string('media_type');
            $table->string('height');
            $table->string('width');
            $table->timestamps();
	 });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('media');
    }
}
