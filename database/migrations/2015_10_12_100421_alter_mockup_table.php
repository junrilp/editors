<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMockupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mockup', function($table) {
          $table->enum('is_3d', array('yes', 'no'))->default('yes')->after('title');
          $table->float('price')->after('is_3d');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn('is_3d', array('yes', 'no'));
        $table->dropColumn('price');
    }
}
