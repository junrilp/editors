<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveMediaTableColumns extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('media', function (Blueprint $table) {
            $table->dropColumn('file_name');
            $table->dropColumn('file_location');
            $table->dropColumn('media_type');
            $table->dropColumn('height');
            $table->dropColumn('width');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('media', function (Blueprint $table) {
            $table->string('file_name');
            $table->string('file_location');
            $table->string('media_type');
            $table->string('height');
            $table->string('width');
        });
    }

}
