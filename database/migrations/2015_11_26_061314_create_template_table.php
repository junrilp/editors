<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('template', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('user_id')->references('id')->on('users')->onDelete('cascade');
                    $table->string('title');
                    $table->longText('template_data');
                    $table->integer('width');
                    $table->integer('height');
                    $table->integer('price');
                    $table->integer('status')->default('1');
                    $table->timestamps();
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('template');
    }

}
