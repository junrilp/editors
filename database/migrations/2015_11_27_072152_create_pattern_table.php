<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatternTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pattern', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('media_id')->references('id')->on('media')->onDelete('cascade');
                    $table->string('color');
                    $table->enum('type', array('pattern', 'image'))->default('pattern');
                    $table->string('title');
                    $table->integer('price');
                    $table->integer('status')->default('1');
                    $table->timestamps();
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pattern');
    }
}
