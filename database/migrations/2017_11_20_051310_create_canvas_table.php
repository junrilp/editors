<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCanvasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('canvas', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('user_id')->references('id')->on('users')->onDelete('cascade');
                    $table->integer('category_id');
                    $table->string('frame_id');
                    $table->string('project_title');
                    $table->longText('data');
                    $table->longText('img_data');
                    $table->integer('open');
                    $table->integer('status')->default('1');
                    $table->string('width');
                    $table->string('height');
                    $table->string('modified_by');
                    $table->string('parent_id');
                    $table->string('is_paid');
                    $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('canvas');
    }
}
