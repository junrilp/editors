<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMockupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mockup', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->references('id')->on('category')->onDelete('cascade');
			$table->integer('media_id')->references('id')->on('media')->onDelete('cascade');
            $table->string('title');
            $table->string('height');
            $table->string('width');
			$table->string('status');
			$table->timestamps();
	 });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mockup');
    }
}
