<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('media_id')->references('id')->on('media')->onDelete('cascade');
            $table->enum('menu_type', array('template', 'background','uploads','image'));
            $table->string('menu_title');
			$table->longText('data');
			$table->string('price');
			$table->string('help');
			$table->string('color_code');
			$table->string('status');
     		$table->timestamps();
	 });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu');
    }
}
