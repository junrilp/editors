<?php

use Illuminate\Database\Seeder;
use editor\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->truncate();
        Role::create([
            'id'            => 1,
            'name'          => 'admin'
            
        ]);
        Role::create([
            'id'            => 2,
            'name'          => 'subscriber'
           
        ]);
    }
}
