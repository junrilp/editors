<?php

use Illuminate\Database\Seeder;
use editor\Category;

class CategoryTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('category')->truncate();
        Category::create([
            'id' => 1,
            'name' => 'Book',
            'status'=>1
        ]);
        
        Category::create([
            'id' => 2,
            'name' => 'Binders',
            'status'=>1
        ]);
        
        Category::create([
            'id' => 4,
            'name' => 'Box',
            'status'=>1
        ]);

        Category::create([
            'id' => 5,
            'name' => 'DVD',
            'status'=>1
        ]);
        
         Category::create([
            'id' => 6,
            'name' => 'Devices',
             'status'=>1
        ]);
    }

}

