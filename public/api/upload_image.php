<?php

$fullpath = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://'.$_SERVER['HTTP_HOST'].'/online-editor/public/files';

$target_dir = "../files/";
$fileName = strtotime(date('d-m-Y H:i:s')) . "_" . basename($_FILES["file"]["name"]);
$target_file = $target_dir . $fileName;
$uploadOk = 1;
$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

//Create thumb image
function create_thumb($src,$dest,$desired_width = false, $desired_height = false)
{
    /*If no dimenstion for thumbnail given, return false */
    if (!$desired_height&&!$desired_width) return false;
    $fparts = pathinfo($src);
    $ext = strtolower($fparts['extension']);
    /* if its not an image return false */
    if (!in_array($ext,array('gif','jpg','png','jpeg'))) return false;

    /* read the source image */
    if ($ext == 'gif')
        $resource = imagecreatefromgif($src);
    else if ($ext == 'png')
        $resource = imagecreatefrompng($src);
    else if ($ext == 'jpg' || $ext == 'jpeg')
        $resource = imagecreatefromjpeg($src);
    
    $width  = imagesx($resource);
    $height = imagesy($resource);
    /* find the "desired height" or "desired width" of this thumbnail, relative to each other, if one of them is not given  */
    if(!$desired_height) $desired_height = floor($height*($desired_width/$width));
    if(!$desired_width)  $desired_width  = floor($width*($desired_height/$height));
  
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width,$desired_height);
  
    /* copy source image at a resized size */
    imagecopyresized($virtual_image,$resource,0,0,0,0,$desired_width,$desired_height,$width,$height);
    
    /* create the physical thumbnail image to its destination */
    /* Use correct function based on the desired image type from $dest thumbnail source */
    $fparts = pathinfo($dest);
    $ext = strtolower($fparts['extension']);
    /* if dest is not an image type, default to jpg */
    if (!in_array($ext,array('gif','jpg','png','jpeg'))) $ext = 'jpg';
    $dest = $fparts['dirname'].'/'.$fparts['filename'].'.'.$ext;
    
    if ($ext == 'gif')
        imagegif($virtual_image,$dest);
    else if ($ext == 'png')
        imagepng($virtual_image,$dest,1);
    else if ($ext == 'jpg' || $ext == 'jpeg')
        imagejpeg($virtual_image,$dest,100);
    
    /*return array(
        'width'     => $width,
        'height'    => $height,
        'new_width' => $desired_width,
        'new_height'=> $desired_height,
        'dest'      => $dest
    );*/
}

if (isset($_POST["submit"])) {
    //die('dhsgjhsdb');
    $check = getimagesize($_FILES["file"]["tmp_name"]);
    if ($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
$result = array();
// Check if file already exists
if (file_exists($target_file)) {
    $result['errors'][] = "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
//if ($_FILES["file"]["size"] > 500000) {
//     $result['errors'][] = "Sorry, your file is too large.";    
//    $uploadOk = 0;
//}
// Allow certain file formats
if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
    $result['errors'][] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    $result['errors'][] = "Sorry, your file was not uploaded.";
    $result['status'] = false;
} else {
    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
        
        //create thumb as well
        $src = $fullpath.'/'.$fileName;
        $dest = $target_dir.'thumb/'.$fileName;
        $desired_width = 125;
        $desired_height = 170;
        
        create_thumb($src, $dest, $desired_width,$desired_height);
        $result['success'] = "Your file has been uploaded.";
        $result['url'] = "files/" . $fileName;
        $result['thumb_url'] = "files/thumb/" . $fileName;
        $result['status'] = true;
    } else {
        $result['errors'] = "Sorry, there was an error uploading your file.";
        $result['status'] = false;
    }
}

echo json_encode($result);