
$("#media_limit").val(0);
var setScrollPos = 1;
$(document).ready(function () {
    //This is for add background ,when the page load background is selected and the description of background will be shown .
    $(".image-text").text('Background will be visible on the frontend view in the background tab');
    if (setScrollPos == 0) {
        $('.load-more').hide();
    }
    $('.color_picker').minicolors({
        control: $(this).attr('data-control') || 'wheel',
        defaultValue: $(this).attr('data-defaultValue') || '',
        format: $(this).attr('data-format') || 'hex',
        keywords: $(this).attr('data-keywords') || '',
        inline: $(this).attr('data-inline') === 'true',
        letterCase: $(this).attr('data-letterCase') || 'lowercase',
        opacity: $(this).attr('data-opacity'),
        position: $(this).attr('data-position') || 'top bottom',
        change: function (value, opacity) {
            if (!value)
                return;
            if (opacity)
                value += ', ' + opacity;
            if (typeof console === 'object') {
                var colorCode = value.split(',');
            }
        },
        theme: 'bootstrap'
    });

    //if backround is selected then background color field is shown else for image color field will be hide .
    $('.image_type').change(function () {
        $(".image-text").text("");
        if ($(this).val() == "image") {
            $(".image-text").text('Image will be visible on the frontend view in the Image tab');
            $('.color').hide();
        } else {
            $(".image-text").text('Background will be visible on the frontend view in the background tab');
            $('.color').show();
        }

    });

    //This is used in the media section..load more ,if images are gteater then 0 then images are shown else load more div will hide
    $(document).on('click', '.more', function () {
       // if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                getThumbNailImages();
        //}
    });

    // Media modal - thumb click toggle
    $("body").on("click", ".media_modal_thumb", function () {
        $(".media_modal_thumb").removeClass("selected")
        $(".img_thumb_chkbox").removeClass("checked").removeAttr("checked")
        $(this).addClass("selected")
        var id = $(this).data("id")
        var chkBox = $('#source_' + id)
        chkBox.toggleClass("checked")
        chkBox.attr("checked", chkBox.hasClass("checked"))
    })
    
     $("body").on("click", ".page-sidebar-wrapper .gallery", function () {
        $('#dashboard').addClass('active');
    });

    var getThumbNailImages = function () {
        $("#LoadingImage").show();
        $('.load-more').css("display", "block");
        $.ajax({
            url: $baseUrl + '/media',
            method: 'post',
            data: {_token: $('input[name=_token]').val(), limit: $("#media_limit").val()},
            success: function (json) {
                var res = jQuery.parseJSON(json);
                $("#LoadingImage").hide();
                $("#media_limit").val(res.limit);
                var data = "";
                setTotalMedia = res.totalMedia;
                setScrollPos = res.count;
                
                $("#renderMediaTotal").val(parseInt($("#renderMediaTotal").val()) + parseInt(setScrollPos));
                totalMediaRender= $("#renderMediaTotal").val();
                 
                if (parseInt(setTotalMedia) == parseInt(totalMediaRender)) {
                    $('.load-more').css("display", "none");
                }
                if (res.count > 0) {
                    $.each(res, function (index, element) {
                        if (element.src != undefined) {
                            data += '<div class="thumb-container col-lg-2 col-md-4 col-xs-6 thumb media_modal_thumb remove-item_' + element.id + '" data-id="' + element.id + '">'; 
                            data += '<div class="xs-action-media-control hidden">';
                            data += '<input type="' + $modalObj.mediaType + '" class="img_thumb_chkbox" name="img_thumb" id="source_' + element.id + '" value="' + element.id + '" key-src="' + element.src + '"></div>';
                            data += '<div><a href="#" class="thumbnail media_thumb ">'
                            data += '<img alt="" src="' + element.src + '" class="img-responsive">';
                            data += '</a></div>';
                            data += '<div class="remove-media-icon"><a href="javascript:void();" class="remove-item" img_id="' + element.id + '"><i class="fa fa-trash"></i></a></div>';
                            data += '</div>';

                        }
                    });

                    $("#thumbs_store.gallery, #thumbs_store2.gallery").append(data);
                }
                //alert(response)
            }, error: function (e) {
                $("#LoadingImage").hide();
                console.log(e);
            }
        });
    }

    $(document).on('click', '.media_thumb', function () {
       
        $('#media_id').val(" ");
        if ($(this).parent('div').find('input').attr('checked')) {
            $(this).parent('div').find('input').prop('checked', false);
        } else {
            $(this).parent('div').find('input').prop('checked', true);
        }
        var media_id = $(this).parent('div').parent('div').attr('data-id');
        $('#media_id').val(media_id);
    });


    $(document).on('click', '.remove-item', function (event) {
        event.preventDefault();
        var x = confirm("Are you sure you want to delete this Image ?. This will not be recoverable press OK to continue.");
        if(x){
            $.ajax({
            url: $baseUrl + '/media/' + $(this).attr("img_id"),
            method: 'post',
            data: {_token: $('input[name=_token]').val()},
            success: function (json) { 
                var res = jQuery.parseJSON(json);
                if (x){
                     $('.remove-item_' + res.id + '').remove(); 
                }
                else{
                    return ;
                }
              //  alert('Are you sure you want to delete this Image ?. This will not be recoverable press OK to continue.');
                
            }, error: function (e) {
                console.log(e);
            }
        });
    }else{
      return  
    }
    });

    var t = {
        url: $baseUrl + "/media/uploads",
        dragDrop: !0,
        fileName: "image",
        allowedTypes: "jpg,png,jpeg",
        returnType: "json",
        multiple: !1,
        showDone: !1,
        showAbort: !1,
        dragDropStr: "<span><b>Drag &amp; Drop File</b></span>",
        formData: {_token: $('input[name=_token]').val(), mediaSource: $("#media_source").val()},
        onSuccess: function (e, t) {
            // console.log(t)
            if ("success" == t.status) {
                $("#status").html("<div class='success-upload'>" + t.message + "</div>");
                var data = "";

                data += '<div class="col-lg-2 col-md-4 col-xs-6 thumb media_modal_thumb remove-item_' + t.id + '" data-id="' + t.id + '">'; 
                data += '<div class="xs-action-media-control hidden">';
                data += '<input type="' + $modalObj.mediaType + '" class="img_thumb_chkbox" name="img_thumb" id="source_' + t.id + '" value="' + t.id + '" key-src="' + t.image_path + '"></div>';
                data += '<div><a href="#" class="thumbnail media_thumb ">'
                data += '<img alt="" src="' + t.image_path + '" class="img-responsive">';
                data += '</a></div>';
                data += '<div><a href="javascript:void();" class="remove-item" img_id="' + t.id + '"><i class="fa fa-trash"></i></a></div>';
                data += '</div>'; 

                $("#thumbs_store, #thumbs_store2").append(data);
            } else {
                $("#status").html("<div class='error-upload'>" + t.message + "</div>");
            }
            setTimeout(function () { 
                $(".ajax-file-upload-statusbar").slideUp(500);
            }, 2e3);
        }
    };
    $("#mulitplefileuploader").uploadFile(t);
    
    var t2 = {
        url: $baseUrl + "/media/uploads",
        dragDrop: !0,
        fileName: "image",
        allowedTypes: "jpg,png,jpeg",
        returnType: "json",
        multiple: !1,
        showDone: !1,
        showAbort: !1,
        dragDropStr: "<span><b>Drag &amp; Drop File</b></span>",
        formData: {_token: $('input[name=_token]').val(), mediaSource: $("#media_source").val()},
        onSuccess: function (e, t) {
            // console.log(t)
            if ("success" == t.status) {
                $("#status").html("<div class='success-upload'>" + t.message + "</div>");
                var data = "";

                data += '<div class="col-lg-2 col-md-4 col-xs-6 thumb media_modal_thumb remove-item_' + t.id + '" data-id="' + t.id + '">'; 
                data += '<div class="xs-action-media-control hidden">';
                data += '<input type="' + $modalObj.mediaType + '" class="img_thumb_chkbox" name="img_thumb" id="source_' + t.id + '" value="' + t.id + '" key-src="' + t.image_path + '"></div>';
                data += '<div><a href="#" class="thumbnail media_thumb ">'
                data += '<img alt="" src="' + t.image_path + '" class="img-responsive">';
                data += '</a></div>';
                data += '<div><a href="javascript:void();" class="remove-item" img_id="' + t.id + '"><i class="fa fa-trash"></i></a></div>';
                data += '</div>'; 

                $("#thumbs_store, #thumbs_store2").append(data);
            } else {
                $("#status").html("<div class='error-upload'>" + t.message + "</div>");
            }
            setTimeout(function () { 
                $(".ajax-file-upload-statusbar").slideUp(500);
            }, 2e3);
        }
    };
    $("#mulitplefileuploader2").uploadFile(t2);
    
    $("#upload_content_frm").submit(function () {
        //$("#myUploads").modal('hide');
        
        var $selected_ids = '';
        var collectSource = [];
        $("#upload_content_frm fieldset input").each(function (index, value) {
            if ($(this).is(':checked')) {
                //alert($(this).val());
                $selected_ids += $(this).val() + ',';
                var collectSrc = $(this).attr('key-src');
                collectSource.push(collectSrc);
            }
        });
        $selected_ids = $selected_ids.substring(0, $selected_ids.length - 1);

        if ($selected_ids == '') {
            alert("Please choose at least one image");
        } else {
            //set pictures as thumbnail for media to be shown to user
            $.each(collectSource, function (index, value) {
                $("#" + $modalObj.mediaCollect).append('<img src="' + value + '" style="width:60px;margin-right:10px;"/>');
            });

            $('#' + $modalObj.mediaId).val($selected_ids);
            $("#myUploads").modal('hide');
//            $('.gallery').parent('li').removeClass('active');
            $(".modal-backdrop").remove();

        }
        return false;
    });

    
    $('.gallery').on('click',function(){
        if($(this).attr('data-flag') == 'media'){
            $('#myUploads .media-okay').hide();
        }else{
           $('#myUploads .media-okay').show();
        }
    })
    $('#myUploads, .background-popup').on('show.bs.modal', function (e) {
        $modalObj = $(e.relatedTarget).data();
        $('.page-sidebar-wrapper li').removeClass('active');
        $('.gallery').parent('li').addClass('active');
        $("#media_source").val($modalObj.mediaSource);
        getThumbNailImages();
        //$(e.currentTarget).find('input[name="bookId"]').val(bookId);
    });
    

   
    //hanles mockuups and sizes layout.
    var mockups_layout = function (val) {
        if (val == 'yes') {
            $(".xs-field-sizes").slideDown('slow');
        } else if (val == 'no') {
            $(".xs-category,.xs-title,.xs-price,.xs-media").slideUp('slow');
        } else {
            $(".xs-field-sizes").slideUp('slow');
        }
    }
    mockups_layout($("#mockup_size_type").val());

    $("#mockup_size_type").change(function () {
        mockups_layout($(this).val());
    });

});

