$(document).on('ready page:load', function() {
    $(document).foundation();
    //Show the template categories on page load 
    $("#loadingToolPage").show();
    $('.get-category-data:eq("0")').trigger("click");
    
});

//$('div.range-slider').foundation('slider', 'set_value', 5.01);

/**
 * Conacting to facebook
 */

/*window.fbAsyncInit = function() {
 FB.init({
 appId: '122729291410394',
 xfbml: true,
 version: 'v2.4'
 });
 };
 
 (function(d, s, id) {
 var js, fjs = d.getElementsByTagName(s)[0];
 if (d.getElementById(id)) {
 return;
 }
 js = d.createElement(s);
 js.id = id;
 js.src = "http://connect.facebook.net/en_US/sdk.js";
 fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));
 */

var progressBarCount, progressBarImg;
progressBarCount = 0;

resizeWindow = function() {
    $(".left-bar-nav-container").css("height", ($(window).height() - $('.section-header').height()));
    $(".right-bar-nav-container").css("height", ($(window).height() - $('.section-header').height()));
    $(".left-bar-nav-panel,.left-bar-nav-panel-2").css("height", ($(window).height() - $('.section-header').height()));
    var rgtPanel = ($(window).width() - parseFloat($('.left-bar-nav-container').width()));
    rgtPanel = (rgtPanel === 1253) ? rgtPanel - 1 : rgtPanel - 3;
    //Panel width
    $(".right-bar-nav-container,.text-editor-circle").css("width", rgtPanel);

    //Text Panel Images container height setting.
    var e = (($(window).height() - $('.section-header').height()) * 60) / 100;
    $(".scroll-loader").css("height", e);
};

full_width_left_bar = function() {
    //$(".left-bar-nav-container").css("minWidth", $(window).width());
    $(".left-bar-nav-panel-2").css({
        minWidth: ($(window).width() - $(".left-bar-nav-panel").width()),
        background: "#fff",
        overflowX: "hidden",
        overflowY: "auto"
    });
    var rgtPanel = ($(window).width() - parseFloat($('.left-bar-nav-container').width()));
    rgtPanel = (rgtPanel === 1253) ? rgtPanel - 1 : rgtPanel - 3;
    $(".right-bar-nav-container,.text-editor-circle").css("width", rgtPanel);
};

remove_width_left_bar = function() {
    $(".left-bar-nav-panel-2").css({
        minWidth: "auto",
        background: "transparent",
        overflow: "unset"
    });
    var rgtPanel = ($(window).width() - parseFloat($('.left-bar-nav-container').width()));
    rgtPanel = (rgtPanel === 1253) ? rgtPanel - 1 : rgtPanel - 3;
    $(".right-bar-nav-container,.text-editor-circle").css("width", rgtPanel);
};

/*No more uses*/
setCanvasDimension = function(w, h) {
    localStorage.setItem("canvas-width", $('#_user_canvas_dimWidth').val());
    localStorage.setItem("canvas-height", $('#_user_canvas_dimHeight').val());
    localStorage.setItem("active_temp_id", $('#_m_canvas_id').val());
    $("#_user_canvas_dimention").val(w + '_' + h);
};

backdrop = function(flag) {
    if (flag == true) {
        $(".reveal-modal-bg").click(function(event) {
            event.stopPropagation();
        });
    }
};

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var errorImage = function(el) {
    $(el).attr("src", "http://kustportal.edu.ng/aris/images/page_not_found.jpg");
}

var project_name_save = function(el) {
    var val = $(el).val();
    $(el).parent('a').removeClass("project-name-text").addClass("project-name-cls");
    $(el).parent('a').html(val);
    $("#_user_canvas_title").val(val);
    $(".project-save-canvas").removeClass("canvas-save");
}

var progressImageDownload = function() {
    $('#modalNavMenuOpt').foundation('reveal', 'open');
    progressBarImg = setInterval(function() {
        var n = Math.floor((Math.random() * 10) + 1);
        progressBarCount = (progressBarCount + n > 100) ? 100 : progressBarCount + n;
        $('.finalalize-image span.meter').css("width", progressBarCount + "%");
        $("#progress_percent_rate").text(progressBarCount + "%");
    }, 100);
}

var completeImageDownload = function() {
    clearInterval(progressBarImg);
    progressBarCount = 0;
    $('#modalNavMenuOpt').foundation('reveal', 'close');
//    $('.finalalize-image span.meter').css("width", "0");
//    $("#progress_percent_rate").text("0%");
}

$(document).ready(function() {

    //set the animation bar at the bottom according to window height
    var windowHeight = $(window).height();
    var bottomheight = windowHeight / $('.animation-bar-title').innerHeight();
    $(".uim-animation-bar-head").css('height', bottomheight);

    //Remove localStorage
    localStorage.removeItem('canvas-width');
    localStorage.removeItem('canvas-height');
    localStorage.removeItem('active_temp_id');
    resizeWindow();
    //resize_width_left_bar_on_tab();
    $("#picker_spectrums").spectrum({
        color: "#f00"
    });

    $(".xs-pot-select").click(function() {
        if ($(this).val() == 1) {
            $(".xs-custom-size").fadeOut('slow');
        } else {
            $(".xs-custom-size").fadeIn();
        }
    });
    $(document).on('click', function(e) {
        if (e.target.id != 'exportCanvas') {
            $("ul.export-canvas-to").css("display", "none");
        }

    })
    //hide the backgroung images on click of background icon
    $(document).on('click', '.backgrounds', function(e) {
        $('.layoutbackgroundDiv').hide();
    })

    //Show the inner div on click of circle icons
    $(document).on("click", '.text-options-circle', function() {
        if ($("body").find("." + $(this).find("a").attr("data-class")).length == 1) {
            $('.circle-inner-content').css('display', 'block');
        } else {
            $('.circle-inner-content').css('display', 'none');
        }
        $(".tblr-inner-data:not(." + $(this).find("a").attr("data-class") + ")").removeClass("text-editor-show");
        $("body").find("." + $(this).find("a").attr("data-class")).toggleClass("text-editor-show");
        $('.text-options-circle').removeClass('active');
        $(this).addClass('active');

    });

    //Trigger the wheel color picker on colorpicker icon click
    $(document).on('click', '.color-picker-button', function(e) {
        //Set the default color of wheel color picker to grey
        $('#trigger1').trigger('focus');
        $('.color--picker-input').val('000000')
        $('.xs_canvas_color_opt').css('display', 'block')
        $('.text-editor-circle').css('display', 'block');
    })

    //Trigger the wheel color picker on ok button
    $(document).on('click', '.color-submit', function(e) {
        //Set the default color of wheel color picker to grey
        $('#trigger1').trigger('click');
        $('.xs_canvas_color_opt').css('display', 'block')
        $('.text-editor-circle').css('display', 'block');
    })
    //Color-picker color changes
    $(document).on('click', '.inner_color_picker', function(e) {
        //console.log(e.originalEvent)
        var color;
        $(e.originalEvent.path).each(function(index, val) {
//            var color;
            $('.inner_color_picker').each(function(k,v){
                color = $(v).find('span').html();
            });
        });

        var buttonOptChoose = $('input:radio[name=textbutton_choose_for]:checked').val() ? $('input:radio[name=textbutton_choose_for]:checked').val() : $('input:radio[name=textcolor_choose_for]:checked').val();
        if(buttonOptChoose == 'text') plugin.setTextColor(color);
        else if(buttonOptChoose == 'button') plugin.setButtonColor(color);
        else if(buttonOptChoose == 'shape') plugin.setShapeColor(color);
    })

    //Change the color of text according to the selected color
    $(document).on('focus', '#trigger1', function(e) {
        var color;
        $('#trigger1').wheelColorPicker('getValue');
        if ($('.color--picker-input').val()) {
            color = $('.color--picker-input').val();
        } else {
            color = $('#trigger1').wheelColorPicker('getValue');
        }
        $('.color--picker-input').val(color);
        $('.jQWCP-wWidget').css('display', 'block');
        var t = '#' + color;
        var colorOptChoose = $('input:radio[name=color_choose_for]:checked').val();
        var buttonOptChoose = $('input:radio[name=button_choose_for]:checked').val();
//        console.log(colorOptChoose)
        switch (colorOptChoose || buttonOptChoose) {
            case 'text':
                plugin.setTextColor(t);
                break;
            case 'buttontext':
                plugin.setButtonTextColor(t);
                break;
            case 'stroke':
                plugin.setActiveProp("setStroke", t);
                break;
            case 'gradient':
                plugin.setActiveProp('setGradient', t);
                break;
            case 'button':
                plugin.setButtonColor(t);
                break;
            case 'shape':
                plugin.setShapeColor(t);
                break;
        }
        $('.color--picker-input').val('');
    });

    //hide and show the backgroung images and buttons on change of select box
    $(document).on('change ', '.background-button', function(e) {
        var selected = ($(this).val()).toLowerCase();
        $(".menuBgControls").hide();
        switch (selected) {
            case "colors":
                $(".colorControls").show();
                break;
            case "backgrounds":
                $(".backgroundControls").show();
                break;
            case "buttons":
                $(".buttonControls").show();
                break;
        }
    });
    /* Key events */

    $(document).keydown(function(e) {
        var activeObject = canvas.getActiveObject();
        var keycode = e.keyCode;
        if (e.keyCode == 46) {
            console.log('Delete');
            plugin.removeSelected();
            setZindexLayers();
        } else if (e.keyCode == 86 && e.ctrlKey) {
            console.log('ctrl V');
            plugin.setClone();
        } else if (e.keyCode == 89 && e.ctrlKey) {
            console.log('ctrl Y');
            plugin.redo();
        } else if (e.keyCode == 90 && e.ctrlKey) {
            console.log('ctrl Z');
            plugin.undo();
        } else if (e.keyCode == 83 && e.ctrlKey) {
            e.preventDefault();
            console.log('Save');
            $(".project-save-canvas").trigger("click");
        } else if (e.keyCode == 37) {
            console.log('left move');
            plugin.moveObject('left', 10);
        } else if (e.keyCode == 38) {
            console.log('top move');
            plugin.moveObject('top', 10);
        } else if (e.keyCode == 39) {
            console.log('right move');
            plugin.moveObject('right', 10);
        } else if (e.keyCode == 40) {
            console.log('bottom move');
            plugin.moveObject('bottom', 10);
        }
    });


    //  User can customize canvas by choosing default templates or own template by clicking on it  
    $("body").on("click", ".grid-item-value .card-block-wrapper .card-block-img, .customize-template", function() {
        $(this).attr('disabled','disabled');
        var source_id = $(this).attr('source_id');
        var width = $(this).attr('width');
        var height = $(this).attr('height');
        localStorage.setItem('canvas-width', width);
        localStorage.setItem('canvas-height', height);
        localStorage.setItem('templateId', source_id);
        localStorage.setItem('sourceType', 2);
        var u = window.location.origin;
        $("#_canvas_data_json").val(plugin.toJson());
        $("#_canvas_img_souce").val(plugin.toImage());

        $.ajax({
            url: '/gettemplate',
            data: {tid: localStorage.getItem('templateId'), _token: $("#oeditorForm input[name='_token']").val()},
            method: 'post',
            success: function(r) {

                var responseValue = JSON.parse(r);
                var rs = r;
                plugin.renderWithJson(rs);
                setTimeout(function() {
                    updateModifications(true);
                    updateAnimationSection();
                }, 1000);

                var canvas_data = {
                    canvas_data: responseValue.frames,
                    dimention: responseValue.width + '_' + responseValue.height,
                    title: responseValue.project_title,
                    _token: $("#oeditorForm input[name='_token']").val(),
                    category_id: responseValue.category_id,
                    canvas_id: responseValue.id,
                };
                $.ajax({
                    url: u + '/editor/clonesave',
                    type: 'post',
                    data: canvas_data,
                    success: function(data) {
                        $("#loadingToolPage").hide();

                        var res = jQuery.parseJSON(data);
                        if (res.status == 'success') {
                            $("#_m_canvas_id").val(res.tmpid);
                        }
                        window.location.href = "/editor/" + $("#_m_canvas_id").val();
                    }
                });
            }
        });


    });


    $("body").on("click", ".itemOptions ul.items li", function(e) {
        var vs = ($(this).attr("class"));
        var html = '';
        switch (vs) {
            case "itemPreview":
                var src = $(this).attr('link-src');
                html = '<img src="' + src + '" onerror="errorImage(this)">';
                break;
            case "itemInfo":
                html = '<p>If you want to design your cover offline and upload it<br>to convert into 3D, use the size below:</p>';
                html += '<h1 class="m-xsh1">' + $(this).attr('tg-width') + '<span>x</span>' + $(this).attr('tg-height') + ' </h1>';
                html += '<small class="m-xssmall">Width x Height measured in pixels.</small>';
                break;
            case "itemGuide":
                var src = $(this).attr('link-src');
                html = '<p>Download the template as a guide if you want to design the cover with your own design software. Save your design as a jpg and upload to our cover maker to generate the 3D mock up</p>';
                html += '<div class="downloadbtn"><a href="' + src + '" download></a></div>';
                html += '<br/><small>The final 2D cover can be downloaded</small>';
                break;
        }
        $('#modal3dLayoutOpt').foundation('reveal', 'open');
        $("#modal3dContentDiv").html(html);

    });


    $("body").on("click", ".project-name-cls", function() {
        var val = $(this).text();
        $(this).html('<input type="text" name="project_name" id="project_name" onblur="project_name_save(this)" value="' + val + '">');
        $(this).find('input').focus();
        $(this).removeClass("project-name-cls").addClass("project-name-text");

    });
    
   
    //Code to save the canvas data 
    $("body").on("click",".project-save-canvas", function() {
        var el = this;
        var saveButtonHtm = $(this).html();
        if ($(this).hasClass("canvas-save")) {
            alert("There is nothing to save!");
            return false;
        }else{
            $(el).text("Saving...");
        }
        //check if canvas is not blank
        if (plugin.checkCanvasLength() <= 0 && canvas.backgroundImage == 0 && canvas.backgroundColor == '#FFF') {
            alert("Blank canvas could not be saved! Please add some more content");
            return false;
        }
        updateModifications(true);
        plugin.toFrameImage('jpeg', false).then(function(src) {
            $(el).addClass("canvas-save");
            $("#_canvas_data_json").val(plugin.toJson());
            $("#_canvas_img_souce").val(JSON.stringify(src));
            $('#saveOverlayLoader').fadeIn(400);
            var u = window.location.origin;
            $.ajax({
                url: u + '/editor/save',
                type: 'post',
//                async: false,
                data: $("#oeditorForm").serialize() + $('#animation-bar-form').serialize(),
                success: function(data) {
                    
                    var res = jQuery.parseJSON(data);
                    if (res.status == 'success') {
                        window.onbeforeunload = function() {
                            return "Your customisation are saving. Are you sure want to quit this process, No changes will be save in case of page refresh";
                        };
                        $(el).addClass("canvas-save");
                        $("#_m_canvas_id").val(res.tmpid);
                    }
                    //window.location.reload();
                    setTimeout(function() {
                        $(el).html(saveButtonHtm);
                        $('#saveOverlayLoader').fadeOut(400);
                        window.onbeforeunload = null;
                        objectModified = false;
                    }, 3000);

                }
            });
        });
    });

    $("#modalLayoutSizeFx").foundation({
        reveal: {
            animation: 'fadeAndPop',
            animation_speed: 400,
            close_on_background_click: false,
            close_on_esc: false,
            dismiss_modal_class: 'close-reveal-modal',
            bg_class: 'reveal-modal-bg',
            bg: $('.reveal-modal-bg'),
            css: {
                open: {
                    'opacity': 0,
                    'visibility': 'visible',
                    'display': 'block'
                },
                close: {
                    'opacity': 1,
                    'visibility': 'hidden',
                    'display': 'none'
                }
            }
        }
    });
   

    //$(".uim-animation-bar-segment").css("max-width", $(document).width() - 200);
    //Animation bar section end
    $(".animate-triangle").on("click", function() {
          $('.uim-animation-bar-wrap').addClass('droppable');
          var bottom = ($(".uim-animation-bar-wrap").css("bottom") == '0px' ? -(Math.abs($('.uim-animation-bar-segment').height())) : 0);
          $(".uim-animation-bar-wrap").animate({
          bottom: bottom
          }, 500);
          $('.animate-triangle').css('transform','rotate(360deg)');
          var Right = ($(".count-layers-obj").css("right") == '0px' ? 110 : 0);
          if(bottom){
             $('.animate-triangle').css('transform','rotate(180deg)');
          }
         
         setZindexLayers();
    });

    // $(".animate-triangle").on("click", function() {
    //     var right = ($(".uim-animation-bar-wrap").css("right") == '0px' ? -(Math.abs($(".uim-animation-bar-wrap").width())) : 0);
    //     $(".uim-animation-bar-wrap").animate({
    //         bottom: "-130px"
    //     }, 500);
    //     $('.animate-triangle').css('transform','rotate(360deg)');
    //     var Right = ($(".count-layers-obj").css("right") == '0px' ? 110 : 0);
    //     if(Right){
    //         $('.animate-triangle').css('transform','rotate(180deg)');
    //     }
    //     $(".count-layers-obj").animate({
    //         right: Right
    //     }, 500);
    //     setZindexLayers();
    // });

    //Export click toggle
    $(".project-finalize-canvas").on("click", function() {
        canvas.deactivateAll().renderAll();
        updateAnimationSection();
        $(this).parent("li").find("ul.export-canvas-to").toggle();
    });

    /*
     *@descript: Export image in gif,jpg,png format
     *@return: downloaded image url 
     */
    $(".export-canvas-to li a").on("click", function() {
        $("#animation-bar-form #imagetype").val($(this).data("value"));
        if ($(this).data("value") == 'gif') {
            if ($("#animation-bar-form #imagetype").length == 1) {
                alert('Opps! You must need to create multiple frame for GIF functionality');
                $(".animate-triangle").trigger("click");
                return;
            }
        } else if($(this).data("value") == 'new'){
            window.location.href='/design';
        } else if($(this).data("value") == 'my'){
            window.location.href='/canvas';
        }
        progressImageDownload();
        $.ajax({
            type: 'POST',
            url: "/exports",
            dataType: "json",
            data: $("form#animation-bar-form").serialize(),
            success: function(data) {
                if (data.imgtype == 'gif') {
                    if (data.status == true) {
                        $('.finalalize-image span.meter').css("width", "100%");
                        $("#progress_percent_rate").text("100%");
                        $("#downloadImageCanvas").attr("href", data.path);
                        $('#downloadImageCanvas').attr("download");
                        $('#downloadImageCanvas')[0].click();

                    }
                } else { //other than gif image
                    if (data.archive_status == true) {

                        $('.finalalize-image span.meter').css("width", "100%");
                        $("#progress_percent_rate").text("100%");
//                        $("#downloadImageCanvas").attr("href", data.stack[i].path);
//                        $('#downloadImageCanvas').attr("download");
//                        $('#downloadImageCanvas')[0].click();
                        window.location.href = '/' + data.archive_path;
                    }
                }
                completeImageDownload();
            }
        });
    });

    //Render button on the canvas.
    $("body").on("click", ".buttonControls button", function() {
        var obj = {
            color: $(this).attr("color"),
            radius: $(this).attr("radius"),
            colortype: $(this).attr("colortype"),
            text: ($(this).text()).trim(),
            height: 37,
            width: 128
        };
        plugin.createButton(obj);
    });

    //Render shapes on the canvas.
    $("body").on("click", ".shapeControls #allShapes", function() {
        var obj;
        var textShape = this.querySelector("p").innerHTML;
            obj = {
                color: "#48b0f7",
                text: textShape,
                width: 100,
                height: 100
            };

        plugin.createShape(obj);
    });

    $("body").on("click", ".count-layers-obj ul li", function() {
//        console.log(this)
        $('.count-layers-obj ul li a').removeClass('active');
        $(this).find('a').addClass('active');
        var index = parseInt($(this).find("a").data("index"));
        plugin.makeObjectActive(index);
    });
    // When the user clicks on <span> (x), close the button modal
    $("body").on("click", "#buttonmodal span", function() {
        $('#buttonmodal').css('display', 'none');
        $('#overlaybuttonGroup').hide();
    });



});

$(window).resize(function() {
    resizeWindow();
    //This method exist in custom-fabric.js file
    centralized();
    //alert(window.isDisable);
    if (window.isDisable == 1) {
        full_width_left_bar();
    }
});

$(window).load(function() {
    $("#loadingToolPage").fadeOut(500);
    
    //This method exist in custom-fabric.js file
    centralized();
    
    window.editMode = '0';
    //push blank array into state if frames are greater then 1
    if ($("body").find(".animation-segment").length > 1) {
        $(".animation-segment").each(function(index) {
            state.push([]);
        });
    }
   $(".canvas-inside").css("display","block");
    //Load canvas for edit mode
    setTimeout(function() {
        
        plugin.renderWithJson($('#canvas_data').val());
        localStorage.setItem('canvas-width', $('#_user_canvas_dimWidth').val());
        localStorage.setItem('canvas-height', $('#_user_canvas_dimHeight').val());
        localStorage.setItem('active_temp_id', $('#_m_canvas_id').val());
//        plugin.addBoundingColor();
        updateModifications(true);
        updateAnimationSection();
        $(".canvas-inside").css("display","none");
        
    }, 500);
    
    $("#createCanvasEl").trigger("click");

    $(".xs-pot-select:radio[value=2]").prop("checked", true).trigger("click");
    //backdrop(true);

});
