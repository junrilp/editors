var baseUrl = "https://editor.bannerspro.com";
// if (window.location.host == 'localhost') {
//     baseUrl = window.location.origin + "/online-editor";
// } else {
//     baseUrl = window.location.origin;
// }

angular.module('infiniteScroll', [])
        .directive('infiniteScroll', ["$window", function($window) {
                return {
                    link: function(scope, element, attrs) {
                        var offset = parseInt(attrs.threshold) || 0;
                        var e = element[0];
                        element.bind('scroll', function() {
                            if (scope.$eval(attrs.canLoad) && e.scrollTop + e.offsetHeight >= e.scrollHeight - offset) {
                                scope.$apply(attrs.infiniteScroll);
                            }
                        });
                    }
                };
            }]);

var app = angular.module('editorApp', ['infiniteScroll', 'ngAnimate', 'colorpicker.module'], function($compileProvider) {
    $compileProvider.directive('compile', function($compile) {
        return function(scope, element, attrs) {
            scope.$watch(
                    function(scope) {
                        return scope.$eval(attrs.compile);
                    },
                    function(value) {
                        element.html(value);
                        $compile(element.contents())(scope);
                    }
            );
        };
    });

});

/**
 * This Service contains all the function which deals with file upload operations by ajax
 * 
 */
app.directive('dragcontent', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.draggable({
                revert: false,
                helper: 'clone',
                drag: function() {
//                    $(this).css("opacity", 0);
//                    $(this).css("color", "#3F4651");

                    $(".scroll-loader").css("overflow", "unset");
                    $("ul.laidOut.example-animate-containe ").css("overflow", "unset");

                },
//                revert: function() {
//                    $(this).animate({opacity: 1}, 1000);
//                    $(this).css("color", "#FFFFFF");
//                }
            });

            $("#canvas_section_head .canvas-container").droppable({
                drop: function(event, ui) {

                    $(".scroll-loader").css({overflowY: "auto", overflowX: "hidden"});
                    $("ul.laidOut.example-animate-containe").css({overflowY: "auto", overflowX: "hidden"});
                    var type = $(ui.draggable).attr("data-type");
                    switch (type) {
                        case 'text':
                            var obj = JSON.parse($(ui.draggable).find('a').attr("data-value"));
                            var lft = ui.offset.left - ($('canvas').offset().left);
                            var tp = ui.offset.top - ($('canvas').offset().top);

                            obj.top = tp;
                            obj.left = lft;
                            plugin.addText(obj);
                            if ($('#editor_tool').val() == 0) {
                                $("#section_toolkit_menu").css('display', 'none');
                            } else {
                                $(".text-editor-circle").css('display', 'none');
                            }

                            var activeObj = canvas.getActiveObject();
                            var leftOff = activeObj.width / 2 + obj.left;
                            var topOff = activeObj.height / 2 + obj.top;
                            var topEl, leftEl;
                            topEl = 0;
                            leftEl = 0;

                            // 25  - -.50
                            // 50  0
                            // 75 .34
                            // 100 /40
                            // 150 .50
                            // 200 .60
                            var zoomLabel = $("#zoomTextCaption").attr('data-value');
                            var factor;
                            var bs = 0.20;
                            switch (zoomLabel) {
                                case '25':
                                    factor = -4.2;
                                    break;
                                case '50':
                                    factor = -1.2;
                                    break;
                                case '75':
                                    factor = -.38;
                                    break;
                                case '100':
                                    factor = 0;
                                    break;
                                case '150':
                                    factor = .30;
                                    break;
                                case '200':
                                    factor = .45;
                                    break;
                            }

                            var finalLeft = leftOff - (leftOff * factor);
                            var finalTop = topOff - (topOff * factor);

                            activeObj.set({
                                left: finalLeft,
                                top: finalTop
                            });
                            activeObj.setCoords();
                            canvas.deactivateAll().renderAll();
                            setTimeout(function() {
                                canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1));
                                var obj = canvas.getActiveObject();
                                setCirclePosition()
                                if ($('#editor_tool').val() == 0) {
                                    $("#section_toolkit_menu").css('display', 'block');
                                } else {
                                    $(".text-editor-circle").css('display', 'block');
                                }
                            }, 1000);

                            break;
                        case 'image':
                            $(".canvas-inside").css("display","block");
                            var obj = {};
                            var src = $(ui.draggable).attr("image-src");
                            var lft = ui.offset.left - ($('canvas').offset().left);
                            var tp = ui.offset.top - ($('canvas').offset().top);

                            obj.top = tp;
                            obj.left = lft;
                            obj.src = src;
                            obj.bgimg = 1;
                            plugin.addImage(1, src);
                            break;
                        case 'layout':
                            var obj = $(ui.draggable).parent('a').attr("source-id");
                            $(ui.draggable).animate({opacity: 1}, 1000);
                            var u = (window.location.host == 'localhost') ? "http://localhost/online-editor" : "http://uat.uimatic.com/online-editor";
                            $.ajax({
                                url: u + '/gettemplate',
                                data: {tid: obj, _token: $("#oeditorForm input[name='_token']").val()},
                                method: 'post',
                                success: function(r) {
                                    var rs = r;
                                    plugin.renderWithJson(rs);
                                    setTimeout(function() {
                                        updateModifications(true);
                                    }, 1000);

                                }
                            });
                            break;
                        case 'backImage':
                            $(".canvas-inside").css("display","block");
                            var obj = $(ui.draggable).attr("data-img");
                            $(ui.draggable).animate({opacity: 1}, 1000);
                            plugin.addPattern(obj);
                            if ($(ui.draggable).attr("data-color") !== '' && $(ui.draggable).attr("data-color") !== undefined) {
                                plugin.setCanvasBackground($(ui.draggable).attr("data-color"));
                            }
                            break;
                        case 'uploadImage':
                            $(".canvas-inside").css("display","block");
                            var obj = {};
                            var src = $(ui.draggable).attr("image-src");
                            var lft = ui.offset.left - ($('canvas').offset().left);
                            var tp = ui.offset.top - ($('canvas').offset().top);

                            obj.top = tp;
                            obj.left = lft;
                            obj.src = src;
                            obj.bgimg = 1;
                            plugin.addImage(1, src);
                            break;
                    }

                }
            });
        }
    };
});

app.service('fileUpload', ['$http', function($http) {

        /**
         * 
         * @param {object} file : file to be upload
         * @param {type} uploadUrl : Url of the api or php file which will handle all file upload operations. Location where file is placed or uploaded must be spacified in php file for dasta security 
         * 
         */
        this.uploadFileToUrl = function(file, uploadUrl) {
            var fd = new FormData();
            fd.append('file', file);
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                    .success(function(response) {
                        return response;
                    })
                    .error(function() {
                        return response;
                    });
        };

    }]);


app.factory('facebookService', function($q) {
    return {
        getMyLastName: function() {
            FB.login(function(response) {
                if (response.authResponse) {
                    var access_token = FB.getAuthResponse()['accessToken'];
                    console.log('Access Token = ' + access_token);

                    var deferred = $q.defer();
                    FB.api('/me/photos/uploaded', function(response) {
                        if (!response || response.error) {
                            deferred.reject('Error occured');
                        } else {
                            deferred.resolve(response);
                        }
                    });
                    return deferred.promise;
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }
            }, {scope: 'user_photos, user_posts'});



        }
    };
});

app.controller('leftNavBarController', function($scope, $http, facebookService, fileUpload) {
    window.dragflag=true;
    //set layout as default tab
    $scope.tog = 1;
    if (window.anonymousLocation !== 1) {
        window.isDisable = $scope.isDisable = 1;
    }
    //$scope.API: contains all api urls.
    $scope.API = {
        imageUpload: "/editor/upload_image/" + $("#_m_canvas_id").val(),
        getUserImage: "/editor/get_user_images/" + $("#_m_canvas_id").val()
    };
    $scope.layouts = [];
    $scope.texts = [];
    $scope.backgrounds = [];
    $scope.uImages = [];
    $scope.canLoad = true;

    // $scope.backgroundColors: contains all the default colors for backgrounds section in left side bar section
    $scope.backgroundColors = ["#000000", "#cc0000", "#ffcc00", "#009900", "#0066cc"];
    // $scope.defaultTextColors: contains all the default colors for text on canvas
    $scope.defaultTextColors = ["#000000", "#ffffff", "#cc0000", "#ffcc00", "#009900", "#0066cc"];
    // $scope.userImages: Contains all the images of user it must get updated on every draw, to leverage garbage data
    $scope.userImages = [];
    if (window.location.host == 'localhost') {
        $scope.baseUrl = window.location.origin + "/online-editor";
    } else {
        $scope.baseUrl = window.location.origin;
    }

    $scope.mediaThumb = 'uploads/thumbnails';
    $scope.mediaThumb200 = 'uploads/thumbnails_200';
    $scope.prolix = 'uploads/prolix';

    $scope.fbImages = [];
    $scope.fontSizeLimit = ['12', '14', '16', '18', '20', '22', '24', '26', '28', '30', '40', '50', '60', '70', '80', '90', '100'];
    $scope.fileUploadProcessing = false;

    //Button render
    $scope.buttonsRect = {
        set_1: [
            {"type": "rect", "text": "Click Here", "color": "#48b0f7", "radius": "4", "colortype": "fill"},
            {"type": "rect", "text": "Click Here", "color": "#48b0f7", "radius": "4", "colortype": "outline"},
            {"type": "rect", "text": "Find Out More", "color": "#6ec0fa", "radius": "4", "colortype": "fill"},
            {"type": "rect", "text": "Find Out More", "color": "#6ec0fa", "radius": "4", "colortype": "outline"},
            {"type": "rect", "text": "Add to Cart", "color": "#3a8fc8", "radius": "4", "colortype": "fill"},
            {"type": "rect", "text": "Add to Cart", "color": "#3a8fc8", "radius": "4", "colortype": "outline"},
            {"type": "rect", "text": "Click Here", "color": "#60b937", "radius": "12", "colortype": "fill"},
            {"type": "rect", "text": "Click Here", "color": "#60b937", "radius": "12", "colortype": "outline"},
            {"type": "rect", "text": "Find Out More", "color": "#ff6e21", "radius": "12", "colortype": "fill"},
            {"type": "rect", "text": "Find Out More", "color": "#ff6e21", "radius": "12", "colortype": "outline"},
            {"type": "rect", "text": "Click Here", "color": "#c64643", "radius": "4", "colortype": "fill"},
            {"type": "rect", "text": "Click Here", "color": "#c64643", "radius": "4", "colortype": "outline"},
            {"type": "rect", "text": "Buy Now", "color": "#ff6e21", "radius": "4", "colortype": "fill"},
            {"type": "rect", "text": "Buy Now", "color": "#ff6e21", "radius": "4", "colortype": "outline"},
            {"type": "rect", "text": "Apply Now", "color": "#f67975", "radius": "4", "colortype": "fill"},
            {"type": "rect", "text": "Apply Now", "color": "#f67975", "radius": "4", "colortype": "outline"}
        ]
    }

    $scope.shapes = {
        set_1: [
            {"type": "circle", "text": "shape1", "color": "#48b0f7", "width": "60px", "height": "60px", "background": "../images/finalShapes/shape1.png"},
            {"type": "circle", "text": "shape2", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape2.png"},
            {"type": "circle", "text": "shape3", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape3.png"},
            {"type": "circle", "text": "shape4", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape4.png"},
            {"type": "circle", "text": "shape5", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape5.png"},
            {"type": "circle", "text": "shape6", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape6.png"},
            {"type": "circle", "text": "shape7", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape7.png"},
            {"type": "circle", "text": "shape8", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape8.png"},
            {"type": "circle", "text": "shape9", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape9.png"},
            {"type": "circle", "text": "shape10", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape10.png"},
            {"type": "circle", "text": "shape11", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape11.png"},
            {"type": "circle", "text": "shape12", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape12.png"},
            {"type": "circle", "text": "shape13", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape13.png"},
            {"type": "circle", "text": "shape14", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape14.png"},
            {"type": "circle", "text": "shape15", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape15.png"},
            {"type": "circle", "text": "shape16", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape16.png"},
            {"type": "circle", "text": "shape17", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape17.png"},
            {"type": "circle", "text": "shape18", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape18.png"},
            {"type": "circle", "text": "shape19", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape19.png"},
            {"type": "circle", "text": "shape20", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape20.png"},
            {"type": "circle", "text": "shape21", "color": "#48b0f7", "width": "70px", "height": "70px", "background": "../images/finalShapes/shape21.png"},
        ]
    }
    //Texts
    $scope.text = {
        set_1: [
            {"type": "text", "showfontSize": "50px", "fontSize": "50", "fontFamily": "Abril Fatface", "color": "#000", "text": "Add heading", "fontWeight": "bold", "fromTop": "100"},
            {"type": "text", "showfontSize": "50px", "fontSize": "50", "fontFamily": "Asap", "color": "#000", "text": "Add heading", "stroke": "#cc0000", "strokeWidth": "1", "textShadow": "red 2px 1px 0px", "fontWeight": "normal", "fromTop": "100"},
            {"type": "text", "showfontSize": "50px", "fontSize": "50", "fontFamily": "Bree Serif", "color": "#000", "text": "Add heading", "fontWeight": "bold", "fromTop": "100"},
            {"type": "text", "showfontSize": "230%", "fontSize": "60", "fontFamily": "Courgette", "color": "#000", "text": "Add Subheading", "fontWeight": "Italic", "fromTop": "60"},
            {"type": "text", "showfontSize": "230%", "fontSize": "60", "fontFamily": "Dancing Script", "color": "#000", "text": "Add Subheading", "stroke": "green", "strokeWidth": "1", "textShadow": "green 2px 1px 0px", "fontWeight": "Italic", "fromTop": "60"},
            {"type": "text", "showfontSize": "230%", "fontSize": "60", "fontFamily": "Gentium Basic", "color": "#000", "text": "Add Subheading", "fontWeight": "Italic", "fromTop": "60"},
            {"type": "text", "showfontSize": "153%", "fontSize": "50", "fontFamily": "Lobster", "color": "#000", "text": "Add a little bit of body text", "fontWeight": "normal", "fromTop": "20"},
            {"type": "text", "showfontSize": "153%", "fontSize": "50", "fontFamily": "Lobster Two", "color": "#000", "text": "Add a little bit of body text", "stroke": "yellow", "strokeWidth": "1", "textShadow": "yellow 2px 1px 0px", "fontWeight": "normal", "fromTop": "20"},
            {"type": "text", "showfontSize": "153%", "fontSize": "50", "fontFamily": "Merriweather", "color": "#000", "text": "Add a little bit of text ", "fontWeight": "normal", "fromTop": "20"},
        ]
    }


    $scope.setCanvasDim = function() {
        var source_id = $(this).find('img').attr('source_id');
        var dimensions = $('input:radio[name=dimensions]:checked').val();
        var category = $("#_category_id").val();
        var redirectFlag = true;
        if (category == '') {
            alert("Please select category");
            return;
        }
        else if (dimensions === undefined && angular.element(document.querySelector('.width')).val() == '' && angular.element(document.querySelector('.height')).val() == '') {
            alert("Please select canvas size");
            redirectFlag = false;
        } else if (angular.element(document.querySelector('.width')).val() != '' && angular.element(document.querySelector('.height')).val() != '') {
            localStorage.setItem('canvas-width', angular.element(document.querySelector('.width')).val());
            localStorage.setItem('canvas-height', angular.element(document.querySelector('.height')).val());
            dimensions = localStorage.getItem('canvas-width') + '_' + localStorage.getItem('canvas-height');
            redirectFlag = true;
        } else if (dimensions != undefined) {
            var value = dimensions.split("_");
            localStorage.setItem('canvas-width', value[0]);
            localStorage.setItem('canvas-height', value[1]);
            redirectFlag = true;
        }

        //Setting up form element -> diamention for the form element, Don't remove this line, using for DB entry
        plugin.toImage('jpeg', false).then(function(src) {
            plugin.toImage('jpeg', true).then(function(thumb) {
                $("#_user_canvas_dimention").val(dimensions);
                $("#_canvas_data_json").val(plugin.toJson());
                $("#_canvas_img_souce").val(src);
                $("#_canvas_img_thumb").val(thumb);
                $.ajax({
                    url: window.location.origin + '/editor/save',
                    type: 'post',
                    data: $("#oeditorForm").serialize(),
                    success: function(data) {

                        var res = jQuery.parseJSON(data);
                        if (res.status == 'success') {
                            $("#_m_canvas_id").val(res.tmpid);
                        }
                        if (redirectFlag) {
                            window.location.href = '/editor/' + $("#_m_canvas_id").val();
                        }
                    }
                });
            });
        });
    };
    $scope.designStepTwo = function(w, h, title) {
        title = (title && title != undefined) ? title : '';
        localStorage.setItem("canvas-title", title);
        window.isDisable = 0;
        $("#menu_3d").remove();
        remove_width_left_bar();

        //Set Canvas Diamension to local storage
        setCanvasDimension(w, h);

        //Request for layout template ajax
        $scope.addItems(2);
        setTimeout(function() {
            adjustCanvas();
        });
    };

    /*$scope.setCanvasSize  = function(){
     var val = parseInt($('input:radio[name=optionCol]:checked').val());
     if (val == 1) {
     window.isDisable  = $scope.isDisable = 1;
     full_width_left_bar();
     } else {
     window.isDisable = $scope.isDisable = 0;
     var dia = ($("#layout_canvas_w_h").val()).split("_");
     var w = parseInt(dia[0]);
     var h = parseInt(dia[1]);
     remove_width_left_bar();
     setCanvasDimension(w,h);
     
     localStorage.setItem("canvas-title", ':Untitled Project');
     $scope.addItems(2);
     setTimeout(function(){
     adjustCanvas();
     },1000);
     
     }
     $('#modalLayoutSizeFx').foundation('reveal', 'close');
     setTimeout(function(){
     $("#canvas_section_head .canvas-container").css("opacity",1);
     },200);
     };*/

    $scope.setCanvasSize = function() {
        window.isDisable = $scope.isDisable = 0;

        //var dia = ($("#layout_canvas_w_h").val()).split("_");
        //get the local strORAGE VALUE WITH HEIGHT AND WIDTH
        var w = parseInt(localStorage.getItem('canvas-width'));
        var h = parseInt(localStorage.getItem('canvas-height'));
        remove_width_left_bar();
        setCanvasDimension(w, h);
        localStorage.setItem("canvas-title", ':Untitled Project');
        $scope.addItems(2);
        $('#modalLayoutSizeFx').foundation('reveal', 'close');
        setTimeout(function() {
            adjustCanvas();
            $("#canvas_section_head .canvas-container").css("opacity", 1);

            //-Currently we are not using this function because using realod function for inserting/updating template
            //-Dont delete this code             
            /*if (localStorage.getItem('sourceType') == 2) {
             $.ajax({
             url: '/gettemplate',
             data: {tid: localStorage.getItem('templateId'), _token: $("#oeditorForm input[name='_token']").val()},
             method: 'post',
             success: function (r) {
             var rs = r;
             plugin.renderWithJson(rs);
             setTimeout(function () {
             updateModifications(true);
             updateAnimationSection();
             }, 1000);
             }
             });
             }*/
        }, 1000);
    };


    $scope.addItemsOnScroll = function() {

        //var url = 'api/data' + $scope.tog + '.json';

        var url = ($scope.tog == 1) ? '/cover' : '/tabdata?tog=' + $scope.tog + '&limit=0';
        //for fetching the preformatted texts on click of text menu , Fetcch the json data of different texts
        if ($scope.tog == 3) {
            url = $scope.baseUrl + '/api/data3.json';
        }
        if ($scope.tog == 2) {
            url += '&width=' + localStorage.getItem("canvas-width") + '&height=' + localStorage.getItem("canvas-height") + '&id=' + localStorage.getItem("active_temp_id");

        }
        $http.get(url).success(function(data) {
            switch ($scope.tog) {
                case 1:
                    $scope.threeD = data;
                    break;
                case 2:
                    for (var i = 0; i < data.length; i++) {
                        $scope.layouts.push(data[i]);
                    }
                    break;
                case 3:
                    $scope.texts = data;
                    break;
                case 4:
                    $('.layoutbackgroundDiv').hide();
                    for (var i = 0; i < data.length; i++) {
                        $scope.backgrounds.push(data[i]);
                    }
                    break;
                case 6:
                    for (var i = 0; i < data.length; i++) {
                        $scope.uImages.push(data[i]);
                    }
                    break;
                case 7:
                    for (var i = 0; i < data.length; i++) {
                        $scope.backgrounds.push(data[i]);
                    }
                    break;
                case 8:
                    $(".buttonControls").show();
                    break;
                case 9:
                    $(".shapeControls").show();
                    break;
                default:
                    break;
            }

            jQuery(".scroll-loader").height(jQuery(".left-bar-nav-panel-2").height() - 30);
        }).error(function(data, status) {
            console.log(data || "Request failed");
        });
    };

    /**
     * function to fire addItemsOnScroll() without scroll and data will be added in the section correspond to $scope.tog value
     **/
    $scope.reset = function() {
        $scope.items = [];
        $scope.canLoad = true;
    };

    $scope.reset();

    /**
     * Function to fetch data first time in corresponding sections on clicking LAYOUT/TEXT/BKGROUND
     **/
    $scope.addItems = function(togId) {
        $('.layoutbackgroundDiv').hide();
        if (window.isDisable == '1' && togId != 1) {
            $(".right.xs-nav-options").css("display", "none");
            return false;
        }

        $scope.tog = togId;

        if ($scope.tog == '1') {
            $(".right.xs-nav-options").css("display", "none");
            full_width_left_bar();
            return false;
        }
        $(".right.xs-nav-options").css("display", "block");
        //remove full width
        remove_width_left_bar();
        switch ($scope.tog) {
            case 1:
                break;
            case 2:
                if ($scope.layouts.length === 0) {
                    $scope.reset();
                    $scope.addItemsOnScroll();
                }

                break; 
            case 3:
                $scope.addItemsOnScroll();
                //Only Text is adding right now
//                if ($scope.texts.length === 0) {
//                 $scope.reset();
//                 }
                break;
            case 4:
                $('.layoutbackgroundDiv').hide();
                if ($scope.backgrounds.length === 0) {
                    $scope.reset();
                    $scope.addItemsOnScroll();
                }
                break;
            case 5:

                if ($scope.userImages.length === 0) {
                    $scope.getUserImges(0, '');
                }
                break;
            case 6:
                if ($scope.uImages.length === 0) {
                    $scope.reset();
                    $scope.addItemsOnScroll();
                }
                break;
            case 7:
                if ($scope.backgrounds.length === 0) {
                    $scope.reset();
                    $scope.addItemsOnScroll();
                }
                break;
            case 8:
                $scope.addItemsOnScroll();
                break;
            case 9:
                $scope.addItemsOnScroll();
                break;
            default:
                break;
        }

        //Text Panel Images container height setting.
        if (document.getElementsByClassName('scroll-loader') > 0) {
            var e = (($(window).height() - $('.section-header').height()) * 70) / 100;
            setTimeout(function() {
                document.getElementsByClassName('scroll-loader')[0].style.height = e + "px";
            }, 1000);
        }
    };



    /*
     * Function to set $scope.myFile which store file to be uploaded. After this uploadFile() function uploade file stored in $scope.myFile.
     * @element {object} : current object.
     * 
     */
     $scope.dropFile = function(event) {
           event.preventDefault();
           $scope.myFile = event.dataTransfer.files[0];
           $scope.uploadFile();
     }

    $scope.setFile = function(element) {
        $scope.myFile = element[0].files[0];
        $scope.uploadFile();
    };


    /*
     * Function to upload file stored in $scope.myFile. destination where file will be stored will be spacified in php file for data security.
     * @uploadUrl {string} : URL of ajax file(API path for uploading image).
     */
    $scope.uploadFile = function() {
        var file = $scope.myFile;
        var uploadUrl = $scope.API.imageUpload;
        var fd = new FormData();
         var elem = document.getElementById("myBar");   
          var width = 20;
          var id = setInterval(frame, 10);

          function frame() {
            if (width >= 100) {
              clearInterval(id);
            } else {
              width++; 
              elem.style.width = width + '%'; 
            }
          }
        fd.append('image', file);
        $scope.fileUploadProcessing = true;

        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })

                .success(function(response) {
                    if (response.status) {
                        var currentImage = {src: response.url, thumb: response.thumb_url};
                        $scope.fileUploadStatus = response.success;
                        $scope.getUserImges(1, currentImage);
                        // $("#user_upload_section").append('<li class="large-4 column"><a onclick="return false;" data-type="uploadImage" dragcontent><img onclick="return false;" width="125" height="170" thumb-src="'+baseUrl+'/public/'+currentImage.src+'" src="'+baseUrl+'/public/'+thumbImage.thumb+'" style="height: auto;" ></a></li>');
                        $scope.fileUploadProcessing = false;
                        plugin.addImage(1,currentImage.src);
                        setTimeout(function() {
                            $scope.fileUploadStatus = false;
                            $scope.$apply();
                        }, 10000);
                    } else {
                        $scope.fileUploadStatus = "Try again.";
                        $scope.fileUploadProcessing = false;
                        setTimeout(function() {
                            $scope.fileUploadStatus = false;
                            $scope.$apply();
                        }, 10000);
                    }

                })
                .error(function() {
                    console.log(response);
                    return response;
                });
    };


    /*
     * Function to fetch images uploaded by user.
     */
    $scope.getUserImges = function(mode, currentImage) {
        var getUserImgesUrl = $scope.API.getUserImage;
        $http.post(getUserImgesUrl, {id: '1'}).then(function(response) {
            if (response.data.length > 0) {
                if (mode = 1) {
                    //  response.data.push(currentImage)
                }
                $scope.userImages = response.data;
                $scope.noUserImages = 1;
            } else {
                $scope.noUserImages = -1;
            }
        }, function(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    };

    /**
     * To get last name from facebook
     * @returns {undefined}
     */

    $scope.getMyLastName = function() {

        FB.login(function(response) {
            if (response.authResponse) {
                var access_token = FB.getAuthResponse()['accessToken']; // currently not in use
                var result = [];
                FB.api('/me/photos/uploaded', function(response) {
                    if (!response || response.error) {
                        result.push('Error occured');
                    } else {
                        $scope.fbImages = [];
                        $scope.userImages = [];
                        angular.forEach(response.data, function(value, key) {
                            FB.api('/' + value.id + '/picture?type=normal', function(image_details) {
                                var data = {src: image_details.data.url}
                                $scope.userImages.push(data);
                                $scope.$apply();
                            });
                        });
                    }
                });
                $scope.noUserImages = 1;
            } else {
                console.log('User cancelled login or did not fully authorize.');
            }
        }, {scope: 'user_photos, user_posts'});
    };
    var element = document.getElementsByClassName("droppable");
    if (element != '') {
        makeDroppable(element, callback);
    }

    function callback(files) {
       
        // Here, we simply log the Array of files to the console.
        $scope.myFile = files[0];
        $scope.uploadFile();

    }

    function makeDroppable(element, callback) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('multiple', true);
        input.style.display = 'none';
     
        function addEventListenerEl(objElement) {
            
            objElement.addEventListener('dragover', function(e) {
                
                $scope.tog = 5;
                e.preventDefault();
                e.stopPropagation();
                $('.upper-canvas').addClass('dragover');
                $(objElement).addClass('dragover');

            });
            objElement.addEventListener('dragleave', function(e) {
           
                e.preventDefault();
                e.stopPropagation();
                $('.upper-canvas').removeClass('dragover');
                $(objElement).removeClass('dragover');
            });
           
            objElement.addEventListener('drop', function(e) {
                e.preventDefault();
                e.stopPropagation();
               
                if (e.target.className == 'canvas-container-main droppable dragover' || e.target.className == 'upper-canvas canvas-frame dragover') {
                    triggerCallback(e);
                }
                 $(objElement).removeClass('dragover');
                 $('.upper-canvas').removeClass('dragover');
            });

        }

        for (var i = 0; i < element.length; i++) {
            addEventListenerEl(element[i]);
        }

        function triggerCallback(e) {
            var files;
            if (e.dataTransfer) {
                files = e.dataTransfer.files;
            } else if (e.target) {
                files = e.target.files;
            }
            callback.call(null, files);
        }

    }

});

//Render as an HTML
app.filter('html', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});