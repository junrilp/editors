function settingMenuChecked(objType, el) {
    //console.log(objType);
    switch (objType) {
        case 'menu_select':
            if ($(el).find('i').length > 0) {
                $(el).find("i").remove();
            } else {
                $(el).append('<i class="check-on"></i>');
            }
            break;
        case 'menu_radio':
            $(".xs-popover-more-layer li a[data-attr='left']").find("i").remove();
            $(".xs-popover-more-layer li a[data-attr='center']").find("i").remove();
            $(".xs-popover-more-layer li a[data-attr='right']").find("i").remove();
            $(el).append('<i class="check-on"></i>');
            break;
    }
}

function dataValueString(e) {
    var t = e.reduce(function(e, t) {
        return e + "[data-value='" + t + "'],"
    }, "");
    return t = t.substring(0, t.length - 1)
}

function setTextModalState(e) {
    $(".panel-text").find(".active").removeClass("active").end().find(dataValueString(e)).addClass("active").trigger("click")
}

function getTextModalState() {
    var e = {}, t = ($(".panel.text"), {
        fill: ["#000000", "#FFFFFF", "#6A737B", "#0060A9", "#00A4E4", "#522E91", "#E74898"],
        fontSize: ["12", "14", "18", "24", "30", "36", "48", "60"],
        textAlign: ["left", "center", "right", "justify"],
        styles: ["bold", "italic", "underline", "line-through"]
    }),
    a = {
        bold: "fontWeight",
        italic: "fontStyle",
        underline: "textDecoration",
        "line-through": "textDecoration"
    }, n = function(e) {
        return $(dataValueString(t[e])).filter(".active").data("value")
    };
    e.fill = n("fill"), e.fontSize = n("fontSize"), e.textAlign = n("textAlign");
    var i = n("styles");
    return "bold" != i && (e.fontWeight = "100"), e[a[i] || "fontWeight"] = i, e
}

var $ = jQuery.noConflict(),
        canvas = new fabric.Canvas("canvas", {
            backgroundColor: "#FFF"
        }),
canvasImage = new fabric.Canvas("canvas_mirror", {
    backgroundColor: "#FFF"
}),
f = fabric.Image.filters,
        arrStore = new Array,
        debug = 1,
        _pen_color = $(".pencil-line-color.active").attr("data-value"),
        _pen_width = $(".pencil-line-width.active").attr("data-value"),
        _isControlChange = !1,
        state = [[]],
        frames = [],
        framedata = [],
        framethumb = [],
        mods = 0,
        modRedo = 0,
        zoomStart = 1,
        zoomRatio = 0.28,
        canvasDefaultHeight = canvas.height,
        canvasDefaultWidth = canvas.width,
        canvasCenterAxis = canvas.getCenter(),
        imgPercentRatio = 30,
        bottomTBarRatio = 10,
        topTBarRatio = 85,
        leftTBarRatio = 85,
        increseZoomlabelRatio = 25,
        timerInterval = 0,
        selectedFrame = 1,
        frameEditMode = true,
        fitToScreen = false;
        objectModified = false;

fabric.Object.prototype.cornerSize = 8, fabric.Object.prototype.toObject = function(e) {
    return function() {
        return fabric.util.object.extend(e.call(this), {
            foobar: this.foobar
        });

    }
}(fabric.Object.prototype.toObject);


// mouse event
fabric.util.addListener(canvas.upperCanvasEl, 'dblclick', function(e) {
    if (canvas.findTarget(e)) {
        var objType = canvas.findTarget(e).type;
        var activeObj = canvas.getActiveObject();
        if (objType === 'group') {
            $('#overlaybuttonGroup').show();
            $('#buttonmodal').css('display', 'block');
            $('#buttonText').val(activeObj._objects[1].text)
        }
        // To change the text of the buttons
        $(document).on("click", "#buttonmodal .changeText", function() {
            rearrangeButtons();

        });
    }
});
//fabric.Canvas.prototype.customiseControls({
//        tl: {
//            action: 'rotate',
//            cursor: 'cow.png',
//        },
//        tr: {
//            action: 'scale',
//        },
//        bl: {
//            action: 'remove',
//            cursor: 'pointer',
//        },
//        br: {
//            action: 'moveUp',
//            cursor: 'pointer',
//        },
//        mb: {
//            action: 'moveDown',
//            cursor: 'pointer',
//        },
////        mr: {
////            action: function(e, target) {
////                target.set({
////                    left: 200,
////                });
////                canvas.renderAll();
////            },
////            cursor: 'pointer',
////        },
////        mt: {
////            action: {
////                'rotateByDegrees': 30,
////            },
////            cursor: 'pointer',
////        },
//    });
//
//    // basic settings
//    fabric.Object.prototype.customiseCornerIcons({
//        settings: {
//            borderColor: '#0094dd',
//            cornerSize: 18,
//            cornerShape: 'rect',
//            cornerBackgroundColor: 'grey',
//        },
//         tl: {
//            icon: 'icons/rotate.svg'
//        },
//        tr: {
//            icon: 'icons/resize.svg'
//        },
//        bl: {
//            icon: 'icons/remove.svg'
//        },
//        br: {
//            icon: 'icons/up.svg'
//        },
//        mb: {
//            icon: 'icons/down.svg'
//        },
////        mt: {
////            icon: 'icons/rotate.svg'
////        }
//    }, function() {
//        canvas.renderAll();
//    });
//    
function setImagePosition(obj) {
    var activeObj = canvas.getActiveObject();
    var leftOff = activeObj.width / 2 + obj.left;
    var topOff = activeObj.height / 2 + obj.top;
    var topEl, leftEl;
    topEl = 0;
    leftEl = 0;

    var zoomLabel = $("#zoomTextCaption").attr('data-value');
    var factor;
    switch (zoomLabel) {
        case '25':
            factor = (obj.bgImg == 0) ? .35 : 1;
            break;
        case '50':
            factor = (obj.bgImg == 0) ? .1 : .80;
            break;
        case '75':
            factor = (obj.bgImg == 0) ? -0.15 : .15;
            break;
        case '100':
            factor = (obj.bgImg == 0) ? -0.10 : .10;
            break;
        case '150':
            factor = (obj.bgImg == 0) ? -0.5 : -0.3;
            break;
        case '200':
            factor = (obj.bgImg == 0) ? -0.5 : -0.5;
            break;
    }
    var finalLeft = leftOff + (leftOff * factor);
    var finalTop = topOff + (topOff * factor);
    activeObj.set({
        left: finalLeft,
        top: finalTop,
        opacity: 1
    });
    canvas.deactivateAll().renderAll();
    canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1));
    canvas.getActiveObject().setCoords();

    /*setTimeout(function(){
     canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1));
     $("#section_toolkit_menu").show();
     },2000);*/
}

function setCirclePosition() {
    var activeObject = canvas.getActiveObject();
    var canvasOffset = $('#canvas').offset();
    var zoomStatus = $("#zoomTextCaption").attr('data-value');
    var tp, lf;
    switch (zoomStatus) {
        case '25':
            tp = (activeObject.getTop() * 90 / 100) - 150;
            lf = (activeObject.getLeft() * 90 / 100) + 150;
            break;
        case '50':
            tp = (activeObject.getTop() * 60 / 100) - 100;
            lf = (activeObject.getLeft() * 60 / 100) + 100;
            break;
        case '75':
            tp = (activeObject.getTop() * 30 / 100) - 50;
            lf = (activeObject.getLeft() * 30 / 100) + 50;
            break;
        case '100':
            tp = 0;
            lf = 0;
            break;
        case '150':
            tp = activeObject.getTop() * (-40 / 100);
            lf = activeObject.getLeft() * (-40 / 100) - 150;
            break;
        case '200':
            tp = activeObject.getTop() * (-80 / 100);
            lf = activeObject.getLeft() * (-80 / 100) - 200;
            break;
    }
    if (activeObject.type == 'image') {
        $(".text-editor-circle").css({
            top: (((canvasOffset.top + activeObject.getTop()) - 150) - tp),
            left: ((canvasOffset.left + activeObject.getLeft()) + 150) - lf,
        })
    } else {
        $(".text-editor-circle").css({
            top: ((canvasOffset.top + activeObject.getTop()) - tp) - $(".text-editor-circle").height(),
            left: ((canvasOffset.left + activeObject.getLeft()) + (activeObject.width / 2)) - lf,
        })
    }
}


function positionToolBar(obj) {
    var zoomCaptionValue = parseInt($("#zoomTextCaption").attr('data-value'));
    var top, left, tp, lf;
    if (zoomCaptionValue >= 150 && $(window).width() < 3000) {
        top = obj.getBoundingRect().top - 20;
        left = obj.getBoundingRect().left + 350;
    } else {
        tp = (($(".canvas-container-main").height() - $("#canvas_section_head .canvas-container").outerHeight()) / 2) + $(".canvas-container-main").scrollTop();
        lf = (($(".canvas-container-main").width() - $("#canvas_section_head .canvas-container").outerWidth()) / 2) + $(".canvas-container-main").scrollLeft();
        top = (obj.getBoundingRect().top + tp) - topTBarRatio;
        left = obj.getBoundingRect().left + lf;
    }

    top = (top < 0) ? (tp - topTBarRatio) : top;
    left = (left < 0) ? lf : left;

    $('.text-editor-circle ,#section_toolkit_menu').css({
        top: top,
        left: left
    });

    //top-left
    if (obj.getBoundingRect().top < 0) {
        var top = top + 200;
        $('.text-editor-circle , #section_toolkit_menu').css({
            top: top
        });
    }
}

function setZindexLayers() {
    var totalObj = (canvas.getObjects().length - 1);
    var selectedObj = canvas.getActiveObject();
    var indexPos = canvas.getObjects().indexOf(selectedObj);

    if (totalObj < 1) {
        $(".move-f-layer-icon,.move-b-layer-icon").addClass("disable");
    } else if (indexPos === totalObj) {
        $(".move-f-layer-icon").addClass("disable");
        $(".move-b-layer-icon").removeClass("disable");
    } else if (indexPos === 0) {
        $(".move-f-layer-icon").removeClass("disable");
        $(".move-b-layer-icon").addClass("disable");
    }
    if (totalObj > 1 && indexPos != totalObj && indexPos != 0) {
        $(".move-f-layer-icon,.move-b-layer-icon").removeClass("disable");
    }
    createLayer(indexPos);
}
function createLayer($bp) {
    var totalObj = (canvas.getObjects().length - 1);
    $(".count-layers-obj ul").remove();
    if (totalObj < 0) {
        $(".count-layers-obj").css("opacity", 0);
        return false;
    }
    $(".count-layers-obj").css('opacity', 1);
    var li = '';
    for (var i = totalObj; i >= 0; i--) {
        var selected = ($bp == i) ? 'active' : '';
        li += '<li><a id="bound_layer_' + i + '" class="bound-layer-ball ' + selected + '" href="#" data-index="' + i + '"></a></li>';
    }
    $(".count-layers-obj").html('<ul>' + li + '</ul>');
    $(".count-layers-obj ul ").sortable({
        axis: "y",
        start: function(event, ui) {
            $(ui.item).data('start', $(ui.item).index());
        },
        update: function(event, ui) {
            var start = (totalObj - $(ui.item).data('start'));
            var end = (totalObj - $(ui.item).index());
            updateZPosition(start, end);
        }
//        stop: function(event, ui) {
//            
//            var currentIndex  = parseInt($(ui.item[0]).find("a").data("index"));
//            var newPos = parseInt(ui.item.index());
//            console.log(newPos);
//            console.log('move pos' + (totalObj - newPos));
//            var object = canvas.getActiveObject();
//            console.log(object)
//            canvas.moveTo(object, (totalObj - newPos));
//            plugin.makeObjectActive(newPos)
//            canvas.renderAll();
//
//            //Update index
//            var len = totalObj;
//            $(".count-layers-obj ul li").each(function(i) {
//                $(this).find("a").attr("data-index", len);
//                len--;
//            });
//        }
        
    }).disableSelection();
   
}
function updateZPosition(start, end) {
    
        // offset to account for id == canvas object
        start += 0;
        end += 0;
        var objects = canvas.getObjects();
        var len = objects.length;
        var item = objects.splice(start, 1)[0];
        objects.splice(end, 0, item);
        plugin.makeObjectActive(end)
        canvas.renderAll();
}

function updateModifications(savehistory) {
    
    if (savehistory === true) {

        $("#undoOption,#redoOption").css('visibility', 'visible');
        //var ActiveframeId = $(".frame-active").parent('div').attr("data-count");
        var ActiveframeId = selectedFrame;
        var e = canvas.toJSON();
        var canvasData = {};
        canvasData = JSON.stringify(e);
        //console.log(ActiveframeId)
        state[ActiveframeId - 1].push(canvasData);
        $(".project-save-canvas").removeClass("canvas-save");
        plugin._debug("Slate values updated for index:" + (ActiveframeId - 1));
        plugin._debug(state);
        mods = ((state[ActiveframeId - 1]).length - 1);
        if (objectModified) {
            window.onbeforeunload = function() {
                return "Your customisation are saving. Are you sure want to quit this process, No changes will be save in case of page refresh";
            };
        }
    }
    updateAnimationSection();

}

function updateAnimationSection() {
    
    if (frameEditMode) {
        //Export all data to second canvas and deactivate all object to and then export from this canvas
        //This new canvas is hidden and using for second one.
        canvasImage.loadFromJSON(plugin.toJsonFrame(), function() {
            canvasImage.deactivateAll().renderAll();
        });
       setTimeout(function() {
            var seg = (selectedFrame);
            var dom = $(".uim-animation-bar-segment .animation-segment").eq((seg - 1));
            var frameData = plugin.toJsonFrame();
            var imgBase64 = plugin.toImageCopy();
            dom.find(".animation-segment-img").css("background-image", "url(" + imgBase64 + ")");
//            dom.find("img").attr("src", imgBase64);
            dom.find(".frame").val(imgBase64);
            dom.find(".framedata").val(frameData);
            $(".animation-segment").each(function(index) {
                localStorage.setItem('frame_' + $(this).attr("data-count"), $(this).find(".framedata").val());
            });
//            localStorage.setItem('frame_' + selectedFrame, plugin.toJsonFrame());
        }, 100);
    }
}

function updateToolTip(e) {
    $("#font-family").val(e.getFontFamily());
    $("#font-size").val(e.getFontSize());
    $("#textColorPickerId").parent('li').css('background-color', e.fill);
    $("#buttonColorPickerId").parent('li').css('background-color', e.fill);
    $('.text-options-circle').removeClass('active');
    $('.text-options-circle').removeClass('active');

}

function updateCircleToolTip(e) {
    $("#circle-font-family").val(e.getFontFamily());
    $("#circle-font-size").val(e.getFontSize());
    $('.text-options-circle').removeClass('active');

}

function updateButtonToolTip(e) {
    $("#font-family").val(e._objects[1].getFontFamily());
    $("#buttonColorPickerId").parent('li').css('background-color', e._objects[1].stroke);
    $('.text-options-circle').removeClass('active');
    $('.tblr-inner-data').removeClass("text-editor-show");
}

function resetToolTip() {
    $("#color-plates-text,.xs-popover-more-layer").hide('fast');
    $("#color-plates-button,.xs-popover-more-layer").hide('fast');
    $('.tblr-inner-data').removeClass("text-editor-show");
    $("#textColorPickerId").removeClass("picker-open");
    $("#buttonColorPickerId").removeClass("picker-open");
    $(".more-layer-icon").removeClass("open-pop");
}


function centralized() {
    var top = (($(".canvas-container-main").height() - $("#canvas_section_head .canvas-container").outerHeight()) / 2) + $(parent).scrollTop();
    var left = (($(".canvas-container-main").width() - $("#canvas_section_head .canvas-container").outerWidth()) / 2) + $(parent).scrollLeft();
    top = (top < 0) ? 0 : top;
    left = (left < 0) ? 0 : left;
    $("#canvas_section_head .canvas-container").css({
        top: (top + "px"),
        left: (left + "px")
    });
}

/*
 * @info Adjust canvas according to custom width and 3d mockup height
 * @return boolen 
 */
function adjustCanvas() {
    //reset zomming ratio
    zoomStart = 1;
    $("#zoomTextCaption").attr("data-value", 100);
    var w = localStorage.getItem("canvas-width");
    var h = localStorage.getItem("canvas-height");
    canvas.setWidth(w);
    canvas.setHeight(h);
    canvasDefaultWidth = w;
    canvasDefaultHeight = h;
    canvas.renderAll();

//    canvasImage.senderAll();

    canvasCenterAxis = canvas.getCenter();
    centralized();
    triggerOut();
}


//recursive function until canvas fit to the screen
function triggerOut() {
    var w = canvas.width;
    var h = canvas.height;
    var targetWidth = $(".right-bar-nav-container").width();
    var targetHeight = $(".right-bar-nav-container").height();
    if (w > targetWidth) {
        $("#zoomOut").trigger("click");
        triggerOut();
    } else if (h > targetHeight) {

        $("#zoomOut").trigger("click");
        triggerOut();
    }

    $("#canvas_section_head.canvas-container-main div.canvas-container").css("visibility", "visible");

}

//Rearrage the animation frames when one frame is deleted.
function rearrangeFrames() {
    $('.animation-segment').each(function(index, val) {
        $(this).attr('data-count', (index + 1));
    });
}

//Remove selected local stroage
function removeLocalStroage(niddleStr) {
    $.each(window.localStorage, function(key, value) {
        var regEx = new RegExp(niddleStr, 'gi');
        if (key.match(regEx) != null) {
            localStorage.removeItem(key);
        }
    });
}

//Rearrange local stroage
function reArrangeLocalStorage(seq) {
    var niddle = "frame_";
    localStorage.removeItem(niddle + seq); //remove selected stack first
    var frameStore = [];
    $.each(window.localStorage, function(key, value) {
        var regEx = new RegExp(niddle, 'gi');
        if (key.match(regEx) != null) {
            frameStore.push(value);
        }
    });
    removeLocalStroage(niddle); //Remove all local stroage first
    //Insert and re arrange local stroage again
    $.each(frameStore, function(index, value) {
        localStorage.setItem(niddle + (index + 1), value);
    });
}

function reArrangeUndoStore(seq) {
    var indexArr = (seq - 1);
    if (state[indexArr] == undefined)
        return;
    delete state[indexArr];
    var newStore = [];
    for (var i = 0; i < state.length; i++) {
        if (state[i] != undefined) {
            newStore.push(state[i]);
        }
    }
    state = newStore;
    // console.log(state);
}

function rearrangeButtons() {
    var activeObj = canvas.getActiveObject();
    //console.log(activeObj)
    var left = activeObj.left;
    var top = activeObj.top;
    var color = activeObj._objects[0].fill;
    var fontColor = activeObj._objects[1].fill;
    var stroke = activeObj._objects[1].stroke;
    var width =  $('#buttonText').val();
    //console.log(activeObj._objects[0].radius,)
   canvas.remove(activeObj)
     var text = new fabric.IText($('#buttonText').val(), {
        fill: fontColor,
        fontWeight:"bold",
        fontSize: 13,
        fontFamily: "arial",
    });
    var rect = new fabric.Rect({
        width: text.getWidth() + 30,
        height: 37,
        fill: color ,
         rx: 4,
        ry: 4,
        
    });
    
    text.set({
        left: ((rect.getWidth() / 2.02) - (text.getWidth() / 2)),
        top: ((rect.getHeight() / 2) - (text.getHeight() / 2.3))
    });
    canvas.add(new fabric.Group([rect, text], {
        left: left,
        top: top,
        //fill: color ,
        stroke: color,
        height: 37,
        
    }));
    
    $('#overlaybuttonGroup').hide();
    canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1));
    canvas.renderAll();
    plugin.addBoundingColor();
    updateModifications(true);
}
var newObj, text, rect, textObj;
var plugin = {

    addText: function(obj) {
        textObj = obj;
        var t = canvasCenterAxis;
        var top = (obj.hasOwnProperty("top")) ? obj.top : ((t.top) - (obj.fromTop));
        var left = (obj.hasOwnProperty("left")) ? obj.left : (t.left);
        console.log(obj)
        var e = obj.text,
                a = {
                   top: top,
                    left: left,
                    opacity: (obj.opacity) ? obj.opacity : 1,
                    fontFamily: (obj.fontFamily) ? obj.fontFamily : "Abril Fatface",
                    fontSize: obj.fontSize,
                    fontWeight: obj.fontWeight,
                    stroke: obj.stroke,
                    shadow: (obj.shadow) ? obj.shadow : null,
                    fill: (obj.fill) ? obj.fill : null,
                    lockUniScaling: true,
                    /*hasControls : false,
                     hasBorders :false*/
                };
               // console.log("text this"+ obj.stroke)
        //$.extend(a, getTextModalState());
        var n = new fabric.IText(e, a).on("editing:exited", function() {
            updateModifications(true);
        });
        canvas.add(n), canvas.renderAll(),updateModifications(true), canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1));
    },
    addLine: function() {
        var height = canvasDefaultHeight;
        var t = canvasCenterAxis;
        canvas.add(new fabric.Line([0, 0, 0, height], {
            left: t.left,
            top: -3,
            stroke: '#CDD1CD',
            strokeWidth: 2,
            hasControls: false,
            hasBorders: false,
            hasRotatingPoint: false,
            centeredRotation: false,
            lockMovementX: true,
            lockMovementY: true,
            lockScalingX: true,
            lockScalingY: true
        }));
        canvas.renderAll();
    },
    addBoundingColor: function(e) {
        e = e ? e : 0, canvas.getObjects().length > 0 && canvas.item(canvas.getObjects().length - 1).set({
            cornerColor: "#41CE91",
            padding: e,
            transparentCorners: !1
        });
    },
    createCurve: function(e, t, a) {
        canvas.remove(e);
        var n = new CurvedText(canvas, t);
        n.setText(a);
        var i = canvas.item(canvas.getObjects().length - 1),
                o = {
                    attr: "stroke",
                    value: t.stroke
                };
        plugin.applyPropToGroup(i._objects, o), i.set({
            fill: t.fill,
            fontFamily: t.fontFamily
        }), canvas.setActiveObject(i)
    },
    applyFilter: function(e, t) {
        var a = canvas.getActiveObject();
        a.filters[e] = t, a.applyFilters(canvas.renderAll.bind(canvas))
    },
    applyFilterLayers: function(e, t, a) {
        var n = canvas.item(a);
        n.filters[e] = t, n.applyFilters(canvas.renderAll.bind(canvas))
    },
    applyFilterValue: function(e, t, a) {
        var n = canvas.getActiveObject();
        n.filters[e] && (n.filters[e][t] = a, n.applyFilters(canvas.renderAll.bind(canvas)))
    },
    applyPropToGroup: function(e, t) {
        var a = (t.attr, t.value);
        e && jQuery.each(e, function(e, t) {
            t.set({
                stroke: a
            })
        })
    },
    setActiveProp: function(e, t) {
      var a = canvas.getActiveObject();
        if (a && 0 != a.length) {
            var n = a;
            switch (e) {
                case "fontFamily":
                   if (t == "Lobster") {
                       var inText = new fabric.IText(n.text);
                       inText.set({
                           
                            fontFamily: t
                       });
                        n.set({
                            width: 1000,
                            fontFamily: t
                        });
                        canvas.renderAll(), updateModifications(true);
                    } else {
                        n.set({
                            fontFamily: t
                        });
                    } 
                    if (text != undefined) {
                        text.set({
                            originX: 0
                        });
                    } 
                    break;
                case "fontStyle":
                    if (n.fontStyle) {
                        n.set({
                            fontStyle: ''
                        });
                    } else {
                        n.set({
                            fontStyle: t
                        });
                    }
                    break;
                case "fontAlignment":
                    n.set({
                        textAlign: t
                    });
                    break;
                case "lineHeight":
                        if (t < 1) {
                            if (t == 0) { n.set({ lineHeight: 0.99 }); }
                            else if (t == -0) { n.set({ lineHeight: 0.96 }); }
                            else if (t == -1) { n.set({ lineHeight: 0.93 }); }
                            else if (t == -2) { n.set({ lineHeight: 0.9 }); }
                            else if (t == -3) { n.set({ lineHeight: 0.89 }); }
                            else if (t == -4) { n.set({ lineHeight: 0.86 }); }
                            else if (t == -5) { n.set({ lineHeight: 0.83 }); }
                            else if (t == -6) { n.set({ lineHeight: 0.8 }); }
                            else if (t == -7) { n.set({ lineHeight: 0.79 }); }
                            else if (t == -8) { n.set({ lineHeight: 0.76 }); }
                            else if (t == -9) { n.set({ lineHeight: 0.73 }); }
                            else if (t == -10) { n.set({ lineHeight: 0.7 }); }
                        } else {
                            n.set({
                                lineHeight: t
                            });
                        }
                    break;
                case "letterSpacing":
                    n.set({
                        lineHeight: t
                    });
                    break;
                case "textColor":
                    n.set({
                        fill: t
                    });
                    break;
                case "buttonTextColor":
                    n._objects[1].set({
                        stroke: t
                    });
                    break;
                case "buttonColor":
                    n.set({
                        fill: t
                    });
                    break;
                case "shapeColor":
                    n.set({
                        fill: t
                    });
                    break;
                case "fontSize":
                    n.set({
                        fontSize: t
                    });
                    break;
                case "fontWeight":
                    if (n.fontWeight === "normal") {
                        n.set({
                            fontWeight: "bold"
                        });
                    } else {
                        n.set({
                            fontWeight: "normal"
                        });
                    }
                    break;
                case "fontOpacity":
                    n.setOpacity(t);
                    break;
                case "setText":
                    if (/group/.test(n.type)) {
                        var i = t,
                                o = n.toObject();
                        delete o.type, o.textAlign = "center", o.radius = 100, o.spacing = 10, o.fontSize = 20, o.fontWeight = "bold", o.angle = "0", o.fontFamily = o.objects[0].fontFamily, o.stroke = o.objects[0].stroke, o.strokeWidth = o.objects[0].strokeWidth, plugin.createCurve(n, o, i)
                    } else
                        n.setText(t);
                    break;
                case "setStroke":
                    if (/group/.test(n.type)) {
                        var r = n._objects;
                        r.forEach(function(e) {
                            e.setStroke(t), e.setStrokeWidth(1)
                        })
                    } else
                        n.setStroke(t), n.setStrokeWidth(1);
                    break;
                case "setThinStroke":
                    if (/group/.test(n.type)) {
                        var r = n._objects;
                        r.forEach(function(e) {
                            e.setStroke(t), e.setStrokeWidth(1)
                        })
                    } else
                        n.setStroke(t), n.setStrokeWidth(1);
                    break;
                case "setMediumStroke":
                    if (/group/.test(n.type)) {
                        var r = n._objects;
                        r.forEach(function(e) {
                            e.setStroke(t), e.setStrokeWidth(3)
                        })
                    } else
                        n.setStroke(t), n.setStrokeWidth(3);
                    break;
                case "setThickStroke":

                    if (/group/.test(n.type)) {
                        var r = n._objects;
                        r.forEach(function(e) {
                            e.setStroke(t), e.setStrokeWidth(5)
                        })
                    } else
                        n.setStroke(t), n.setStrokeWidth(5);
                    break;
                case "removeStroke":
                    if (/group/.test(n.type)) {
                        var r = n._objects;
                        r.forEach(function(e) {
                            e.setStroke(!1), e.setStrokeWidth(!1)
                        });
                    } else
                        n.setStroke(!1), n.setStrokeWidth(!1);
                    break;
                case "setTextDecoration":
                    /text/.test(n.type) && n.setTextDecoration(t);
                    break;
                case "setShadow":
                    if (n.getShadow()) {
                        n.setShadow("");
                    } else {
                        n.setShadow("3px 3px 10px rgba(0,0,0,0.5)");
                    }
                    break;
                case "setGradient":
                    n.setGradient('fill', {
                        type: 'linear',
                        x1: 0,
                        y1: n.height / 5,
                        x2: 0,
                        y2: n.height / 2,
                        colorStops: {
                            0: t,
                            0.4: plugin.hexToRGBA(t, 40),
                            0.8: plugin.hexToRGBA(t, 40),
                            1: plugin.hexToRGBA(t, 30),
                        }
                    });
                    break;
                case "undo":
                    "setTextDecoration" == t.prop && n.setTextDecoration("none");
                    break;
                case "removeElement":
                    canvas.remove(n);
                    break;
                case "removeProperties":
                    n.set({
                        fontWeight: "100",
                        textDecoration: "none",
                        fontStyle: "normal"
                    });
                    break;
                case "setFlipLayerX":
                    var l = n.flipX === !0 ? !1 : !0;
                    n.set("flipX", l)
                    break;
                case "setFlipLayerY":
                    var l = n.flipY === !0 ? !1 : !0;
                    n.set("flipY", l)
                    break;
                case "setTextTransform":
                    if (n.getText() === n.getText().toUpperCase()) {
                        n.set({
                            text: (n.getText()).toLowerCase()
                        });
                    } else {
                        n.set({
                            text: (n.getText()).toUpperCase()
                        });
                    }
                    break;
                case "addListType":
                    n.set({
                        textTransform: t
                    });
                    break;
            }
            
            canvas.renderAll();
            updateModifications(true);
        }
    },
    setFlipLayerX: function(e) {
        var obj = canvas.getActiveObject();
        if (!obj)
            return;
        this.setActiveProp("setFlipLayerX", e);
    },
    setFlipLayerY: function(e) {
        var obj = canvas.getActiveObject();
        if (!obj)
            return;
        this.setActiveProp("setFlipLayerY", e);
    },
    setFont: function(e) {
        this.setActiveProp("fontFamily", e);
    },
    setFontStyle: function(e) {
        this.setActiveProp("fontStyle", e.toLowerCase())
    },
    setFontWeight: function(e) {
        this.setActiveProp("fontWeight", e.toLowerCase())
    },
    setAlignment: function(e) {
        this.setActiveProp("fontAlignment", e.toLowerCase())
    },
    setLineHeight: function(e) {
        this.setActiveProp("lineHeight", e)
    },
    setLetterSpacing: function(e) {
        this.setActiveProp("letterSpacing", e)
    },
    setTextColor: function(e) {
        console.log(e)
        var t = canvas.getActiveObject();
        t && "i-text" === t.type && this.setActiveProp("textColor", e)
    },
    setButtonTextColor: function(e) {
        var t = canvas.getActiveObject();
        t && "i-text" === t._objects[1].type && this.setActiveProp("buttonTextColor", e)
    },
    setButtonColor: function(e) {
        var t = canvas.getActiveObject();
         //console.log(t.text);
     //   console.log(canvas.getActiveObject().get('type'));
        t && "group" === t.type && this.setActiveProp("buttonColor", e)
    },
    setShapeColor: function(e) {
        var t = canvas.getActiveObject();
         //   console.log(canvas.getActiveObject().get('type'));
        //console.log(t.text);
        t && "group" === t.type && this.setActiveProp("shapeColor", e)
    },
    setFontSize: function(e) {
        this.setActiveProp("fontSize", e)
    },
    setTextDecoration: function(e) {
        this.setActiveProp("setTextDecoration", e)
    },
    setFontOpacity: function(e) {
        e /= 100, this.setActiveProp("fontOpacity", e)
    },
    setRadiusSpacing: function(e, t) {
        var a = canvas.getActiveObject();
        a && a.set(e, t), canvas.renderAll()
    },
    setText: function(e) {
        this.setActiveProp("setText", e.toUpperCase())
    },
    setColorPicker: function(e) {
        $("#colorPicker").spectrum({
            color: e,
            showInput: !0,
            allowEmpty: !0
        }), $("#colorPicker").on("dragstop.spectrum", function(e, t) {
            console.log('fgcbdfxhb')
            plugin.setTextColor(t.toHexString())
        })
    },
    setLogoColor: function(e) {
        plugin.applyFilter(12, new f.Tint({
            color: e,
            opacity: .9
        }));
    },
    setCanvasBackground: function(e) {
        canvas.setBackgroundColor(e);
        canvas.renderAll();
        updateModifications(true);
    },
    setTextTransform: function(e) {
        plugin.setActiveProp("setTextTransform", e);
    },
    setZoom: function(e) {
        canvas.setHeight(canvasDefaultHeight * e);
        canvas.setWidth(canvasDefaultWidth * e);
        canvas.setZoom(e);
        canvas.renderAll();
    },
    getActiveProp: function(e) {
        var t = canvas.getActiveObject();
        if (0 !== t.length) {
            var a = t;
            switch (e) {
                case "getText":
                    $("#areaText").val(/group/.test(a.type) ? plugin.getGroupText(a._objects) : a.getText());
                    break;
                case "fontAlignment":
                    a.set({
                        textAlign: value
                    })
            }
            canvas.renderAll()
        }
    },
    getText: function() {
        this.getActiveProp("getText")
    },
    getGroupText: function(e) {
        var t = "";
        return e && jQuery.each(e, function(e, a) {
            t += a.text
        }), t
    },
    removeElement: function() {
        this.setActiveProp("removeElement", "rm"), plugin.resetTextArea()
    },
    removeWhiteBackground: function() {
        plugin.applyFilter(2, new f.RemoveWhite({
            threshold: 50,
            distance: 50
        }))
    },
    removeFirstText: function() {
        var e = canvas._objects;
        if (e.length > 1) {
            var t = e[e.length - 1];
            canvas.remove(t)
        }
    },
    removeSelected: function() {
        var e = canvas.getActiveObject(),
                t = canvas.getActiveGroup();
        if (t) {
            var a = t.getObjects();
            canvas.discardActiveGroup(), a.forEach(function(e) {
                canvas.remove(e)
            })
        } else
            e && canvas.remove(e);
        canvas.renderAll()
    },
    resetTextArea: function() {
        $("#areaText").val(""), plugin.setOKButton(1)
    },
    clearCanvas: function() {
        canvas.clear().renderAll();
        canvas.setBackground(null);
        canvas.setBackgroundColor(null, canvas.renderAll.bind(canvas));
        plugin.addLine();
    },
    clearAllObjects: function() {
        {
            var e = canvas.getObjects().length - 1;
            $(".thumbnail-img-canvas.active").attr("data-space")
        }
        if (e > 2) {
            for (i = e - 1; i > 1; i--)
                canvas.remove(canvas.item(i));
            plugin.layerStructure()
        }
        $("#areaText").val(""), plugin.setOKButton(1)
    },
    toJson: function() {
        //deselect all element
        canvas.deactivateAll().renderAll();
        var e = canvas.toJSON(),
                t = JSON.stringify(e);
        return t;
    },
    toSVG: function() {
        var e = canvas.toSVG();
        window.open("data:image/svg+xml;utf8," + encodeURIComponent(e))
    },
    toFrameImage: function(mimetype) {
        return new Promise(function(resolve, reject) {
            console.log('promise')
            canvas.deactivateAll().renderAll();
            var obj = {format: mimetype};
            var framesData = [];
            if ($("body").find(".animation-segment").length >= 1) {
                $(".animation-segment").each(function(index) {
                    var dataimg = $(this).find(".framedata").val();
                    var exportCanvas = document.createElement("canvas");
                    var exportCanvasInstance;
                    exportCanvasInstance = new fabric.Canvas(exportCanvas, {width: canvasDefaultWidth, height: canvasDefaultHeight});
                    exportCanvasInstance.loadFromJSON(dataimg, function() {
                        exportCanvasInstance.renderAll.bind(exportCanvasInstance)
                        var dataURL = exportCanvasInstance.toDataURL(obj)
                        framesData.push(dataURL);
                    });
                });
            }
            resolve(framesData)


        });
    },
    toImage: function(mimetype, isthumb) {
        return new Promise(function(resolve, reject) {
            var obj = {format: mimetype};
            var exportCanvas = document.createElement("canvas");
            var exportCanvasInstance;
            if (isthumb && (mimetype == 'jpeg' || mimetype == 'jpg')) {
                exportCanvasInstance = new fabric.Canvas(exportCanvas, {width: (canvasDefaultWidth * zoomRatio), height: (canvasDefaultHeight * zoomRatio)});
                exportCanvasInstance.setZoom(zoomRatio);
                obj.quality = 0.6;
            } else {
                exportCanvasInstance = new fabric.Canvas(exportCanvas, {width: canvasDefaultWidth, height: canvasDefaultHeight});
            }
            exportCanvasInstance.loadFromJSON(plugin.toJson(), function() {
                exportCanvasInstance.renderAll.bind(exportCanvasInstance)
                var dataURL = exportCanvasInstance.toDataURL(obj);
                resolve(dataURL);
            });

        });
    },
//    toImage: function(e) {
//        /*var format = e;
//         var qty = 1;
//         var m = 1;
//         var t = canvasImage.toDataURL({
//         format: format,
//         multiplier: m,
//         quality: qty
//         });*/
//
//        var t = canvas.toDataURL();
//        //return e && void 0 != e ? t : void window.open(t)
//        return t;
//    },
    toImageCopy: function(e) {
        /*var format = e;
         var qty = 1;
         var m = 1;
         var t = canvasImage.toDataURL({
         format: format,
         multiplier: m,
         quality: qty
         });*/

        var t = canvas.toDataURL();
       
//        console.log(t)
        //return e && void 0 != e ? t : void window.open(t)
        return t;
    },
    saveImgAspactRatio: function() {
        var c = plugin.toJson();
        canvasImage.clear();
        canvasImage.loadFromJSON(c, function() {
            canvasImage.deactivateAll().renderAll();
            downloadImage();
        });
    },
    zoomEachEl: function(factor) {
        canvas.setHeight(canvas.getHeight() * factor);
        canvas.setWidth(canvas.getWidth() * factor);
        if (canvas.backgroundImage) {
            // Need to scale background images as well
            var bi = canvas.backgroundImage;
            bi.width = bi.width * factor;
            bi.height = bi.height * factor;
        }
        var objects = canvas.getObjects();
        for (var i in objects) {
            var scaleX = objects[i].scaleX;
            var scaleY = objects[i].scaleY;
            var left = objects[i].left;
            var top = objects[i].top;

            var tempScaleX = scaleX * factor;
            var tempScaleY = scaleY * factor;
            var tempLeft = left * factor;
            var tempTop = top * factor;

            objects[i].scaleX = tempScaleX;
            objects[i].scaleY = tempScaleY;
            objects[i].left = tempLeft;
            objects[i].top = tempTop;

            objects[i].setCoords();
        }
        canvas.renderAll();
        canvas.calcOffset();
    },
    renderWithJson: function(t) {
//        console.log(t)
        canvas.clear();
        canvas.setBackgroundImage(0,canvas.renderAll.bind(canvas));
        canvas.loadFromJSON(t, canvas.renderAll.bind(canvas));
//        canvas.loadFromJSON(t, function() {
//            canvas.renderAll.bind(canvas);
//            //plugin.addLine();
//        });
    },
    handleHasControl: function(e) {
        canvas.getActiveObject().hasControls = e, canvas.getActiveObject().hasBorders = e
    },
    settingUpText: function() {
        var e = {}, t = canvas.getActiveObject();
        if (t) {
            if (/group/.test(t.type)) {
                default_text = plugin.getGroupText(t._objects), e = t.toObject(), e.fontSize = 20, e.angle = "0", e.fontFamily = e.objects[0].fontFamily, e.stroke = e.objects[0].stroke, delete e.type;
                var a = new fabric.Text(default_text, e);
                canvas.remove(t), canvas.add(a).renderAll(), canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1))
            } else
                / text /.test(t.type) && (default_text = t.getText(), e = t.toObject(), delete e.type, e.textAlign = "center", e.radius = 100, e.spacing = 10, e.fontSize = 20, e.fontWeight = "bold", e.angle = "0", e.stroke = e.stroke, e.strokeWidth = e.strokeWidth, plugin.createCurve(t, e, default_text));
            $("#textToCurve").hasClass("is-curve-text-selected") ? ($("#textToCurve").text("Convert to Curve"), $("#textToCurve").removeClass("is-curve-text-selected")) : ($("#textToCurve").text("Convert to Text"), $("#textToCurve").addClass("is-curve-text-selected"))
        }
    },
    settingDisbleEnable: function(e, t) {
        e.forEach(function(e) {
            e = "." + e, $(e + " :input").each(t && 1 == t ? function() {
                $(this).attr("disabled", !1)
            } : function() {
                "file" != $(this).attr("type") && $(this).attr("disabled", !0)
            })
        })
    },
    addPattern: function(e) {
        $(".canvas-inside").css("display","block");
        var t = new Image;
        t.onload = function() {
            // this is syncronous
            var f_img = new fabric.Image(t);
            f_img.setWidth(canvasDefaultWidth - 2);
            f_img.setHeight(canvasDefaultHeight - 2);
            canvas.setBackgroundImage(f_img);
            canvas.renderAll();
            plugin.addBoundingColor()
            updateModifications(true);
            $(".canvas-inside").css("display","none");
        };
        t.src = e;
//        $('#overlayLoader').show();
//        //Check if the background already selected
//        if (canvas.item(0) != undefined && canvas.item(0).foobar == "layer_background") {
//            canvas.remove(canvas.item(0));
//        }
//
//        var t = new Image;
//        t.onload = function() {
//            plugin._debug("IMAGE HAS BEEN LOADED AND RENDERED SUCCESSFULLY (addPattern)"), fabric.Image.fromURL(e, function(e) {
////                var t = canvas.getCenter();
//                var t = canvasCenterAxis;
//                e.setWidth(canvasDefaultWidth - 2);
//                e.setHeight(canvasDefaultHeight - 2);
//
//                canvas.add(e.set({
//                    top: t.top,
//                    left: t.left,
//                    originX: "center",
//                    originY: "center",
//                    scaleY: 1,
//                    scaleX: 1,
//                    hasControls: true,
//                    hasBorders: true,
//                    hasRotatingPoint: true,
//                    centeredRotation: true,
//                    lockMovementX: true,
//                      lockMovementY: true,
////                    lockScalingX: true,
////                    lockScalingY: true
//                }));sel
//
//                canvas.sendToBack(e);
//            }, {
//                foobar: "layer_background"
//            }), canvas.renderAll(), setTimeout(function() {
//                $('#overlayLoader').hide();
//                plugin.addBoundingColor(), updateModifications(true), canvas.setActiveObject(canvas.item(0))
//            }, 1e3)
//        }, t.onerror = function() {
//            alert("Got error here image not loading properly")
//        }, t.src = e

    },
    addImage: function(ot, e) {
//        console.log(e)
        $(".canvas-inside").css("display","block");
        //Check if the background already selected
        var paramType = (typeof e);
        var cs = canvasCenterAxis;
        var top = (e.hasOwnProperty("top")) ? e.top : (cs.top);
        var left = (e.hasOwnProperty("left")) ? e.left : (cs.left);
        var bgImg = (e.hasOwnProperty("bgimg")) ? e.bgimg : 0;
        var e = (e.hasOwnProperty("src")) ? e.src : e;
        var t = new Image;
        t.onload = function() {
            plugin._debug("IMAGE HAS BEEN LOADED AND RENDERED SUCCESSFULLY (addImages)"), fabric.Image.fromURL(e, function(e) {
                //ol -> means image will be with full width
                if (ot === 1) {
                    var width = (canvasDefaultWidth * imgPercentRatio) / 100;
                    var height = (canvasDefaultHeight * imgPercentRatio) / 100;
                    e.scaleToWidth(width);
                    e.scaleToHeight(height);

                }
                canvas.add(e.set({
                    top: top,
                    left: left,
                    originX: "center",
                    originY: "center",
                    opacity: (paramType === 'object') ? 0 : 1
                }));
            }, {
                foobar: "layer_image"
            }), canvas.renderAll(), setTimeout(function() {
                $(".canvas-inside").css("display","none");
                plugin.addBoundingColor(), updateModifications(true), canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1));
                setCirclePosition()
                if (paramType === 'object') {
                    var obj = {};
                    obj.top = top;
                    obj.left = left;
                    obj.bgImg = bgImg;
                    setImagePosition(obj);

                }
            }, 1e3)
        }, t.onerror = function() {
            alert("Got error here image not loading properly")
        }, t.src = e
    },
    capitalize: function(e) {
        return e.charAt(0).toUpperCase() + e.slice(1).toLowerCase()
    },
    /*setJsonCanvas: function() {
     var e = canvas.toJSON(),
     t = JSON.stringify(e),
     a = $(".thumbnail-img-canvas.active").attr("data-space");
     plugin._debug("Log signified json value " + t), arrStore[a] = t, window.localStorage.setItem(a, t), setTimeout(function() {
     window.localStorage.setItem(a + "_dataUrl", plugin.toImage(1))
     }, 500)
     },*/
//    deleteLayer: function(e) {
//        var t = canvas.item(e);
//        canvas.remove(t), setTimeout(function() {
//            plugin.layerStructure(), plugin.setJsonCanvas()
//        }, 300), $("#_layout_border").val(1), $(".select-pattern img").removeClass("alreay-selected")
//    },
    trackEvents: function() {
        var e = canvas.getObjects().length - 1;
        if (e)
            for (var t = e; t >= 0; t--)
                canvas.item(t).hasControls = canvas.item(t).hasBorders = canvas.item(t).hasRotatingPoint = canvas.item(t).centeredRotation = !1, canvas.item(t).lockMovementX = canvas.item(t).lockMovementY = !0
    },
    _debug: function(e) {
        debug && console.log(e);
    },
    hexToRGBA: function(hex, opacity) {
        hex = hex.replace('#', '');
        r = parseInt(hex.substring(0, 2), 16);
        g = parseInt(hex.substring(2, 4), 16);
        b = parseInt(hex.substring(4, 6), 16);

        result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
        return result;
    },
    cmykToRGB: function(e, t, a, n) {
        return "" == e && (e = 0), "" == t && (t = 0), "" == a && (a = 0), "" == n && (n = 0), e = parseFloat(e), t = parseFloat(t), a = parseFloat(a), n = parseFloat(n), e > 1 && (e /= 100), t > 1 && (t /= 100), a > 1 && (a /= 100), n > 1 && (n /= 100), r = Math.round((1 - e) * (1 - n) * 255), g = Math.round((1 - t) * (1 - n) * 255), b = Math.round((1 - a) * (1 - n) * 255), colorCode = plugin.rgbToHex(r, g, b), colorCode
    },
    hsl_to_rgb: function(e, t, a) {
        function n(e, t, a) {
            return 0 > a && (a += 1), a > 1 && (a -= 1), 1 / 6 > a ? e + 6 * (t - e) * a : .5 > a ? t : 2 / 3 > a ? e + (t - e) * (2 / 3 - a) * 6 : e
        }
        if (0 == t)
            rgb_r = rgb_g = rgb_b = a;
        else {
            var i = .5 > a ? a * (1 + t) : a + t - a * t,
                    o = 2 * a - i;
            rgb_r = n(o, i, e + 1 / 3), rgb_g = n(o, i, e), rgb_b = n(o, i, e - 1 / 3)
        }
        return rgb_r = Math.floor(255 * rgb_r), rgb_g = Math.floor(255 * rgb_g), rgb_b = Math.floor(255 * rgb_b), colorCode = plugin.rgbToHex(rgb_r, rgb_g, rgb_b), colorCode
    },
    rgbToHex: function(e, t, a) {
        return plugin.toHex(e) + plugin.toHex(t) + plugin.toHex(a)
    },
    toHex: function(e) {
        return e = parseInt(e, 10), isNaN(e) ? "00" : (e = Math.max(0, Math.min(e, 255)), "0123456789ABCDEF".charAt((e - e % 16) / 16) + "0123456789ABCDEF".charAt(e % 16))
    },
    setColorCode: function() {
        function e(e) {
            1 == e ? $("#inputColorCode").css("border", "1px solid #ff0000") : $("#inputColorCode").css("border", "1px solid #f1f1f1")
        }

        function t(e) {
            return /^#([A-Fa-f0-9]{3}$)|([A-Fa-f0-9]{6}$)/.test(e)
        }
        var a, n, i = $("#inputColorCode").val().toLowerCase().trim();
        switch (e(0), "" == i && e(1), i = i.replace(/\s+/g, ""), i = i.split("("), a = i[1] ? i[1].replace(")", "").split(",") : i[0], i[0]) {
            case "hsl":
                a.length < 3 && e(1);
                var o = parseFloat(a[0]),
                        r = parseFloat(a[1]),
                        l = parseFloat(a[2]);
                n = plugin.hsl_to_rgb(o, r, l);
                break;
            case "rgb":
                a.length < 3 && e(1);
                var c = parseFloat(a[0]),
                        s = parseFloat(a[1]),
                        u = parseFloat(a[2]);
                n = plugin.rgbToHex(c, s, u);
                break;
            case "cmyk":
                a.length < 4 && e(1);
                var g = parseFloat(a[0]),
                        v = parseFloat(a[1]),
                        d = parseFloat(a[2]),
                        p = parseFloat(a[3]);
                n = plugin.cmykToRGB(g, v, d, p);
                break;
            default:
                t(i[0]) ? n = i[0] : e(1)
        }
        if (n && void 0 != n) {
            var f = $("input:radio[name=_set_layer_color]:checked").val();
            "pattern" == f && (f = canvas.getObjects().length - 3), plugin.setLayerColor(f, n), plugin.setJsonCanvas()
        }
    },
    sendBackwards: function() {
        var e = canvas.getActiveObject();
        e && canvas.sendBackwards(e);
        canvas.renderAll();
        setZindexLayers();
    },
    sendToBack: function() {
        var e = canvas.getActiveObject();
        e && canvas.sendToBack(e);
        canvas.renderAll();
        setZindexLayers();
    },
    bringForward: function() {
        var e = canvas.getActiveObject();
        e && canvas.bringForward(e);
        canvas.renderAll();
        setZindexLayers();
    },
    bringToFront: function() {
        var e = canvas.getActiveObject();
        e && canvas.bringToFront(e);
        canvas.renderAll();
        setZindexLayers();
    },
    activeDrawingMode: function() {
        canvas.isDrawingMode = !0, canvas.freeDrawingBrush.color = _pen_color, canvas.freeDrawingBrush && (canvas.freeDrawingBrush.width = parseInt(_pen_width, 10) || 1)
    },
    deActiveDrawingMode: function() {
        canvas.isDrawingMode = !1, $(".activeDrawing").removeClass("activeDrawing")
    },
    setColorPencil: function(e) {
        canvas.freeDrawingBrush.color = e
    },
    setLineWidthPencil: function(e) {
        canvas.freeDrawingBrush.width = e
    },
    setClone: function() {
        var obj = canvas.getActiveObject();
        var newobj;
        $.each(obj, function() {
            if (obj.type == 'i-text') {
                newobj = obj.clone();
            } else {
                newobj = fabric.util.object.clone(obj);
                
            }
        })
        newobj.set({
            top: newobj.get('top') + 70
        });
        
        canvas.add(newobj);
        canvas.renderAll();
    },
    undo: function() {

        var lclSelectedFrame = selectedFrame - 1;
        //Exist the undo if index is index/frame is blank or undefined
        if (state[lclSelectedFrame] == undefined) {
            return;
        }
        if (mods === state[lclSelectedFrame].length) {
            modRedo = 0;
        }

        if (mods > 0) {console.log('mod')
            $(".loading-canvas-inside").css("display","block");
            var c = mods;
            //var t = state[(c - 1)];
            var t = state[lclSelectedFrame][c - 1];
            console.log("Undo-> JSON Data for index:" + lclSelectedFrame + ' object' + (c - 1));
            //plugin._debug(t);
            canvas.clear(); //Clear canvas before loading
            canvas.loadFromJSON(t, function() {
                console.log('loadddd')
//                canvas.renderAll.bind(canvas);
//                canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1));
                canvas.deactivateAll().renderAll();
                plugin.addBoundingColor()
                setTimeout(function() {
                     $(".loading-canvas-inside").css("display","none");
                    updateAnimationSection();
                }, 200);
            });
            mods--;
            modRedo = 1;
            
        }
    },
    redo: function() {
        var lclSelectedFrame = selectedFrame - 1;
        //Exist the undo if index is index/frame is blank or undfined
        if (state[lclSelectedFrame] == undefined)
            return;

        if ((mods + 1) === state[lclSelectedFrame].length) {
            modRedo = 0;
        }

        if (modRedo > 0) {
            $(".loading-canvas-inside").css("display","block");
            var c = mods;
            var t = state[lclSelectedFrame][(c + 1)];
            console.log("Redu-> JSON Data for index:" + lclSelectedFrame + ' object' + (c - 1));
            //plugin._debug(t);
            canvas.clear();
            canvas.loadFromJSON(t, function() {
                //canvas.renderAll.bind(canvas);
                //canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1));
                canvas.deactivateAll().renderAll();
                plugin.addBoundingColor()
                setTimeout(function() {
                    $(".loading-canvas-inside").css("display", "none");
                    updateAnimationSection();
                }, 200);
            });
            mods++;
        }
    },
    fitToScreen: function() {
        if (fitToScreen == false) {
            fitToScreen = true;
            timerInterval = setInterval(function() {
                var containerWidth = parseFloat(jQuery("#canvas_section_head").width() - 150);
                var containerHeight = parseFloat(jQuery("#canvas_section_head").height() - 150);
                var canvasWidth = parseFloat(canvas.width);
                var canvasHeight = parseFloat(canvas.height);
                if (canvasWidth < containerWidth && canvasHeight < containerHeight) {
                    $("#zoomIn").trigger("click");
                } else {
                    clearInterval(timerInterval);
                }

            }, 100);
        }

    },
    moveObject: function(pos, to) {
        var obj = canvas.getActiveObject();
        var newObject;
        if (!obj)
            return;
        switch (pos) {
            case 'left':
                obj.set('left', (obj.left) - to);
                break;
            case 'top':
                obj.set('top', (obj.top) - to);
                break;
            case 'right':
                obj.set('left', (obj.left) + to);
                break;
            case 'bottom':
                obj.set('top', (obj.top) + to);
                break;
        }
        obj.setCoords();
        canvas.renderAll();
    },
    toJsonFrame: function() {
        var e = canvas.toJSON(),
                t = JSON.stringify(e);
        return t;
    },
    loadCanvasFromFrame: function() {
        frameEditMode = false;
        var jsonString = "";
        jsonString = localStorage.getItem('frame_' + selectedFrame);
//        canvas.clear();
//        console.log(jsonString)

        //canvas.loadFromJSON(jsonString,function() {
            // canvas.deactivateAll().renderAll();
            // frameEditMode = true;
        //});
        
    },
    createButton: function(obj) {
        console.log("Button initialated");
        newObj = obj;
        rect = new fabric.Rect({
            width:obj.width,
            height: obj.height,
            fill: (obj.colortype == "fill") ? obj.color : "#FFFFFF",
            rx: obj.radius,
            ry: obj.radius
        });
      
        text = new fabric.IText(obj.text, {
            fill: (obj.colortype == "fill") ? "#FFFFFF" : obj.color ,
            fontWeight:"bold",
            fontSize: 13,
            fontFamily: "arial"
        });

        text.set({
            originX: 'center',
            left: rect.width/2,
            top: ((rect.getHeight() / 2) - (text.getHeight() / 2.3))
        });

        canvas.add(new fabric.Group([rect, text], {
            left: Math.floor(Math.random() * canvas.getWidth() / 4) + canvas.getWidth() / 4,
            top: Math.floor(Math.random() * canvas.getHeight() / 4) + canvas.getHeight() / 4,
            stroke: ((obj.colortype == "fill") ?'#FFFFFF' : obj.color ),
            height: obj.height,
        }));

        canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1));
        canvas.renderAll();
        plugin.addBoundingColor();
        updateModifications(true);

    },
    regularPolygonPoints: function(sideCount,radius) {
          var sweep = Math.PI * 2 / sideCount;
          var cx = radius;
          var cy = radius;
          var points = [];
          for (var i = 0; i < sideCount; i++) {
            var x = cx + radius * Math.cos(i * sweep);
            var y = cy + radius * Math.sin(i * sweep);
            points.push({x:x,y:y});
          }
          return(points);
    },
    starPolygonPoints: function(spikeCount, outerRadius, innerRadius) {
      var rot = Math.PI / 2 * 3;
      var cx = outerRadius;
      var cy = outerRadius;
      var sweep = Math.PI / spikeCount;
      var points = [];
      var angle = 0;

      for (var i = 0; i < spikeCount; i++) {
        var x = cx + Math.cos(angle) * outerRadius;
        var y = cy + Math.sin(angle) * outerRadius;
        points.push({x: x, y: y});
        angle += sweep;

        x = cx + Math.cos(angle) * innerRadius;
        y = cy + Math.sin(angle) * innerRadius;
        points.push({x: x, y: y});
        angle += sweep
      }
      return (points);
    },
    createShape: function(obj) {
        console.log("Shape initialated.");
         if (obj.text == "shape1") {
            var shape = new fabric.Path('M66.667,150v500h16.666L250,483.333L416.667,650h16.666V150H66.667z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape2") {
            var shape = new fabric.Path('M476.238,350.813c-23.083-18.317-34.853-46.729-31.481-76.01c2.203-19.153-4.35-37.955-17.979-51.58 \
                c-13.634-13.632-32.434-20.185-51.583-17.979c-29.291,3.366-57.691-8.396-76.008-31.48C287.205,158.661,269.278,150,250.001,150 \
                c-19.278,0-37.206,8.661-49.188,23.763c-18.317,23.082-46.719,34.848-76.003,31.483c-19.153-2.212-37.955,4.344-51.585,17.975 \
                c-13.632,13.627-20.185,32.429-17.981,51.585c3.371,29.275-8.397,57.688-31.485,76.008C8.661,362.798,0,380.725,0,400 \
                c0,19.275,8.66,37.202,23.761,49.188c23.085,18.319,34.853,46.729,31.482,76.01c-2.204,19.153,4.349,37.955,17.978,51.58 \
                c13.634,13.633,32.45,20.188,51.584,17.979c29.278-3.369,57.69,8.396,76.008,31.48c11.981,15.102,29.909,23.765,49.188,23.765 \
                c19.277,0,37.204-8.661,49.187-23.764c18.319-23.081,46.722-34.846,76.003-31.483c19.154,2.202,37.955-4.344,51.586-17.975 \
                c13.632-13.627,20.185-32.43,17.981-51.584c-3.371-29.276,8.398-57.69,31.485-76.009c15.097-11.983,23.758-29.911,23.758-49.188 \
                S491.339,362.798,476.238,350.813z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape3") {
            var shape = new fabric.Path('M499.444,355.623c0-25.887-3.142-50.294-9.431-73.223c-6.288-22.929-13.129-41.234-20.524-54.917 \
                c-7.396-13.683-15.716-26.072-24.962-37.166c-9.242-11.094-15.899-18.491-19.969-22.188c-4.075-3.706-7.59-6.294-10.539-7.766 \
                l-12.204-8.875l-13.313,8.875c-20.708,14.053-41.787,21.079-63.237,21.079c-24.407,0-44.745-6.657-61.019-19.97l-14.422-11.094 \
                l-13.313,11.094c-17.011,13.313-37.72,19.97-62.128,19.97c-21.449,0-42.528-7.026-63.237-21.079l-13.313-8.875l-12.204,8.875 \
                c-3.698,2.219-7.766,5.362-12.204,9.43c-4.438,4.068-11.28,11.834-20.524,23.298c-9.245,11.464-17.381,23.853-24.408,37.166 \
                c-7.026,13.313-13.313,31.064-18.86,53.252c-5.547,22.188-8.69,45.856-9.43,71.003c-0.74,4.438,0.555,14.237,3.883,29.4 \
                s10.354,35.687,21.079,61.573c10.725,25.887,24.593,51.403,41.604,76.551c17.011,25.147,41.604,49.739,73.777,73.777 \
                c32.173,24.038,68.599,41.973,109.279,53.808c32.543-8.876,62.127-21.079,88.754-36.611c26.626-15.532,48.446-31.805,65.456-48.815 \
                c17.012-17.011,31.806-35.132,44.376-54.361c12.574-19.23,22.188-37.166,28.846-53.808c6.656-16.642,11.835-32.543,15.531-47.706 \
                c3.699-15.162,5.918-26.997,6.657-35.501C500.186,364.313,500.186,358.582,499.444,355.623z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape4") {
            var shape = new fabric.Path('M444.641,318.776c2.813-35.865,13.713-68.565,32.7-98.101L404.556,150 \
                c-23.206,19.691-49.578,30.239-79.113,31.646c-27.427,2.813-53.095-2.109-77.004-14.768c-24.614,11.955-50.282,16.878-77.004,14.768 \
                c-28.13-2.109-53.094-11.604-74.895-28.481l-72.785,69.62c17.581,30.942,27.426,62.94,29.536,95.992 \
                c0.704,15.472-3.868,36.92-13.713,64.346c-5.625,14.768-9.845,27.778-12.658,39.03c-2.11,11.252-3.516,20.042-4.22,26.371 \
                c-0.704,28.832,7.735,54.853,25.316,78.059c13.362,17.581,35.865,36.568,67.511,56.962c33.755,16.878,59.775,27.427,78.059,31.646 \
                c2.11,0.704,4.747,1.934,7.912,3.692s5.801,2.988,7.911,3.692l14.768,6.329c11.955,7.032,20.042,14.064,24.262,21.097 \
                c4.923-7.735,13.009-14.768,24.261-21.097c9.143-3.517,15.822-6.329,20.043-8.439c7.032-2.813,10.899-4.57,11.603-5.274 \
                c4.22-1.405,9.494-3.516,15.823-6.329l23.207-8.438c17.581-5.625,30.59-11.252,39.029-16.878 \
                c30.239-20.394,52.391-39.029,66.455-55.907c17.582-23.206,26.372-49.577,26.372-79.113c-1.406-13.362-7.385-34.459-17.933-63.292 \
                C447.453,356.4,443.234,334.248,444.641,318.776z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape5") {
            var shape = new fabric.Path('M459,0H51C22.95,0,0,22.95,0,51v459l102-102h357c28.05,0,51-22.95,51-51V51C510,22.95,487.05,0,459,0z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape6") {
            var shape = new fabric.Path('M110.333,645.546c-1.228,0-2.47-0.304-3.591-0.909c-2.454-1.318-3.984-3.895-3.984-6.667V530.03 \
                C36.439,495.546,0,435.167,0,359.515c0-116.909,107.47-205.061,250-205.061s250,88.151,250,205.061 \
                c0,116.894-107.47,205.045-250,205.045c-4.682,0-9.273-0.258-13.879-0.5l-0.303-0.015L114.5,644.288 \
                C113.242,645.121,111.788,645.546,110.333,645.546z M250,169.606c-133.879,0-234.849,81.652-234.849,189.909 \
                c0,50.348,17.091,118.818,98.53,159.076c2.576,1.272,4.212,3.909,4.212,6.788v98.5l111.651-73.849 \
                c1.364-0.909,3.015-1.439,4.576-1.242l2.818,0.151c4.333,0.242,8.651,0.47,13.045,0.47c133.878,0,234.848-81.637,234.848-189.894 \
                S383.879,169.606,250,169.606z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape7") {
            var shape = new fabric.Path('M0,152.458v347.402h96.521l-0.035,147.678l147.98-147.678H500V152.458H0z M109.585,615.952l0.027-129.174 \
                H13.092V165.532h473.789V486.78H239.052L109.585,615.952z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape8") {
            var shape = new fabric.Path('M249.989,623.727c-487.725-266.056-134.987-596.826,0-374.058C384.998,26.9,737.757,357.671,249.989,623.727z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape9") {
            var shape = new fabric.Path('M471.317,305.426c-12.79-64.341-58.526-105.814-117.441-105.814c-32.945,0-68.604,13.566-103.488,39.147 \
                c-35.271-26.356-70.93-39.922-104.264-39.922c-58.915,0-105.039,41.86-117.442,106.977 \
                C10.078,402.326,69.768,530.62,244.574,600.388c1.938,0.775,3.876,1.163,5.814,1.163c1.938,0,3.876-0.388,5.813-1.163 \
                C430.62,529.069,489.923,401.162,471.317,305.426L471.317,305.426z M250.388,568.604C96.899,505.039,43.798,394.186,59.69,311.628 \
                c9.302-49.225,43.411-81.396,86.434-81.396c29.07,0,61.628,13.954,94.186,40.698c5.814,4.651,13.954,4.651,19.768,0 \
                c32.558-25.969,64.729-39.535,93.798-39.535c43.023,0,76.744,31.395,86.435,80.232C456.201,393.411,403.488,503.876,250.388,568.604 \
                L250.388,568.604z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape10") {
            var shape = new fabric.Path('M59.092,45.862l20.443-13.101L59.788,30.67L65.12,7.695L51.118,22.186l-3.894-16.37l-5.894,15.278 \
                L25.318,0.461l5.06,25.859l-14.264-3.171c0,0,4.321,8.283,6.74,13.269L0,34.82l20.679,12.089l-5.939,6.115l9.608-1.756l-6.812,24.1 \
                l20.259-16.933l5.241,13.09l5.41-10.47l17.43,18.02l-8.699-24.141l19.14,1.938L59.092,45.862z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape11") {
            var shape = new fabric.Path('M467.662,427.178c-1.624-3.894-5.415-6.436-9.622-6.436H343.933V160.414c0-5.748-4.665-10.414-10.413-10.414H166.908 \
                c-5.748,0-10.414,4.666-10.414,10.414v260.331H41.95c-4.207,0-7.997,2.54-9.622,6.414c-1.604,3.895-0.729,8.372,2.249,11.351 \
                L242.32,646.939c1.958,1.957,4.603,3.062,7.373,3.062c2.77,0,5.416-1.104,7.373-3.041l208.348-208.431 \
                C468.392,435.55,469.286,431.073,467.662,427.178z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape12") {
            var shape = new fabric.Path('M336.694,229.501c-6.812-7.05-18.148-7.05-25.198,0c-6.813,6.812-6.813,18.148,0,24.944l127.771,127.771 \
                H17.641C7.812,382.232,0,390.044,0,399.873c0,9.828,7.812,17.895,17.641,17.895h421.626L311.496,545.3 \
                c-6.813,7.05-6.813,18.403,0,25.199c7.05,7.05,18.402,7.05,25.198,0L494.713,412.48c7.05-6.813,7.05-18.149,0-24.945 \
                L336.694,229.501z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape13") {
            var shape = new fabric.Path('M0,388.095h500v23.809H0V388.095z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape14") {
            var shape = new fabric.Path('M0,150v500h500V150H0z M483.333,633.333H16.667V166.667h466.667V633.333z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape15") {
            var shape = new fabric.Path('M15.858,650V150l468.282,250.009L15.858,650z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape16") {
            var shape = new fabric.Path('M59.895,58.531l-29-58c-0.34-0.678-1.449-0.678-1.789,0l-29,58c-0.155,0.31-0.139,0.678,0.044,0.973 \
                C0.332,59.798,0.654,59.978,1,59.978h58c0.347,0,0.668-0.18,0.851-0.474C60.033,59.209,60.05,58.841,59.895,58.531z M2.618,57.978 \
                L30,3.214l27.382,54.764H2.618z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape17") {
            var shape = new fabric.Path('M50.213,650c-14.16,0-25.641-11.48-25.641-25.642V175.64c0-14.161,11.481-25.64,25.641-25.64l416.667,230.768 \
                c0,0,19.23,19.231,0,38.463C447.649,438.461,50.213,650,50.213,650z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape18") {
            var shape = new fabric.Path('M30,0C13.458,0,0,13.458,0,30s13.458,30,30,30s30-13.458,30-30S46.542,0,30,0z M30,58C14.561,58,2,45.439,2,30 \
                S14.561,2,30,2s28,12.561,28,28S45.439,58,30,58z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape19") {
            var shape = new fabric.Path('M409.133,109.203c-19.607-33.592-46.205-60.189-79.798-79.796C295.735,9.801,259.058,0,219.273,0 \
                c-39.781,0-76.47,9.801-110.063,29.407c-33.595,19.604-60.192,46.201-79.8,79.796C9.801,142.8,0,179.489,0,219.267 \
                c0,39.78,9.804,76.462,29.407,110.062c19.607,33.592,46.204,60.189,79.799,79.798c33.597,19.605,70.283,29.407,110.063,29.407 \
                c39.78,0,76.47-9.802,110.065-29.407c33.593-19.603,60.189-46.206,79.795-79.798c19.604-33.596,29.403-70.284,29.403-110.062 \
                C438.533,179.485,428.732,142.795,409.133,109.203z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape20") {
            var shape = new fabric.Path('M360.719,352.99c0,4.269-3.459,7.727-7.727,7.727H7.722c-4.263,0-7.722-3.458-7.722-7.727V7.724 \
                c0-4.264,3.459-7.722,7.722-7.722h345.271c4.26,0,7.717,3.458,7.717,7.722V352.99H360.719z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        } else if (obj.text == "shape21") {
            var shape = new fabric.Path('M433.114,5.424C429.497,1.807,425.215,0,420.266,0H18.276C13.324,0,9.041,1.807,5.425,5.424 \
                C1.808,9.04,0.001,13.322,0.001,18.271v401.991c0,4.947,1.807,9.232,5.424,12.848c3.619,3.613,7.902,5.428,12.851,5.428h401.99 \
                c4.949,0,9.23-1.814,12.848-5.428c3.615-3.613,5.422-7.898,5.422-12.848V18.271C438.534,13.319,436.731,9.04,433.114,5.424z');    
            var scale = 100 / shape.width;
            shape.set({ 
                left: 0,
                top: 0,
                scaleX: scale,
                scaleY: scale, 
                fill: obj.color,
            });
        }
        obj.text = "";
        var shapeText = new fabric.IText(obj.text);

        canvas.add(new fabric.Group([shape, shapeText], {
            left: Math.floor(Math.random() * canvas.getWidth() / 4) + canvas.getWidth() / 4,
            top: Math.floor(Math.random() * canvas.getHeight() / 4) + canvas.getHeight() / 4,
            stroke: ((obj.colortype == "fill") ?'#FFFFFF' : obj.color ),
            height: obj.height,
        }));

        canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1));
        canvas.renderAll();
        plugin.addBoundingColor();
        updateModifications(true);

    },
    checkCanvasLength: function() {
        return canvas.getObjects().length;
    },
    makeObjectActive: function(index) {
        canvas.setActiveObject(canvas.item(index));
    },
    getTextWidth: function(x) {
        console.log(x);
    }
};
!function() {
    //plugin.addLine();
    updateModifications(false);
    removeLocalStroage('frame_');
    //App controller for mnaging through Angular js
    app.controller('toolTipController', function($scope, $http, facebookService, fileUpload) {
        canvas.on("object:selected", function() {
            
            var e = canvas.getActiveObject();
            plugin.addBoundingColor();
            // To hide the wheelcolorpicker on selected
            $('.jQWCP-wWidget').css('display', 'none');
            $(".xs-popover-more-layer").css('display', 'none');
           
            if (!e)
                return;

            if ( e && "line" == e.type) {
                return false;
            }
            var activeObj = canvas.getActiveObject();
            $scope.canvasObjTypeCng = e.type;
            window.anonymousLocation = 1;
            $scope.$apply();
            // set the text editor circle position when it moves
            setCirclePosition();
            // Check the editor tool option 
            // if defaut is selected then the top editor option will be visible 
            // else circle option will be visible
            if ($('#editor_tool').val() == 0) {
                $("#section_toolkit_menu").fadeIn(500);
            } else {
                $(".text-editor-circle").fadeIn(500);

            }
            //positionToolBar(e);
            if (plugin._debug("selected object"), e && "i-text" == e.type) {
                setCirclePosition();
                updateToolTip(e);
                updateCircleToolTip(e);
            } else if (plugin._debug("selected object"), e && "image" == e.type) {
                setCirclePosition();
                $('.tblr-inner-data').removeClass("text-editor-show");
                if ($('#editor_tool').val() == 0) {
                    $("#section_toolkit_menu").fadeIn(500);
                } else {
                    $(".text-editor-circle").fadeIn(500);
                }
            } else if (plugin._debug("selected object"), e && "group" == e.type) {
                setCirclePosition();
                updateButtonToolTip(e);
            } else if (plugin._debug("selected object"), e && "i-text" == e._objects[1].type) {
                setCirclePosition();
                updateButtonToolTip(e);
            }
            setZindexLayers();
        });
    });

    canvas.on("mouse:up", function() {
        var e = canvas.getActiveObject();
        if (!e)
            return;
        plugin.addBoundingColor();
        updateAnimationSection();
    }), canvas.on("selection:cleared", function() {

        resetToolTip();
        setZindexLayers();
        if ($('#editor_tool').val() == 0) {
            $("#section_toolkit_menu").fadeOut(500);
        } else {
            $(".text-editor-circle").fadeOut(500);
        }
        $('.circle-inner-content').css('display', 'none')
        $('.count-layers-obj ul li a').removeClass('active')
        updateAnimationSection();
    });
    /*canvas.on('object:moving', function(e) {
     var obj = e.target;
     //setting up tool tip
     //positionToolBar(obj);
     obj.setCoords();
     if (plugin._debug("moving object"), obj && "image" === obj.type) {
     return false;
     }
     
     // if object is too big ignore
     if (obj.currentHeight > obj.canvas.height || obj.currentWidth > obj.canvas.width) {
     return;
     }
     obj.setCoords();
     // top-left  corner
     if (obj.getBoundingRect().top < 0 || obj.getBoundingRect().left < 0) {
     obj.top = Math.max(obj.top, obj.top - obj.getBoundingRect().top);
     obj.left = Math.max(obj.left, obj.left - obj.getBoundingRect().left);
     
     }
     // bot-right corner
     if (obj.getBoundingRect().top + obj.getBoundingRect().height > obj.canvas.height || obj.getBoundingRect().left + obj.getBoundingRect().width > obj.canvas.width) {
     obj.top = Math.min(obj.top, obj.canvas.height - obj.getBoundingRect().height + obj.top - obj.getBoundingRect().top);
     obj.left = Math.min(obj.left, obj.canvas.width - obj.getBoundingRect().width + obj.left - obj.getBoundingRect().left);
     }
     });*/

    canvas.on("object:added", function(e) {
        objectModified = true;
        plugin.addBoundingColor()
        updateAnimationSection();
    });

    canvas.on("object:modified", function() {
        objectModified = true;
        updateModifications(true);
        setCirclePosition();
    });

    canvas.on('text:changed', function(e) {
        updateModifications(true);
    });

    canvas.on('object:scaling', function(e) {
        var obj = e.target;
        if (obj.get('foobar') == "layer_background") {
            if (obj.get('scaleX') < 1) {
                obj.setScaleX(1)
            } else if (obj.get('scaleY') < 1) {
                obj.setScaleY(1)
            }
        }
    });

    $(document).on("click", "li.colorOption a", function() {
        plugin._debug("CANVAS BACKGROUND COLOR : " + $(this).attr("data-color"));
        plugin.setCanvasBackground($(this).attr("data-color"));
        updateAnimationSection();
    });
    
    // text Color picker
    $(document).on("click", "#textColorPickerId", function() {
        $(".xs-popover-more-layer").removeClass("open-pop");
        if ($(this).hasClass("picker-open")) {
            $('#color-plates-text').fadeOut(500);
            $(this).removeClass("picker-open");
        } else {
            $('#color-plates-text').fadeIn(500);
            $(this).addClass("picker-open");
            $(".xs-popover-more-layer").fadeOut('300');
        }
    });
    // button Color picker
    $(document).on("click", "#buttonColorPickerId", function() {
        $(".xs-popover-more-layer").removeClass("open-pop");
        if ($(this).hasClass("picker-open")) {
            $('#color-plates-button').fadeOut(500);
            $(this).removeClass("picker-open");
        } else {
            $('#color-plates-button').fadeIn(500);
            $(this).addClass("picker-open");
            $(".xs-popover-more-layer").fadeOut('300');
        }
    });

    $(document).on("click", "#imageColorPickerId", function() {
        $(".mx-page-more-layer>a").removeClass("open-pop");
        if ($(this).hasClass("picker-open")) {
            $('#color-plates-image-layer').fadeOut(500);
            $(this).removeClass("picker-open");
        } else {
            $('#color-plates-image-layer').fadeIn(500);
            $(this).addClass("picker-open");
            $(".xs-popover-more-layer").fadeOut('300');
        }
    });

    $(document).on("click", "#color-plates-text li.colorOption_text a", function() {
        plugin._debug("CANVAS Text COLOR : " + $(this).attr("data-color"));
        var colorOptChoose = $('input:radio[name=textcolor_choose_for]:checked').val();
        switch (colorOptChoose) {
            case 'text':
                plugin.setTextColor($(this).attr("data-color"));
                $('.xs_canvas_stroke_width').css('display', 'none')
                break;
            case 'stroke':
                var color = $(this).attr("data-color");
                plugin.setActiveProp("setStroke", $(this).attr("data-color"));
                $('.xs_canvas_stroke_width').css('display', 'block')
                $("input[name=stroke_choose_for]:radio").click(function() {
                    if ($('input[name=stroke_choose_for]:checked').val() == "thin") {
                        plugin.setActiveProp("setThinStroke", color);
                        return;
                    }
                    else if ($('input[name=stroke_choose_for]:checked').val() == "medium") {
                        plugin.setActiveProp("setMediumStroke", color);
                        return;
                    }
                    else if ($('input[name=stroke_choose_for]:checked').val() == "thick") {
                        plugin.setActiveProp("setThickStroke", color);
                        return;
                    }
                });
                break;
            case 'gradient':
                plugin.setActiveProp('setGradient', $(this).attr("data-color"));
                $('.xs_canvas_stroke_width').css('display', 'none');
                break;
        }

        $("#textColorPickerId").parent().css("background-color", $(this).attr("data-color"));
    });

    $(document).on("click", "#color-plates-button li.colorOption_button a", function() {
        plugin._debug("CANVAS button COLOR : " + $(this).attr("data-color"));
        var buttonOptChoose = $('input:radio[name=textbutton_choose_for]:checked').val();
        switch (buttonOptChoose) {
            case 'text':
                plugin.setButtonTextColor($(this).attr("data-color"));
                break;
            case 'button':
                plugin.setButtonColor($(this).attr("data-color"));
                break;
            case 'shape':
                plugin.setShapeColor($(this).attr("data-color"));
                break;
        }
        $("#buttonColorPickerId").parent().css("background-color", $(this).attr("data-color"));

    });
   
    // Add text when click on left side text options
    $(document).on("click", ".textControls ul li a.add", function() {
        plugin._debug("TEXT COLOR");
        var obj = JSON.parse($(this).attr("data-value"));
        plugin.addText(obj);

    });
    // Add background when click on left side background images section
    $(document).on("click", ".layoutbackgroundDiv ul li a", function() {
        plugin._debug("BACKGROUND LAYOUT SET");
        plugin.addPattern($(this).attr('data-img'));
        if ($(this).attr("data-color") !== '' && $(this).attr("data-color") !== undefined) {
            plugin.setCanvasBackground($(this).attr("data-color"));
        }
    });
    //To delete the selected object on the canvas
    $(document).on("click", "#deleteLayerId", function() {
        $('.text-options-circle').removeClass('active');
        $('.text-editor-circle .delete-button').addClass('active')
        plugin._debug("CANVAS Delete Active object : ");
        $(".xs-popover-more-layer").removeClass("open-pop");
        plugin.removeSelected();
        setZindexLayers();
        updateModifications(true);
    });

    //Deselect all element when body click found.
    $('.canvas-container-main').click(function(evt) {
        $('#buttonmodal').css('display', 'none');
        //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
        if ($(evt.target).closest('canvas').length) {
            return;
        } else if ($(evt.target).closest('#section_toolkit_menu').length) {
            return;
        } else if ($(evt.target).closest('.text-editor-circle').length) {
            return;
        }
        else {
            canvas.deactivateAll().renderAll();
            if ($('#editor_tool').val() == 0) {
                $("#section_toolkit_menu").fadeOut(500);
            } else {
                $(".text-editor-circle").fadeOut(500);
            }
            resetToolTip();
        }
    });
    // Add image when click on left side images section 
    $(document).on("click", "#manu_images ul li a", function() {
       $(".canvas-inside").css("display","block");
        plugin.addImage(1, $(this).attr("image-src"));
    });
    // Add uploaded image when click on left side uploaded section 
    $(document).on("click", "#menu_upload .itemli a", function() {
        $(".canvas-inside").css("display","block");
        var src = $(this).attr('image-src');
        plugin.addImage(1, src);
    });

    //This code is not used now ,Now the href is set to show the clicked template 
    $(document).on("click", "#menu_layout .uim_image_fill", function() {
        $(".canvas-inside").css("display","block");
        $.ajax({
            url: '/gettemplate',
            data: {tid: $(this).attr("source-id"), _token: $("#oeditorForm input[name='_token']").val()},
            method: 'post',
            success: function(r) {
                var rs = jQuery.parseJSON(r);
//                console.log(rs.frames[0].frame_data);
                plugin.renderWithJson(rs.frames[0].frame_data);
                setTimeout(function() {
                    updateModifications(true);
                    updateAnimationSection();
                    $(".canvas-inside").css("display","none");
                }, 500);
                 
            }
        });

    });

    $(document).on("click", ".xs-menu-bar-tag a:not(.disable),.xs-popover-more-layer li a:not(.disable)", function() {
        var arg = $(this).attr("data-attr");
        switch (arg) {
            case 'bold':
                plugin.yontWeight(arg);
                //settingMenuChecked("menu_select", this);
                break;
            case 'italic':
                plugin.setFontStyle(arg);
                //settingMenuChecked("menu_select", this);
                break;
            case 'uppercase':
                plugin.setTextTransform(arg);
                //settingMenuChecked("menu_select", this);
                break;
            case 'left':
                plugin.setAlignment(arg);
                if ($('#editor_tool').val() == 0) {
                    $("#section_toolkit_menu").css('display', 'block');
                } else {
                    $(".text-editor-circle").css('display', 'block');
                }

                //settingMenuChecked("menu_radio", this);
                break;
            case 'center':
                plugin.setAlignment(arg);
                if ($('#editor_tool').val() == 0) {
                    $("#section_toolkit_menu").css('display', 'block');
                } else {
                    $(".text-editor-circle").css('display', 'block');
                }

                //settingMenuChecked("menu_radio", this);
                break;
            case 'right':
                plugin.setAlignment(arg);
                if ($('#editor_tool').val() == 0) {
                    $("#section_toolkit_menu").css('display', 'block');
                } else {
                    $(".text-editor-circle").css('display', 'block');
                }

                //settingMenuChecked("menu_radio", this);
                break;
            case 'copy':
                plugin.setClone();
                break;
            case 'move_forward':
                plugin.bringForward();
                break;
            case 'text_shadow':
                plugin.setActiveProp('setShadow', '#999999');
                break;
            case 'text_gradient':
                //plugin.setActiveProp('setGradient','#999999');
                break;
            case 'move_backward':
                plugin.sendBackwards();
                break;
            case 'transparancy':
                $("#textColorPickerId").removeClass("picker-open");
                $("#buttonColorPickerId").removeClass("picker-open");
                $("#color-plates-text,.txt-spacing-slide-bx ul").fadeOut();
                $("#color-plates-button,.txt-spacing-slide-bx ul").fadeOut();
                $(".txt-spacing-slide-bx ul").removeClass("open-pop");

                if ($(".transparent-slide-bx ul").hasClass("open-pop")) {
                    $(".transparent-slide-bx .xs-popover-more-layer").fadeOut();
                    $(".transparent-slide-bx ul").removeClass("open-pop");
                } else {
                    $(".transparent-slide-bx .xs-popover-more-layer").fadeIn();
                    $(".transparent-slide-bx ul").addClass("open-pop");
                }
                $("#range-slider-filter").foundation({
                    slider: {
                        on_change: function() {
                            var range = parseInt($('#range-slider-filter').attr('data-slider'));
                            plugin.setFontOpacity(range);
                        }
                    }
                });
                break;
            case 'text-spacing':
                $("#textColorPickerId").removeClass("picker-open");
                $("#buttonColorPickerId").removeClass("picker-open");
                $("#color-plates-text,.transparent-slide-bx ul").fadeOut();
                $("#color-plates-button,.transparent-slide-bx ul").fadeOut();
                $(".transparent-slide-bx ul").removeClass("open-pop");

                if ($(".txt-spacing-slide-bx ul").hasClass("open-pop")) {
                    $(".txt-spacing-slide-bx .xs-popover-more-layer").fadeOut();
                    $(".txt-spacing-slide-bx ul").removeClass("open-pop");
                } else {
                    $(".txt-spacing-slide-bx .xs-popover-more-layer").fadeIn();
                    $(".txt-spacing-slide-bx ul").addClass("open-pop");
                }
                $("#range-slider-line-height").foundation({
                    slider: {
                        on_change: function(obj) {
                            var range = parseInt($('#range-slider-line-height').attr('data-slider'));
                            var rangeF = parseFloat($('#range-slider-line-height').attr('data-slider'));
                                if (range < 1) {
                                    plugin.setLineHeight(range);
                                } else {
                                    plugin.setLineHeight(rangeF);
                                }  
                        }
                    }
                });
                break;
            case 'flip_horizonal':
                plugin.setFlipLayerX("X");
                //settingMenuChecked("menu_select", this);
                break;
            case 'flip_vertical':
                plugin.setFlipLayerY("Y");
                //settingMenuChecked("menu_select", this);
                break;
        }
    });

    $(document).on("click", "#zoomIn", function() {
        var zoomCaptionValue = parseInt($("#zoomTextCaption").attr('data-value'));
        if (zoomCaptionValue >= 200 || window.isDisable == 1) {
            return false;
        }
        var zr = (zoomCaptionValue >= 100) ? (zoomRatio + (zoomRatio / 2)) : zoomRatio;
        var izr = (zoomCaptionValue >= 100) ? (zoomCaptionValue + (increseZoomlabelRatio * 2)) : zoomCaptionValue + increseZoomlabelRatio;
        zoomStart = zoomStart + zr;

        plugin.setZoom(zoomStart);
        $("#zoomTextCaption").html(izr + '%');
        $("#zoomTextCaption").attr('data-value', izr);
        centralized();

    });

    $(document).on("click", "#zoomOut", function() {
        var zoomCaptionValue = parseInt($("#zoomTextCaption").attr('data-value'));

        if (zoomCaptionValue <= 25 || window.isDisable == 1) {
            return false;
        }
        var zr = (zoomCaptionValue >= 150) ? (zoomRatio + (zoomRatio / 2)) : zoomRatio;
        var izr = (zoomCaptionValue >= 150) ? (zoomCaptionValue - (increseZoomlabelRatio * 2)) : zoomCaptionValue - increseZoomlabelRatio;

        zoomStart = zoomStart - zr;
        if (zoomStart >= 0) {
            plugin.setZoom(zoomStart);
        }

        $("#zoomTextCaption").html(izr + '%');
        $("#zoomTextCaption").attr('data-value', izr);
        centralized();
        fitToScreen = false;
    });

//    $(".project-save-template").on("click", function() {
//        $("#oeditorForm #_canvas_data_json").val(plugin.toJson());
//        $("#_user_canvas_title").val($('.project-name-cls').text());
//        $("#_canvas_img_souce").val(plugin.toImage('png'));
//        $.ajax({
//            url: '/template',
//            data: $("#oeditorForm").serialize(),
//            method: 'post',
//            success: function(r) {
//                var res = jQuery.parseJSON(r);
//                if (res.status == 'success') {
//                    setTimeout(function() {
//                        window.location.href = '/template';
//                    }, 1000);
//                    //window.location.reload();
//                } else {
//                    alert("Template Not Saved Successfully ");
//                }
//            }
//        });
//    });

    $("body").on("click", ".animation-segment-img", function() {
        canvas.setBackgroundImage(0);
        selectedFrame = parseInt($(this).closest(".animation-segment").attr("data-count"));
        plugin._debug("on select frame selectedFrame " + selectedFrame);
        $(".animation-segment-img").removeClass("frame-active");
        $(this).addClass("frame-active");
       plugin.loadCanvasFromFrame();

        //Reset Redo and Undo button
        mods = ((state[selectedFrame - 1]).length - 1);
        modRedo = 0;
    });

    //To remove the animation-segments
    $("body").on("click", ".animation-segment a.close", function() {
        var frameCount = $(".animation-segment").length;
        var deletedFrame = $(this).closest(".animation-segment");
        if (frameCount === 2) {
            $('.animation-segment').find('.closespan').css('display', 'none')
        } else if (frameCount > 1) {
            $('.animation-segment').find('.closespan').css('display', 'block');
        }
        deletedFrame.remove(); //Remove current frame
        selectedFrame = (frameCount - 1);
        plugin._debug("on close selectedFrame: " + selectedFrame);
        rearrangeFrames(); //Rearrange frames and then set the options below
        reArrangeLocalStorage(deletedFrame.data("count")); //Rearrange local storage
        reArrangeUndoStore(deletedFrame.data("count")); //Rearrange local storage

        //-Setting up options
        $(".animation-segment .animation-segment-img").removeClass("frame-active");
        $('.animation-segment[data-count="' + selectedFrame + '"]').find('.animation-segment-img').addClass('frame-active');
        plugin.loadCanvasFromFrame();
    });

    //- When there are more than 1 frame on the animation panel,  each frame can be deleted until there is only 1 frame left, which by then, no X is shown. 
    if ($(".animation-segment").length > 1) {
        $('.animation-segment').find('.closespan').css('display', 'block');
    } else if ($(".animation-segment").length === 1) {
        $('.animation-segment').find('.closespan').css('display', 'none');
    }
    //- Animation bar section start
    $(".uim-animation-bar-addnew a").on("click", function() {
        var frameCount = $(".animation-segment").length;
        $('.animation-segment').find('.closespan').css('display', 'block');
        var prevDom = $(".animation-segment:last-child");
        var delayTime = $(prevDom).find("input.frametime").val();
        var dom = $(".animation-segment:last-child").clone().appendTo(".uim-animation-bar-segment");
        selectedFrame = (frameCount + 1);
        $(dom).attr("data-count", (selectedFrame));
        //Unselect all active and make the last one
        $(".animation-segment .animation-segment-img").removeClass("frame-active");
        $(dom).find(".animation-segment-img").addClass("frame-active");
        $(dom).find("input.frametime").val(delayTime);

        //set localstorage for the first time just copy previous json
        var prevFrame = localStorage.getItem('frame_' + frameCount);
        if (prevFrame) {
            localStorage.setItem('frame_' + selectedFrame, prevFrame);
        }
        plugin._debug("on add selectedGrame: " + selectedFrame);

        //Push a blank array into slate array, for undo and redo function
        if (state[selectedFrame - 1] == undefined) {
            state.push([]);
            plugin._debug(state);
        }


    });

//    $("body").on("click",".project-finalize-canvas",function(){
//        alert();
//        //plugin.saveImgAspactRatio('jpg');
//        $.ajax({
//            url: '/export',
//            data: $("#animation-bar-form").serialize(),
//            method: 'post',
//            success: function(r) {
//                console.log(r);
//            }
//        });
//    });

}();
