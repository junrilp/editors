<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="_token" content="{{ csrf_token()}}" />
        <title>Online Editor</title>

        <script src="<?php echo url(); ?>/js/modernizr.js"></script>
        <script src="<?php echo url(); ?>/js/jquery.js"></script>
        <script src="<?php echo url(); ?>/js/jquery-ui.js"></script>
        <script src="<?php echo url(); ?>/js/angular.min.js"></script>
        <script src="<?php echo url(); ?>/js/bootstrap-colorpicker-module.js"></script>
        <script src=" <?php echo url(); ?>/js/angular-animate.min.js"></script>
        <script src="<?php echo url(); ?>/js/app-controller.js"></script>

        <!--<link rel="stylesheet" href="css/perfect-scrollbar.css" />-->
        <link rel="stylesheet" href="<?php echo url(); ?>/css/foundation.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/colorpicker.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/app.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/app.animation.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/spectrum.css" type="text/css" />
    </head>

    <body ng-app="editorApp">
        <style>

            body{
                overflow: auto;
            }
            .grid-item{
                width:20%;
                padding:2px;
            }
            .grid{
                width: 1200px;
                margin: 0 auto;
                /*                position: relative;*/
                clear: both;
            }




        </style>
        <?php if (empty($templateSizeLayouts)) { ?>
            <div class="tabs-content" ng-controller="leftNavBarController">
                <section role="tabpanel" aria-hidden="true" class="content active" id="tab1">
                    <div class="text-center">
                        <div class="started-editor">
                            <h4>No Sizes Choosen </h4>
                            <h5>Please Contact to administrator</h5>
                            <a href="/mockup3d">Create your own canvas size</a>
                        </div>
                    </div>
                </section>
            </div>

        <?php } else { ?>
            <div class="tab-navbar">
                <ul class="tabs editor-tabs" data-tab role="tablist">
                    <li class="tab-title active" role="presentation">
                        <a href="#tab1" role="tab" tabindex="0" aria-selected="true" aria-controls="tab1">START A NEW DESIGN</a>
                    </li>
                    <li id="designfromtemplate" class="tab-title" role="presentation">
                        <a href="#tab2" role="tab" tabindex="1" aria-selected="false" aria-controls="tab2">DESIGN FROM TEMPLATES</a>
                    </li>
                </ul>
            </div>
            <div class="tabs-content" ng-controller="leftNavBarController">
                <section role="tabpanel" aria-hidden="true" class="content active" id="tab1">
                    <div class="text-center">
                        <div class="started-editor">
                            <h4>Click on a size to get started</h4>
                            <div class="large-12 columns button-section">
                                <ul>
                                    <?php foreach ($templateSizeLayouts['dimensions'] as $template) { ?>
                                        <li>
                                            <a class="button radius" source-type="1" data-height="<?php echo $template->height; ?>" data-width="<?php echo $template->width; ?>"><?php echo $template->width . ' &#10005; ' . $template->height; ?>
                                                <input style="visibility:hidden;" type="radio" name="dimensions" value="<?php echo $template->width . '_' . $template->height; ?>" >
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <div class="enter-editor-size">
                            <h4>OR</h4>
                            <p>enter your own custom size</p>
                            <form>
                                <ul>
                                    <li><input class="width" type="text" placeholder="width" /></li>
                                    <li  style="margin: 11px;position: relative;">&#10005 </li>
                                    <li><input class="height" type="text" placeholder="height" /></li>
                                </ul>
                            </form>
                            <div class="continue-button">
                                <a href="#" class="button expand" ng-click="setCanvasDim()"> CONTINUE</a>
                            </div>
                        </div>
                    </div>
                </section>

                <section role="tabpanel" aria-hidden="true" class="content" id="tab2" >
                    <div class="text-center">
                        <div class="main-editor started-editor">
                            <div id="canvasDimensions" class="large-12 columns button-editor" data-filter-group="sizes">
                                <ul>
                                    <?php foreach ($templateSizeLayouts['dimensions'] as $template) { ?>
                                        <li><a href="#" class="button radius is-checked filter button-grid" source-type="2" data-filter="<?php echo '.' . $template->width . "_" . $template->height; ?>"><?php echo $template->width . '&#10005;' . $template->height; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <div id="grid-layouts" class="grid">
                                <?php if (empty($templateSizeLayouts['layouts'])) { ?>
                                    <div class="tabs-content" ng-controller="leftNavBarController">
                                        <section role="tabpanel" aria-hidden="true" class="content active" id="tab1">
                                            <div class="text-center">
                                                <div class="started-editor">
                                                    <h4></h4>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                <?php } else { ?>
                                    <?php foreach ($templateSizeLayouts['layouts'] as $layouts) { ?>
                                        <div class="grid-item  <?php echo $layouts->width . "_" . $layouts->height; ?>">
                                            <a href="#"> <img src="<?php echo $layouts->img_data; ?>" source_id="<?php echo $layouts->id; ?>" width="<?php echo $layouts->width; ?>" height="<?php echo $layouts->height; ?>" /></a>
                                        </div>
                                    <?php
                                    }
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                </section>
<?php } ?>

        </div>
        <form id="oeditorForm" style="">
            <input type="hidden" value="<?php echo csrf_token(); ?>" name="_token">
            <input type="hidden" name="_m_user_id" id="_m_user_id" value="<?php echo Auth::user()->id; ?>">
            <input type="hidden" name="_m_canvas_id" id="_m_canvas_id" value="0">
            <input type="hidden" name="title" id="_user_canvas_title" value=":Untitled Project">
            <input type="hidden" name="dimention" id="_user_canvas_dimention" value="">
            <input type="hidden" name="canvas_data" id="_canvas_data_json" value="">
            <input type="hidden" name="category" id="_category_id" value="">
<!--            <input type="hidden" name="frame_id" id="_frame_id" value="">
            <input type="hidden" name="frame_data" id="_frame_data_json" value="">
            <input type="hidden" name="frame_img_data_source" id="_frame_img_data_source" value="">-->
            <input type="hidden" name="img_data_source" id="_canvas_img_souce" value="">
        </form>

        <!--Add Fabric Js -->
        <script src="<?php echo url(); ?>/js/fabric.min.js"></script>
        <script src="<?php echo url(); ?>/js/customization.canvas.js"></script>
        <script src="<?php echo url(); ?>/js/foundation.min.js"></script>
        <script src="<?php echo url(); ?>/js/spectrum.js"></script>
        <script src="<?php echo url(); ?>/js/script.js"></script>
        <script src="<?php echo url(); ?>/js/isotope.pkgd.min.js"></script>

<!--        <script src="js/perfect-scrollbar.min.js"></script>-->
        <!--End of fabric JS-->
        <script>
            $(document).ready(function () {
                $('input[name="dimensions"]:checked').parent('a').addClass("is-selected");
                $("#designfromtemplate").on("click", function () {
                    setTimeout(function () {
                        $("#canvasDimensions").find("ul li:first-child a").trigger("click");
                    }, 50);
                });
                $(".button-section").on('click', 'ul li a', function () {
                    var canvasDimensions = $(this).find('input').val().split('_');
                    var val = $(this).find('input:radio').prop('checked') ? false : true;
                    $(this).find('input:radio').prop('checked', val);
                    $('.button-section').find('a').removeClass('is-selected')
                    if (val) {
                        $(this).addClass('is-selected')
                        $('.width').val(canvasDimensions[0]);
                        $('.height').val(canvasDimensions[1]);
                    }
                    localStorage.setItem('sourceType', 1);
                });
            });


            var intervalAccord;
            function intervalAccordian() {
                if ($("#menu_3d_container>section ").length > 0) {
                    $(document).foundation();
                    $(document).foundation('accordion', 'reflow');
                    clearInterval(intervalAccord);
                }

            }
            setInterval(function () {
                intervalAccordian()
            }, 1000);

            //Isotope for grid viewvar 
            $grid = $('.grid').isotope({
                itemSelector: '.grid-item',
                layoutMode: 'fitRows'
            });

            // store filter for each group
            var filters = {};

            $('.filter').on('click', function () {
                $('.NoLayouts').remove();
                filters[ 'sizes' ] = $(this).attr('data-filter');
                // combine filters
                var filterValue = concatValues(filters);
                // set filter for Isotope
                $grid.isotope({filter: filterValue});
                if (!$grid.data('isotope').filteredItems.length) {
                    $('.NoLayouts').remove();
                    $('.grid').append('<div class="NoLayouts">No Layouts</div>');
                }
            });

            // change is-checked class on buttons
            $('.button-grid').each(function (i, buttonGroup) {
                var $buttonGroup = $(buttonGroup);
                $buttonGroup.on('click', function () {
                    $('.button-grid').removeClass('is-checked');
                    $(this).addClass('is-checked');
                });
            });

            // flatten object by concatting values
            function concatValues(obj) {
                var value = '';
                for (var prop in obj) {
                    value += obj[ prop ];
                }
                return value;
            }
        </script>
<?php echo Session::set('_canvas_data', ''); ?>
    </body>
</html> 