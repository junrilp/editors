<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="_token" content="{{ csrf_token()}}" />
        <title>Online Editor</title>
        <script src="<?php echo url(); ?>/js/modernizr.js"></script>
        <script src="<?php echo url(); ?>/js/jquery.js"></script>
        <script src="<?php echo url(); ?>/js/jquery-ui.js"></script>
        <script src="<?php echo url(); ?>/js/angular.min.js"></script>
        <script src="<?php echo url(); ?>/js/bootstrap-colorpicker-module.js"></script>
        <script src=" <?php echo url(); ?>/js/angular-animate.min.js"></script>
        <script src="<?php echo url(); ?>/js/app-controller.js"></script>  
        <script src="<?php echo url(); ?>/js/jquery.wheelcolorpicker-3.0.3.min.js"></script>
        <script src="<?php echo url(); ?>/js/jquery.wheelcolorpicker.js"></script>




        <!--<link rel="stylesheet" href="css/perfect-scrollbar.css" />-->
        <link rel="stylesheet" href="<?php echo url(); ?>/css/foundation.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/colorpicker.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/app.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/flaticon.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/font-awesome.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/app.animation.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/spectrum.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/wheelcolorpicker.css" type="text/css" />
        <link href="<?php echo url(); ?>/api/data3.json"/>
        <link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Asap|Bree+Serif|Courgette|Dancing+Script|Gentium+Basic|Lobster|Lobster+Two|Merriweather|Oleo+Script|Playfair+Display|Satisfy" rel="stylesheet">
        <!--<link type="text/css" rel="stylesheet" href="css/wheelcolorpicker.css" />-->
        <style>
            .left-bar{
                    font-family: "Helvetica Neue",Helvetica,Roboto,Arial,sans-serif;
                    color: rgb(144,144,144) !important;
                    font-size: 14px !important;
                    font-weight: bold !important;
                    text-transform: capitalize !important;
            }
            .left-bar:hover{
                color: rgb(144,144,144) !important;
            }
        </style>
    </head>
    <body ng-app="editorApp" >
        <div>
            <a id="downloadImageCanvas" href="#" download style="visibility: hidden;position: absolute;top: 0;z-index: 0;">Download</a>
        </div>
        <div id="saveOverlayLoader" style='display:none;'>
            <img id="overlayLoaderImage" src="<?php echo url(); ?>/images/uploadImage.gif" >
            <div class="savingCanvasMessage">Saving your changes...</div>

        </div> 
        <!--header section--> 
        <section class="section-header">
            <nav data-topbar="" class="top-bar docs-bar hide-for-small">
                <a href="<?php echo url() . '/users' ?>" >
                    <!-- <h4>Bannerspro C Panel</h4>-->
                    <img src="../images/banner-logo.png" alt="logo" class="logo-default">
                </a>

                <!--   <ul class="title-area">
                      <li class="name">
                          <h1><a href="#">BannersPro</a></h1>
                      </li>
                  </ul>
                -->
                <!-- <div class="infoTitleBar">
                    <span class="xs-project-diamantion"></span>
                    <span class="xs-project-name"></span>
                </div> -->
               
                <?php if (empty($designFromTemplateData) || empty($designFromTemplateData->project_title)) : ?>
                    <div id="accessTemplate" class ="template-access">
                        <p><h3>Access Denied !!</h3>You are not able to use this template</p>
                        <a href="<?php echo url() . '/design' ?>">Back to customization</a>
                    </div>
                <?php else: ?>
                    
                    <a class="project-finalize-canvas left-bar" href="/design" style="margin-left: 35px;">New Design</a>
                    <a class="project-finalize-canvas left-bar" href="/canvas" style="margin-left: 65px;">My Design</a>
                   
                    <section class="top-bar-section">
                       <!--  <ul class="left xs-nav-options" style="float: left;">
                                <li class="not-click">
                                    <a class="project-finalize-canvas" href="/design">New Design</a>
                                </li>
                                <li class="not-click">
                                    <a class="project-finalize-canvas" href="/canvas">My Design</a>
                                </li>
                            </ul> -->
                        <ul class="right xs-nav-options">
                           
                            <li class="not-click welcome_profile_hover">
                                <a class="project-myaccount-cls" href="#">
                                    <span>Welcome <?php echo Auth::user()->first_name ?> </span>
                                    <i class="flaticon-avatar user_color_ican" ></i> 
                                    <i class="flaticon-down-arrow"></i></a>

                                <ul class="Welcome_profile">
                                    <li>
                                        <a href="<?php echo url() . '/users/' . (Auth::user()->id) . '/edit'; ?>"><i class="flaticon-avatar"></i> 
                                            <span>View Profile</span></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo url() . '/auth/logout'; ?>"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjY0cHgiIGhlaWdodD0iNjRweCI+CjxnPgoJPGc+CgkJPHBhdGggZD0iTTI1NiwyNzYuNjczYy0yOC4wMTYsMC01MC44MSwyMi43OTMtNTAuODEsNTAuODFjMCwxMy44OTUsNS43NzUsMjcuMzMsMTUuODU3LDM2Ljg5MXY0NS44NzUgICAgYzAsMTkuMjczLDE1LjY4LDM0Ljk1MywzNC45NTMsMzQuOTUzczM0Ljk1My0xNS42OCwzNC45NTMtMzQuOTUzdi00NS44NzVjMTAuMDc4LTkuNTU1LDE1Ljg1Ny0yMi45OTMsMTUuODU3LTM2Ljg5MSAgICBDMzA2LjgxLDI5OS40NjYsMjg0LjAxNywyNzYuNjczLDI1NiwyNzYuNjczeiBNMjczLjk4LDM0Ni41NThjLTQuODUxLDQuNTcxLTcuNjMzLDEwLjk2LTcuNjMzLDE3LjUzdjQ2LjE2MSAgICBjMCw1LjcwNS00LjY0LDEwLjM0NS0xMC4zNDUsMTAuMzQ1Yy01LjcwNSwwLTEwLjM0NS00LjY0LTEwLjM0NS0xMC4zNDV2LTQ2LjE2MWMwLTYuNTctMi43ODItMTIuOTYtNy42My0xNy41MjcgICAgYy01LjMwNC01LjAwMy04LjIyNi0xMS43NzgtOC4yMjYtMTkuMDc4YzAtMTQuNDQ3LDExLjc1NS0yNi4yMDIsMjYuMjAyLTI2LjIwMnMyNi4yMDIsMTEuNzU1LDI2LjIwMiwyNi4yMDIgICAgQzI4Mi4yMDIsMzM0Ljc4MywyNzkuMjgsMzQxLjU1OCwyNzMuOTgsMzQ2LjU1OHoiIGZpbGw9IiMwMDAwMDAiLz4KCTwvZz4KPC9nPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik00MDQuOTc4LDIwOS44NzZoLTIzNi40NHYtOTcuODA0YzAtNDguMjI3LDM5LjIzNC04Ny40NjQsODcuNDYyLTg3LjQ2NHM4Ny40NjMsMzkuMjM3LDg3LjQ2Myw4Ny40NjR2NDQuMjY4ICAgIGMwLDYuNzk1LDUuNTEsMTIuMzA0LDEyLjMwNCwxMi4zMDRzMTIuMzA0LTUuNTA4LDEyLjMwNC0xMi4zMDR2LTQ0LjI2OEMzNjguMDcxLDUwLjI3NSwzMTcuNzk2LDAsMjU2LDAgICAgUzE0My45MjksNTAuMjc1LDE0My45MjksMTEyLjA3MnY5Ny44MDRoLTM2LjkwOGMtMjAuMzUzLDAtMzYuOTExLDE2LjU1OS0zNi45MTEsMzYuOTExdjIyOC4zMDEgICAgYzAsMjAuMzUzLDE2LjU1OCwzNi45MTEsMzYuOTExLDM2LjkxMWgyOTcuOTU3YzIwLjM1MywwLDM2LjkxMS0xNi41NTgsMzYuOTExLTM2LjkxMVYyNDYuNzg4ICAgIEM0NDEuODksMjI2LjQzNSw0MjUuMzMxLDIwOS44NzYsNDA0Ljk3OCwyMDkuODc2eiBNNDE3LjI4Miw0NzUuMDg5YzAsNi43ODQtNS41MTksMTIuMzA0LTEyLjMwNCwxMi4zMDRIMTA3LjAyMiAgICBjLTYuNzg0LDAtMTIuMzA0LTUuNTE5LTEyLjMwNC0xMi4zMDRWMjQ2Ljc4OGMwLTYuNzg0LDUuNTItMTIuMzA0LDEyLjMwNC0xMi4zMDRoMjk3Ljk1N2M2Ljc4NCwwLDEyLjMwNCw1LjUxOSwxMi4zMDQsMTIuMzA0ICAgIFY0NzUuMDg5eiIgZmlsbD0iIzAwMDAwMCIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" /> 
                                            <span>Logout</span></a>
                                    </li>
                                </ul>
                            </li>

                            <li class="not-click">
                                <a class="project-name-cls" href="#"><span><?php echo $designFromTemplateData->project_title; ?></span></a>
                            </li>
                            <li class="not-click">
                                <a id="exportCanvas" class="project-finalize-canvas" href="">
                                    <i class="flaticon-export"></i> 
                                    Export    
                                </a>
                                <ul  class="export-canvas-to">
                                    <li><a class="image-version" href="" data-value="gif">GIF</a></li>
                                    <li><a class="image-version" href="" data-value="png">PNG</a></li>
                                    <li><a class="image-version" href="" data-value="jpg">JPG</a></li>
                                </ul>
                            </li>
                        
                            <li class="not-click" >
                                <a class="project-save-canvas Save_changes_color canvas-save" href="#">
                                    <!--<i class="flaticon-success "></i>-->
                                    <span>Save</span></a>
                            </li>
                           
                        </ul> 
                    </section>
                </nav>
            </section>

            <!--Section for left panel-->
            <section class="section-leftnav">
                <div class="row-fluid">
                    <!--leftnav panel-->
                    <div class="xs-left left-bar-nav-container" ng-controller="leftNavBarController">
                        <div class="xs-left left-bar-nav-panel">
                            <ul class="left-bar-nav-menu"> 

                                <li class="search" ng-if='isDisable == 1'><a id="section_1" ng-class='{"onactive":tog == 1}' ng-click='addItems(1);' data-tooltip aria-haspopup="true" class="has-tip tip-right" title="Layout 3D"></a></li>
                                <li class="layout"><a id="section_2"  ng-class='{"onactive":tog == 2}' ng-click='addItems(2);' data-tooltip aria-haspopup="true" class="has-tip tip-right" title="Select template to customize it">
                                        <i class="flaticon-duplicate-image"></i>
                                    </a>
                                </li>
                                <li class="text"><a id="section_3" ng-class='{"onactive":tog == 3}' ng-click='addItems(3);' data-tooltip aria-haspopup="true" class="has-tip tip-right" title="Add your Own text ">
                                        <i class="flaticon-font-size"></i>
                                    </a></li> 
                                <li class="backgrounds-colors"><a id="section_4" ng-class='{"onactive":tog == 4}' ng-click='addItems(4);' data-tooltip aria-haspopup="true" class="has-tip tip-right" title="Select Backgrounds">
                                        <i class="flaticon-ribbon"></i> 
                                    </a></li> 
                                <li class="uploads"><a id="section_5" ng-class='{"onactive":tog == 5}' ng-click='addItems(5);' data-tooltip aria-haspopup="true" class="has-tip tip-right" title="Upload your own Images or select background">
                                          <i class="flaticon-upload"></i>
                                    </a></li>
                                <li class="images"><a id="section_6" ng-class='{"onactive":tog == 6}' ng-click='addItems(6);' data-tooltip aria-haspopup="true" class="has-tip tip-right" title="Select Images">
                                        <i class="flaticon-image"></i>
                                    </a></li>
                                
                               <li class="buttonsRect"><a ng-class='{"onactive":tog == 8}' ng-click='addItems(8);' data-tooltip aria-haspopup="true" class="has-tip tip-right" title="Select buttons" style="margin-top:10px;">
                                        <img src="https://image.flaticon.com/icons/svg/121/121085.svg" width="20" height="20" alt="Menu free icon" title="Menu free icon">
                                    </a></li>

                                <li class="shapes"><a ng-class='{"onactive":tog == 9}' ng-click='addItems(9);' data-tooltip aria-haspopup="true" class="has-tip tip-right" title="Select shapes" style="margin-top:10px;">
                                        <img src="../images/finalShapes/shapeTool.png" width="20" height="20" alt="Shape free icon" title="Shape free icon">
                                    </a></li>
                                     
                                    <!--                            <li class="textfx"><a id="section_7" ng-class='{"onactive":tog == 7}' ng-click='addItems(7);' data-tooltip aria-haspopup="true" class="has-tip tip-right" title="TextFX"></a></li>-->
                                    <li class="undosection" id="undoOption">
                                        <a id="section_8" data-tooltip aria-haspopup="true" class="has-tip tip-right" title="Undo" onclick="plugin.undo()">
                                            <b><i class="flaticon-undo"></i></b>
                                        </a>
                                    </li>
                                    <li class="redosection" id="redoOption">
                                        <a id="section_9" data-tooltip aria-haspopup="true" class="has-tip tip-right" title="Redo" onclick="plugin.redo()">
                                            <i class="flaticon-redo"></i>
                                        </a>
                                    </li>

                                    <li class="fitscreen" id="fitscreenOption">
                                        <a id="section_10" data-tooltip aria-haspopup="true" class="has-tip tip-right" title="Fit to Screen" onclick="plugin.fitToScreen()">
                                            <i class="flaticon-expand-screen"></i>
                                        </a>
                                    </li>
                                      <li class="item-common-menu" id="Zoom_out_in">
                                            <a title="Zoom in" class= "transparent-layer has-tip tip-down" id="zoomIn"  data-tooltip aria-haspopup="true"> <i class="flaticon-add-circular-outlined-button"></i> </a> 
                                        <span id="zoomTextCaption" data-value="100">100%</span> 
                                        <a title="Zoom out" class= "transparent-layer has-tip tip-right" id="zoomOut"  data-tooltip aria-haspopup="true"> <i class="flaticon-minus-symbol"></i> </a>
                                        </li>
                                </ul>


                            </ul> 
                        </div>

                        <div class="xs-left left-bar-nav-panel-2">
                            <!--                      <div class="dropdown-template_design">
                              <select onchange="plugin.setFont(this.value)" class="select-dropbox" id="font-family">
                                                                        <option value="Abril Fatface" selected=""> 1000x1000 templates</option>
                                                                        <option value="arial">5000x5000 templates</option>
                                                                        <option value="verdana">4000x4000 templates</option>
                                                                        <option value="comic sans ms">3000x3000 templates</option>
                                                                        <option value="impact">500x500 templates</option>
                                                                        <option value="plaster">1000x1000 templates</option>
                                                                    </select>
                            </div>   -->

                            <div id="area_box" class="droppable">
                                <!-- Search part-->
                                <div class="animate-if" id="menu_3d" ng-if="tog == 1 && isDisable == 1">
                                    <!--<div class="menu-cls-3d" id="menu_3d_container" ng-bind-html="threeD | html"></div>-->  
                                    <div class="menu-cls-3d" id="menu_3d_container" compile="threeD"></div>
                                </div>

                                <!--Layout part-->

                                <div id="menu_layout" class="animate-if" ng-if="tog == 2">
                                    <!--<div class="dropdown-template_design">
                                        <select onchange="plugin.setFont(this.value)" class="select-dropbox" id="font-family">
                                            <option value="Abril Fatface" selected=""> 1000x1000 templates</option>
                                            <option value="arial">5000x5000 templates</option>
                                            <option value="verdana">4000x4000 templates</option>
                                            <option value="comic sans ms">3000x3000 templates</option>
                                            <option value="impact">500x500 templates</option>
                                            <option value="plaster">1000x1000 templates</option>
                                        </select>
                                    </div>-->
                                    <br/>
                                    <div class="menu-cls-layout" >
                                        <div  can-load="canLoad">
                                            <ul class="laidOut example-animate-containe uim-container-image-filter">
                                                <li ng-repeat="item in layouts| filter:q as results" class="animate-repeat large-6 medium-3 columns itemli">
                                                    <a class="uim_image_fill" source-id="{{item.id}}" href="" style="background-image:url('{{item.frame_image}}');background-size:cover;background-position:center;" data-type="layout">
                                                        <!--<img ng-src="{{item.frame_image}}" data-type="layout" >-->

                                                                 <!-- <img crossorigin="anonymous" ng-src="{{item.img_data}}"  data-type="layout" dragcontent> -->
                                                        <span class="uim-info-img-filter"><span class="inner">{{item.project_title}}</span></span>
                                                    </a>
                                                </li>
                                                <div style="color:#454545;" class="textControls" ng-show="!layouts.length"> No Layouts</div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <!--Text Layout-->
                                <div id="menu_text" class="animate-if" ng-if="tog == 3">
                                    <div class="row">
                                        <!--<div class="dropdown-template_design">
                                            <select onchange="plugin.setFont(this.value)" class="select-dropbox" id="font-family">
                                                <option value="Abril Fatface" selected=""> Text decoration</option>
                                                <option value="arial">Text style</option>
                                                <option value="verdana">Text decoration</option>
                                            </select>
                                        </div>-->
                                        <br/>
                                        <div class="textControls">
                                            <p style="color:#686868; opacity: 1;">Click or Drag to add text</p>
                                            <ul>
                                                <li style="color: auto; text-align: center;" data-type="text" dragcontent>
                                                    <a data-value='{"fontSize":"70","fontFamily":"Abril Fatface","text":"Add heading","fromTop":"100","stroke":"","strokeWidth":"","fill":"#000"}' style="color: auto; font-family: 'Abril Fatface'; font-size: 50px; font-style: normal; font-weight: normal;" data-style="title" class="add">Add heading</a>
                                                </li>
                                                <li style="color: auto; text-align: center;" data-type="text" dragcontent>
                                                    <a data-value='{"fontSize":"40","fontFamily":"Abril Fatface","text":"Add subheading","fromTop":"40","stroke":"","strokeWidth":"","fill":"#000"}' style="color: auto; font-family: 'Trocchi'; font-size: 230%; font-style: normal; font-weight: normal;" data-style="subtitle" class="add">Add subheading</a>
                                                </li>
                                                <li style="color: auto; text-align: center;" data-type="text" dragcontent>
                                                    <a  data-value='{"fontSize":"26","fontFamily":"Abril Fatface","text":"Add a little bit of body text","fromTop":"0","stroke":"","strokeWidth":"","fill":"#000"}' style="color: auto; font-family: 'Helvetica'; font-size: 153.33333333333334%; font-style: normal; font-weight: normal;" data-style="body" class="add">Add a little bit of body text</a>
                                                </li>

                                            </ul>


                                            <div class="textControls  menuBgControls "  ng-if="tog == 3">
                                                <br/>
                                                <div class="row">
                                                    <ul  ng-repeat="text in texts">
                                                        <li style="color: auto; text-align: center;" data-type="text" dragcontent>
                                                            <a data-value='{"text": "{{text.text}}","fontFamily": "{{text.fontFamily}}", "fontSize": "{{text.fontSize}}" ,"fill":"{{text.fill}}" ,"fromTop":"0","fontWeight":"{{text.fontWeight}}","stroke":"{{text.stroke}}","strokeWidth":"{{text.strokeWidth}}","shadow":{{ text.shadow }} }' ng-style="{'text': text.text ,'fontFamily': text.fontFamily, 'fontSize': text.showfontSize , 'color':text.color ,'font-weight':text.fontWeight,'stroke':text.stroke,'stroke-width':text.strokeWidth ,'text-shadow': text.textShadow}" type="text" class="add">
                                                                {{text.text}}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div> 

                                <!--background Layout-->
                                <div id="menu_background" class="animate-if" ng-if="tog == 4">
                                    <div class="row">
                                        <!--                                        <div class="dropdown-template_design">
                                                                                    <select class="select-dropbox background-button">
                                                                                        <option value="colors" selected='selected'>Background Colors</option>
                                                                                        <option value="backgrounds">Backgrounds</option>
                                                                                        <option value="buttons">Buttons</option>
                                                                                    </select>
                                                                                </div>  -->
                                        <div class="colorControls menuBgControls ">
                                            
                                                <div class="paletteToolbarWrapper static" style="display: block;">
                                                
                                                <menu class="paletteToolbar scrollPane">
                                                <!-- <select name="selection" ng-model="myDropDown">
                                                    <option>Background Color</option>
                                                    <option >Buttons</option>
                                                </select> -->
                                                    <li ng-repeat="backgroundColor in backgroundColors" style="background-color:{{backgroundColor}};" class="colorOption"  ng-init="visible = true" ng-show="visible"><a href="#" data-color="{{backgroundColor}}">{{backgroundColor}}</a><span class="badge hover_me_icon"><i class="fa fa-trash" ng-click="visible = false" style="color:red"></i></span></li>
                                                    <li style = "border:1px solid grey;" class="picker background_picker"><a data-type="backgroundColor" colorpicker ng-model="your.model" href="#">Add a color</a></li>
                                               </menu>
                                            </div>
                                           <ul  class="laidOut example-animate-containe uim-container-image-filter">
                                                <li ng-repeat="item in backgrounds| filter:q as results" class="animate-repeat large-6 medium-6 columns itemli">
                                                    <a data-type="backImage" dragcontent data-img="{{item.original_src}}" class="uim_image_fill" style="background-image:url('{{item.thumb_src}}');background-size:cover;background-position:center;">
                                                        <!--<img data-id =""  ng-src="{{item.thumb_src}}" width="100" height="75">-->
                                                        <span class="uim-info-img-filter"><span class="inner">{{item.currency}}{{item.price}}</span></span>
                                                    </a>
                                                </li>
                                                <div style="color:#454545;" class="textControls" ng-show="!backgrounds.length"> No Backgrounds</div>
                                            </ul>
                                        </div>
                                        

                                    </div>
                                </div>

                                <!--upload Layout-->
                                <div id="menu_upload" class="animate-if" ng-if="tog == 5">

                                    <section id="upload_image" class="uploadButtonAndMessage">
                                        <style>
                                            .fileUpload {
                                                position: relative;
                                                overflow: hidden;
                                            }
                                            .fileUpload input.upload {
                                                position: absolute;
                                                top: 0;
                                                right: 0;
                                                margin: 0;
                                                padding: 0;
                                                font-size: 20px;
                                                cursor: pointer;
                                                opacity: 0;
                                                filter: alpha(opacity=0);
                                                height: 100%;
                                                width: 100%;

                                            }
                                            /*                                        .droppable { background: #797C80 }*/
                                            .droppable.dragover { background: #797C80 }
                                        </style>

                                        <div class="fileUpload btn btn-primary" ondrop="angular.element(this).scope().dropFile(event)" ondragover="return false">
                                            <button class="button buttonBlock fileUploadButton">Upload your own images</button>
                                            <input type="file"  name="image" class="upload" onchange="angular.element(this).scope().setFile($(this))"/>
                                            <p style="color:grey;" ><div id="myProgress" ng-show="fileUploadProcessing">
                                                  <div id="myBar"></div>
                                                </div></p>
                                            <p style="color:grey;" ng-show="fileUploadStatus">{{fileUploadStatus}}</p>
                                            <p ng-hide="fileUploadStatus">...or just drag them from your desktop.</p>
                                        </div>
                                    </section>
                                    <style type="text/css">
                                        #myProgress {
                                          width: 90%;
                                          background-color: #ddd;
                                          margin: -10px 17px;
                                        }

                                        #myBar {
                                          width: 1%;
                                          height: 20px;
                                          background-color: #4CAF50;
                                        }
                                    </style>
                                    <hr style="margin: 0 0 10px 0;">

                                    <div class="image-area-wrapper">
                                        <ul class="laidOut example-animate-containe">
                                            <li ng-repeat="item in userImages| filter:q as results"  class="animate-repeat large-6 medium-6 columns itemli">
                                                <a  dragcontent data-type="uploadImage"  image-src="{{item.original_src}}" link-src="{{item.original_src}}" style="background-image:url('{{item.original_src}}');background-size:cover;background-position:center;" >
                                                    <!--<img onclick="return false;" ng-src="{{item.thumb_src}}" link-src="{{item.original_src}}" width="100" height="75">-->
                                                </a>
                                            </li>
                                            <div style="color:#454545;" class="textControls" ng-show="!userImages.length"> No Uploads</div>
                                        </ul>
                                    </div>

                                </div> 

                                <!--Images section-->
                                <div id="manu_images" class="animate-if"  ng-if="tog == 6" >
                                    <br/>
                                    <div class="row" >
                                        <div can-load="canLoad">
                                            <ul class="laidOut example-animate-containe uim-container-image-filter">
                                                <li ng-repeat="item in uImages| filter:q as results" class="animate-repeat large-6 medium-6 columns itemli">
                                                    <a data-type="image" dragcontent image-src="{{item.original_src}}" data-type="image" style="background-image:url('{{item.thumb_src}}');background-size:cover;background-position:center;">
                                                        <!--<img data-type="image"  ng-src="{{item.thumb_src}}">-->
                                                        <span class="uim-info-img-filter"><span class="inner">{{item.info}}</span></span>
                                                    </a>
                                                </li>
                                                <div style="color:#454545;" class="textControls" ng-show="!uImages.length"> No Images</div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <!--background section-->
                                <div  id="menu_background" class="layoutbackgroundDiv menuBgControls " ng-if="tog == 7">
                                    <br/>
                                    <ul  class="laidOut example-animate-containe uim-container-image-filter">
                                        <li ng-repeat="item in backgrounds| filter:q as results" class="animate-repeat large-6 medium-6 columns itemli">
                                            <a data-type="backImage" dragcontent data-img="{{item.original_src}}" class="uim_image_fill" style="background-image:url('{{item.thumb_src}}');background-size:cover;background-position:center;">
                                                <!--<img data-id =""  ng-src="{{item.thumb_src}}" width="100" height="75">-->
                                                <span class="uim-info-img-filter"><span class="inner">{{item.currency}}{{item.price}}</span></span>
                                            </a>
                                        </li>
                                        <div style="color:#454545;" class="textControls" ng-show="!backgrounds.length"> No Backgrounds</div>
                                    </ul>

                                </div>
                                <!-- button section design @july 20 -->
                                <div class="buttonControls menuBgControls "  ng-if="tog == 8" >
                                    <br/>
                                    <div class="row">
                                        <div class="large-6 medium-6 buttonrule" ng-repeat="button in buttonsRect.set_1">
                                            <button id="BTN" class="" s="button" ng-style="(button.colortype=='fill') ? {'background': button.color, 'color':'#fff' ,'font-weight':'bold', 'border-radius':button.radius + 'px'} : {'background': 'transparent', 'border': '1px solid ' + button.color, 'color':button.color , 'border-radius':button.radius + 'px'}" type="button" radius="{{button.radius}}" color="{{button.color}}" colortype="{{button.colortype}}" style="text-decoration-style: solid">
                                                {{button.text}}
                                            </button>
                                        </div>
                                    </div>

                                </div>

                                <div class="shapeControls menuBgControls "  ng-if="tog == 9" >
                                    <br/>
                                    <div class="row">
                                        <div class="large-3 medium-3 buttonrule" ng-repeat="shape in shapes.set_1">
                                            <div id="allShapes" width="80px" height="100px">

                                                <img src="{{shape.background}}" style="cursor: pointer; width: 50px; height: 50px;">
                                                <p style="display:none">{{shape.text}}</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!--TextFx section-->
                                <!--                                <div id="menu_textfx" class="animate-if" ng-if="tog == 7">
                                                                    <a class="button buttonBlock fileUploadButton" data-reveal-id="modalTextFx">3D Text Rendering</a>
                                                                </div>-->

                            </div>
                        </div>

                        <!--Modal for Layout Size-->
                        <a id="modalCanvasSizeOpener" class="button buttonBlock fileUploadButton" data-reveal-id="modalLayoutSizeFx" style="opacity:0;visibility:hidden;display:none;">Select Canvas Size</a>
                        <div id="modalLayoutSizeFx" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                            <?php if (empty($dimensions) || empty($designFromTemplateData) ) : ?>
                                <h3>Size List Empty</h3>
                                <p>Please contact to site administrator. Or login as <a href="<?php echo url() . '/auth/login' ?>">administrator</a> to add 3D mockups and sizes</p>
                            <?php else: ?>
                                <h3>Choose Canvas Size</h3>
                                <form>
                                    <div class="row">
                                        <div class="large-12 columns">
                                            <label class="xs-label-pin" style="float:left;margin-right:25px;">
                                                <input class="xs-pot-select" type="radio" value="1" name="optionCol" checked="checked"> 
                                                   3D Mockup
                                            </label>
                                            <label class="xs-label-pin" style="">
                                                <input class="xs-pot-select" type="radio" value="2" name="optionCol">
                                                Choose Size
                                            </label>
                                        </div>
                                    </div>

                                    <div class="row xs-custom-size">
                                        <div class="large-12 columns">
                                            <label>
                                                <span style="font-size:12px;">width &#10005 height</span>
                                                <select id="layout_canvas_w_h">
                                                    <?php
                                                    foreach ($dimensions as $i => $data) {
                                                        $sel = ($i == 0) ? "selected" : "";
                                                        ?>
                                                        <option <?php echo $sel; ?>  value="<?php echo $data->width . '_' . $data->height; ?>"><?php echo $data->width . '&#10005;' . $data->height; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-top:10px;">
                                        <div class="large-12 columns">
                                            <a id="createCanvasEl" class="button small" ng-click="setCanvasSize()">Create</a>
                                        </div>
                                    </div>
                                </form>
                            <?php endif; ?>
                        </div>

                    </div>

                    <!-- save canvas modal -->
                    <!--                <div style="display:none;"class="modal fade" id="saveCanvasModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                    
                                                </div>
                                                <div class="modal-body">...</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->
                    <!--Right panel-->
                    <div class="xs-left right-bar-nav-container" style="background: #CDD1CD; text-align: center;">
                        <div id="canvas_section_head"  class="canvas-container-main droppable" style="width:100%;height:100%;margin:0 auto;position:relative;">
                            <canvas id="canvas" width="530" height="400" class="canvas-frame"></canvas>
                            
                            
                            <!--Tool tip-->
                            <section id="section_toolkit_menu" ng-controller="toolTipController">
                                <div class="toolkit-container">
                                    <div class="xs-tool-tip">
                                        <div  class="xs-block-el mx-page-font-family" ng-if="canvasObjTypeCng == 'i-text' || canvasObjTypeCng == 'group'">
                                            <select onchange="plugin.setFont(this.value)" class="select-dropbox" id="font-family">
                                                <option value="arial" selected="">Arial</option>
                                                <option value="Abril Fatface" selected="">Abril Fatface</option>
                                                <option value="Asap" selected="">Asap</option>
                                                <option value="Bree Serif" selected="">Bree Serif</option>
                                                <option value="Courgette" selected="">Courgette</option>
                                                <option value="Dancing Script" selected="">Dancing Script</option>
                                                <option value="Gentium Basic" selected="">Gentium Basic</option>
                                                <option value="Lobster" selected="">Lobster</option>
                                                <option value="Lobster Two" selected="">Lobster Two</option>
                                                <option value="Merriweather" selected="">Merriweather</option>
                                                <option value="Oleo Script" selected="">Oleo Script</option>
                                            </select>
                                        </div>
                                        <div  ng-controller="leftNavBarController" class="xs-block-el mx-page-font-size" ng-if="canvasObjTypeCng == 'i-text'">
                                            <select id="font-size" onchange="plugin.setFontSize(this.value)" class="select-dropbox">
                                                <option ng-repeat="fontSize in fontSizeLimit" value="{{fontSize}}">{{fontSize}}</option>
                                            </select>
                                        </div>


                                        <div ng-controller="leftNavBarController" class="xs-block-el mx-page-text-color item-text-menu" ng-if="canvasObjTypeCng == 'i-text'">
                                            <menu class="paletteToolbar scrollPane">
                                                <li style="background-color:#000000"><a href="#" data-color="#000000" id="textColorPickerId">#000000</a></li>
                                                <!--<li style="background-color:#000000; display:none;"><a href="#" data-color="#000000" id="gradientColorPickerId">#000000</a></li>-->
                                            </menu>
                                            <ul id="color-plates-text" class="paletteToolbar scrollPane xs-popover" style="display:none;">
                                                <li style="width:100%;border-radius:0;box-shadow:none;border:medium none;">
                                                    <div class="xs_canvas_color_opt">
                                                        <label><input type="radio" name="textcolor_choose_for" value="text" checked="checked"> Text</label>
                                                        <label><input type="radio" name="textcolor_choose_for" value="stroke"> Stroke</label>
                                                        <label><input type="radio" name="textcolor_choose_for" value="gradient"> Gradient</label>
                                                        
                                                    </div>
                                                </li>
                                                <li ng-repeat="textColor in defaultTextColors" style="background-color:{{textColor}};" class="colorOption_text"><a href="#" data-color="{{textColor}}">{{textColor}}</a></li>
                                                <li class="picker"><a data-type="textColor" colorpicker ng-model="colorpicker.model" href="#">Add a color</a></li>
                                                <div class="xs_canvas_stroke_width" style="display:none">
                                                    <label><input type="radio" name="stroke_choose_for" value="thin" checked="checked"> Thin </label>
                                                    <label><input type="radio" name="stroke_choose_for" value="medium"> Medium </label>
                                                    <label><input type="radio" name="stroke_choose_for" value="thick"> Thick </label>
                                                </div>
                                            </ul>
                                        </div>

                                        <!-- for button color changes-->
                                        <div ng-controller="leftNavBarController" class="xs-block-el mx-page-text-color item-text-menu" ng-if="canvasObjTypeCng == 'group'">
                                            <menu class="paletteToolbar scrollPane">
                                                <li style="background-color:#000000"><a href="#" data-color="#000000" id="buttonColorPickerId">#000000</a></li>
                                            </menu>


                                            <ul id="color-plates-button" class="paletteToolbar scrollPane xs-popover" style="display:none;">
                                                <li style="width:100%;border-radius:0;box-shadow:none;border:medium none;">
                                                    <div class="xs_canvas_color_opt">
                                                        <label><input type="radio" name="textbutton_choose_for" value="text" checked="checked"> Text</label> 
                                                        <label><input type="radio" name="textbutton_choose_for" value="button"> Button</label>
                                                        <label><input type="radio" name="textbutton_choose_for" value="shape"> Shape</label>
                                                    </div>
                                                </li>
                                                <li ng-repeat="textColor in defaultTextColors" style="background-color:{{textColor}};" class="colorOption_button"><a href="#" data-color="{{textColor}}" style="line-height: 20px">{{textColor}}</a></li>
                                                <li class="picker"><a data-type="buttonTextColor" colorpicker ng-model="colorpicker.model" href="#">Add a color</a></li>


                                            </ul>
                                        </div>
                                        <!--
                                        <div ng-controller="leftNavBarController" class="xs-block-el mx-page-text-color item-text-menu" ng-if="canvasObjTypeCng == 'image' ">
                                            <menu class="paletteToolbar scrollPane">
                                                <li style="background-color:#000000"><a href="#" data-color="#000000" id="imageColorPickerId">#000000</a></li>
                                            </menu>
                                            
                                            <div id="color-plates-image-layer" class="colorControls paletteToolbar scrollPane xs-popover">
                                                <div class="paletteToolbarWrapper static" style="display: block;">
                                                    <menu class="paletteToolbar scrollPane">
                                                        <li ng-repeat="backgroundColor in backgroundColors" style="background-color:{{backgroundColor}};" class="colorOption"><a href="#" data-color="{{backgroundColor}}">{{backgroundColor}}</a></li>
                                                        <li class="picker"><a data-type="backgroundColor" colorpicker ng-model="your.model" href="#">Add a color</a></li>
                                                    </menu>
                                                </div>
                                            </div>
                                        </div>
                                        -->

                                        <div ng-if="canvasObjTypeCng == 'i-text' || canvasObjTypeCng == 'group'"  class="xs-block-el item-common-menu xs-menu-bar-tag">
                                            <a class="bold-layer-icon tbar-menu-icon has-tip tip-down" data-attr="bold" data-tooltip aria-haspopup="true"  title="Bold">
                                                <i class="flaticon-font-style-bold black-color"></i> 
                                            </a>
                                        </div>

                                        <div ng-if="canvasObjTypeCng == 'i-text' || canvasObjTypeCng == 'group'" class="xs-block-el item-common-menu xs-menu-bar-tag">
                                            <a class="italic-layer-icon tbar-menu-icon has-tip tip-down" data-attr="italic" data-tooltip aria-haspopup="true"  title="Italic">
                                                <i class="flaticon-italic-text-option-interface-symbol"></i> 

                                            </a>
                                        </div>

                                        <div ng-if="canvasObjTypeCng == 'i-text'" class="xs-block-el item-common-menu xs-menu-bar-tag">
                                            <a class="left-layer-icon tbar-menu-icon has-tip tip-down" data-attr="left" data-tooltip aria-haspopup="true"  title="Left">
                                                <i class="flaticon-left-alignment-2"></i> 

                                            </a>
                                        </div>

                                        <div ng-if="canvasObjTypeCng == 'i-text'" class="xs-block-el item-common-menu xs-menu-bar-tag">
                                            <a class="center-layer-icon tbar-menu-icon has-tip tip-down" data-attr="center" data-tooltip aria-haspopup="true"  title="Center">
                                                <i class="flaticon-center-align"></i> 

                                            </a>
                                        </div>

                                        <div ng-if="canvasObjTypeCng == 'i-text'" class="xs-block-el item-common-menu xs-menu-bar-tag">
                                            <a class="right-layer-icon tbar-menu-icon has-tip tip-down" data-attr="right" data-tooltip aria-haspopup="true"  title="Right">
                                                <i class="flaticon-right-alignment-1"></i> 

                                            </a>
                                        </div>



                                        <!--  <div ng-if="canvasObjTypeCng == 'image'" class="xs-block-el item-common-menu xs-menu-bar-tag">
                                             <a class="flip-hor-layer-icon tbar-menu-icon has-tip tip-down" data-attr="flip_horizonal" data-tooltip aria-haspopup="true"  title="Horizonal">
                                              
                                                 
                                             </a>
                                         </div> -->

                                        <!--  <div ng-if="canvasObjTypeCng == 'image'" class="xs-block-el item-common-menu xs-menu-bar-tag">
                                             <a class="flip-ver-layer-icon tbar-menu-icon has-tip tip-down" data-attr="flip_vertical" data-tooltip aria-haspopup="true"  title="Vertical">
                                                
                                                 
                                             </a>
                                         </div> -->


                                        <div ng-if="canvasObjTypeCng == 'i-text'" class="xs-block-el item-common-menu xs-menu-bar-tag">
                                            <a class="text-uppercase-layer tbar-menu-icon has-tip tip-down" data-attr="uppercase" data-tooltip aria-haspopup="true"  title="Uppercase">
                                                <i class="flaticon-font-size"></i>


                                            </a>
                                        </div>

                                        <div  class="xs-block-el item-common-menu xs-menu-bar-tag">
                                            <a class="copy-layer tbar-menu-icon has-tip tip-down" data-attr="copy" data-tooltip aria-haspopup="true"  title="Copy">
                                                <i class="flaticon-copy-two-paper-sheets-stroke-symbol-of-interface"></i> 

                                            </a>
                                        </div>

                                        <div ng-if="canvasObjTypeCng == 'i-text'" class="xs-block-el item-common-menu xs-menu-bar-tag txt-spacing-slide-bx">
                                            <a class="text-spacing-layer tbar-menu-icon has-tip tip-down" data-attr="text-spacing" data-tooltip aria-haspopup="true"  title="Text Space">
                                                <i class="flaticon-line-spacing-adjustment-in-a-paragraph"></i> 


                                            </a>
                                            <ul class="xs-popover-more-layer " >
                                                <li>
                                                    <ol class="text-spacing-popover xs-popover">
                                                        <span class="set-transparency-label">Line Height</span>
                                                        <small id="line-height-counter"></small>
                                                        <div id="range-slider-line-height" class="range-slider" data-slider data-options="display_selector: #line-height-counter; start:-10; end:10; initial: 1;step:0.1;">
                                                            <span class="range-slider-handle" role="slider" tabindex="0"></span>
                                                            <span class="range-slider-active-segment"></span>
                                                        </div>
                                                    </ol>
                                                </li>
                                            </ul> 
                                        </div>

                                        <!--                                    <div ng-if="canvasObjTypeCng == 'i-text' " class="xs-block-el item-common-menu xs-menu-bar-tag txt-spacing-slide-bx">
                                                                                <a class="text-stroke tbar-menu-icon has-tip tip-down" data-attr="text_stroke" data-tooltip aria-haspopup="true"  title="Stroke"></a>
                                                                            </div>-->

                                        <div ng-if="canvasObjTypeCng == 'i-text'" class="xs-block-el item-common-menu xs-menu-bar-tag txt-spacing-slide-bx">
                                            <a class="text-shadow tbar-menu-icon has-tip tip-down" data-attr="text_shadow" data-tooltip aria-haspopup="true"  title="Shadow">
                                                <i class="flaticon-square-shadow black-color"></i> 


                                            </a>
                                        </div>

                                        <!--                                    <div ng-if="canvasObjTypeCng == 'i-text' " class="xs-block-el item-common-menu xs-menu-bar-tag txt-spacing-slide-bx">
                                                                                <a class="text-gradient tbar-menu-icon has-tip tip-down" data-attr="text_gradient" data-tooltip aria-haspopup="true"  title="Gradient"></a>
                                                                            </div>-->

                                        <div class="xs-block-el item-common-menu xs-menu-bar-tag transparent-slide-bx" >
                                            <a class="transparent-layer tbar-menu-icon has-tip tip-down"  data-attr="transparancy" data-tooltip aria-haspopup="true"  title="Transparency">
                                               <!-- <i class="flaticon-font-style-bold"></i>  -->
                                                <i class="flaticon-diamond black-color"></i> 
                                            </a>
                                            <ul class="xs-popover-more-layer">
                                                <li>
                                                    <ol class="transparancy-popover xs-popover">
                                                        <span class="set-transparency-label">Set Transparancy</span>
                                                        <div id="range-slider-filter" class="range-slider" data-slider data-options="display_selector: #transparancy-range-limit; start:1; initial: 100;">
                                                            <span class="range-slider-handle" role="slider" tabindex="0"></span>
                                                            <span class="range-slider-active-segment"></span>
                                                        </div>
                                                        <small id="transparancy-range-limit"></small>
                                                    </ol>
                                                </li>
                                            </ul>
                                        </div>



                                        <div class="xs-block-el item-common-menu">
                                            <a class="delete-layer-icon tbar-menu-icon has-tip tip-down" id="deleteLayerId" data-tooltip aria-haspopup="true"  title="Delete">
                                               <!-- <i class="flaticon-rubbish-bin"></i>  -->
                                                <i class="flaticon-rubbish-bin black-color"></i> 
                                            </a>
                                        </div>
                                       <!--<div class="xs-block-el item-common-menu" id="Zoom_out_in">-->
                                       <!--     <a title="Zoom in" class= "transparent-layer has-tip tip-down" id="zoomIn"  data-tooltip aria-haspopup="true"> <i class="flaticon-add-circular-outlined-button"></i> </a> -->
                                       <!-- <span id="zoomTextCaption" data-value="100">100%</span> -->
                                       <!-- <a title="Zoom out" class= "transparent-layer has-tip tip-right" id="zoomOut"  data-tooltip aria-haspopup="true"> <i class="flaticon-minus-symbol"></i> </a>-->
                                       <!-- </div>-->

                                       

                                        <!--     <div class="xs-block-el item-common-menu xs-menu-bar-tag">
                                               <a class="move-f-layer-icon tbar-menu-icon has-tip tip-down" data-attr="move_forward" data-tooltip aria-haspopup="true"  title="Forward">
                                                    
                                                   
                                                   
                                               </a>
                                           </div>
                                        -->
                                        <!--    <div class="xs-block-el item-common-menu xs-menu-bar-tag">
                                               <a class="move-b-layer-icon tbar-menu-icon has-tip tip-down" data-attr="move_backward" data-tooltip aria-haspopup="true"  title="Backword">
                                                    
                                                  
                                                   
                                               </a>
                                           </div> -->
                                        <!--Counting of layers-->

                                    </div>

                                </div>
                            </section>
                        </div>
                        <!-- Loading canvas -->
                        <div  class="loading-canvas-inside">Loading your changes...</div>
                        <div  class="canvas-inside"> Loading canvas ....</div>
                    </div>
                </div>
            </section>

            <!--Section for the right side animation bar-->
            <div class="count-layers-obj" style="opacity: 1;" style="none"></div>
                <span class="" style="color:white">
                     
                </span>
             <section id="drop" class="uim-animation-bar-wrap ">
                
                <div class="uim-animation-bar-head">
                    <div class="animation-bar-title">
                        <i class="fa fa-angle-down animate-triangle" style="cursor: pointer;font-size: 20px;"></i>  <i class="fa fa-film"></i> Animation <i class="fa fa-play"></i><i class="fa fa-stop"></i><i class="fa fa-forward"></i><i class="fa fa-backward"></i>
                    </div>
                    <form id="animation-bar-form" action="#">
                        <input type="hidden" value="<?php echo csrf_token(); ?>" name="_token">
                        <div class="uim-animation-bar-segment"> 
                            <?php 
                            if(!empty($designFromTemplateData->frames[0])){
                                foreach($designFromTemplateData->frames  as $key=>$item) {
//                                    print_r($item->frame_data);
                                  
                                 ?>
                            
                                <div class="animation-segment-1 animation-segment" data-count="<?php echo $key+1; ?>">
                                    <span class="closespan">
                                        <a class="close" aria-label="Close">&#215;</a>
                                    </span>
                                    <div class="animation-segment-img <?php if($key== 0){ ?> frame-active <?php } ?>" style="background-image:url('<?php echo $item->frame_image ?>'); background-size:cover;background-position:center;"> 
                                       
                                    </div>
                                    <input type="hidden" name="framedata[]" class="framedata" value='<?php echo $item->frame_data; ?>'/>
                                    <input type="hidden" name="frames[]" class="frame" value='<?php echo $item->frame_image?>'/>
                                    <input type="text" name="frameTime[]" value="3" class="frametime" title="Time will be in S(Seconds)"/>
                                    <input type="hidden" name="imagetype" id="imagetype" value="gif"/>

                                </div>

                            <?php } } else {?>
                                 <div class="animation-segment-1 animation-segment" data-count="1" style="position: relative;">
                                    <span class="closespan">
                                        <a class="close" aria-label="Close">&#215;</a>
                                    </span>  
                                    <div class="animation-segment-img frame-active"> 
                                        <img src=""/>   
                                    </div>
                                    <input type="hidden" name="framedata[]" class="framedata" value=""/>
                                    <input type="hidden" name="frames[]" class="frame" value=""/>
                                    <input type="hidden" name="framethumb[]" class="frame-thumb" id = "_canvas_img_thumb" value=''/>
                                    <input type="text" name="frameTime[]" value="3" class="frametime" title="Time will be in S(Seconds)"/>
                                    <input type="hidden" name="imagetype" id="imagetype" value="gif"/>

                                </div>   
                             
                            <?php } ?> 
                        
                           
                    </form>
                    
                </div>
 <div class=" uim-animation-bar-addnew">
                                <a href="#"></a>
                            </div>
                        
            </section>
    <!--            <section id="section_toolkit_menu" >


            </section>-->


            <!--Modal for textFX-->
            <div id="modalTextFx" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                <form>
                    <div class="row">
                        <div class="large-12 columns">
                            <label>Enter text
                                <input type="text" placeholder="Enter a Text to create" />
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <label>Select Font
                                <select>
                                    <option selected="" value="Abril Fatface">Abril Fatface</option>
                                    <option value="arial">Arial</option>
                                    <option value="verdana">Verdana</option>
                                    <option value="comic sans ms">Comic Sans MS</option>
                                    <option value="impact">Impact</option>
                                    <option value="plaster">Plaster</option>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <label>
                                <input id="picker_spectrums" type="text" placeholder="Pick Color" style="width:45px;" />
                            </label>
                        </div>
                    </div>
                    <div class="row" style="margin-top:10px;">
                        <div class="large-12 columns">
                            <a class="button small">Get Text</a>
                        </div>
                    </div>
                </form>
            </div>

            <form id="oeditorForm" style="">
                <input type="hidden" value="<?php echo csrf_token(); ?>" name="_token">
                <input type="hidden" name="_m_user_id" id="_m_user_id" value="<?php echo Auth::user()->id; ?>">
                <input type="hidden" name="_m_canvas_id" id="_m_canvas_id" value="<?php echo $designFromTemplateData->id; ?>">
                <input type="hidden" name="title" id="_user_canvas_title" value="<?php echo $designFromTemplateData->project_title; ?>">
                <input type="hidden" name="dimention" id="_user_canvas_dimention" value="<?php echo $designFromTemplateData->width . '_' . $designFromTemplateData->height; ?>">
                <?php if(!empty($designFromTemplateData->frames[0])) {?>
                    <input type="hidden" name="canvas_data" id="_canvas_data_json" value='<?php echo $designFromTemplateData->frames[0]->frame_data; ?>'>
                    <input type="hidden" name="img_data_source" id="_canvas_img_souce" value="<?php echo $designFromTemplateData->frames[0]->frame_image; ?>">
                    
                 <?php } ?>
                <input type="hidden" name="dimWidth" id="_user_canvas_dimWidth" value="<?php echo $designFromTemplateData->width ?>">
                <input type="hidden" name="dimHeight" id="_user_canvas_dimHeight" value="<?php echo $designFromTemplateData->height ?>">
                <input type="hidden" name="editor_tool" id="editor_tool" value="<?php echo $editor_tool->editor_tool ?>">
            </form>

            <div id="modal3dLayoutOpt" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                <div id="modal3dContentDiv"></div>
            </div>

            <div id="modalNavMenuOpt" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                <div id="modalNavContentDiv">
                    <label>Please wait. System is in progress.. <span id="progress_percent_rate"></span></label>
                    <div class="finalalize-image">
                        <div class="progress large-12 success round"><span class="meter" style="width: 0%"></span></div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
            <!--        <div id="gifModal" class="modal fade reveal-modal small"  data-reveal role="dialog">
                      <div class="modal-dialog">
            
                         Modal content
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Modal Header</h4>
                          </div>
                          <div class="modal-body">
                            <p>Please select multiple frames </p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
            
                      </div>
                    </div>-->
            <div id="overlaybuttonGroup" style='display:none;'>
                <div id="buttonmodal" class="modal">

                    <!-- Modal content -->
                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <p>Change Text</p>
                        <input id="buttonText" type="text" value="" name="buttonText"/>
                        <button class="changeText btn btn-primary" type="button" value="Change" name="buttonText">Continue</button>
                    </div>
                </div>
            </div>
                   
            <div class="loadingpage" id="loadingToolPage">
                <div class="loading-gif">
                    <a class="loadingAnchorTag"></a>
                    <p>Loading Please wait....</p>
                </div>
            </div>
            <div id="overlayLoader" style='display:none;'>
                <img id="overlayLoaderImage" src="<?php echo url(); ?>/images/uploadImage.gif" >
            </div>
            

           <!-- <div class="text-editor-circle" ng-controller="toolTipController" >
                <div class='editor-icons'>
                    
                    <div  class="xs-block-el item-common-menu xs-menu-bar-tag copy-button text-options-circle">
                        <a class="copy-layer tbar-menu-icon has-tip tip-right" data-attr="copy" data-tooltip aria-haspopup="true"  title="Copy">
                            <i class="flaticon-copy-two-paper-sheets-stroke-symbol-of-interface"></i> 

                        </a>
                    </div>
                    <div class="xs-block-el item-common-menu delete-button text-options-circle">
                        <a class="delete-layer-icon tbar-menu-icon has-tip tip-right" id="deleteLayerId" data-tooltip aria-haspopup="true"  title="Delete">
                            <i class="flaticon-rubbish-bin"></i>  

                        </a>
                    </div>
                    <div ng-if="canvasObjTypeCng == 'i-text'" class="xs-block-el item-common-menu xs-menu-bar-tag txt-spacing-slide-bx color-picker-button text-options-circle">
                        <a class="color-picker tbar-menu-icon has-tip tip-right"   data-type="textColor" data-class="item-text-editing-options"  data-tooltip aria-haspopup="true" title="Color picker">
                            <input type="text" class="color--picker " id="trigger1"  /> 
                            <i class="flaticon-color-picker"></i> 
                           <img src="https://image.flaticon.com/icons/png/64/138/138339.png" >
                        </a>
                        <div ng-if="canvasObjTypeCng == 'i-text'" class="xs-block-el item-common-menu xs-menu-bar-tag circle-inner-text item-text-editing-options tblr-inner-data" ng-controller="toolTipController">
                            <label class="text-editor-type">Select Color Type</label>
                            <li style="width:500px;border-radius:0;box-shadow:none;border:medium none;height:100%;list-style:none;">
                                <div class="xs_canvas_color_opt circle_col_options">
                                    <label><input type="radio" name="color_choose_for" value="text" checked="checked"> Text</label>
                                    <label><input type="radio" name="color_choose_for" value="stroke"> Stroke</label>
                                    <label><input type="radio" name="color_choose_for" value="gradient"> Gradient</label>
                                </div>
                            </li>
                            <label class="color-code"> Choose color code(hex code eg:000000) </label>
                            <input type="text" class="color--picker-input" value="000000" style="width:80px;" />
                            <button class="color-submit" type="button" name="color-submit" value="ok" >ok</button>
                        </div>
                        
                        
                        <input type="text" class="color--picker-input"/> 
                        
                    </div>
                    <div ng-if="canvasObjTypeCng == 'group'" class="xs-block-el item-common-menu xs-menu-bar-tag txt-spacing-slide-bx color-picker-button text-options-circle">
                        <a class="color-picker tbar-menu-icon has-tip tip-right"  data-type="textColor" data-class="item-button-editing-options"  data-tooltip aria-haspopup="true" title="Color picker">
                            <input type="text" class="color--picker" id="trigger1" /> 
                            <i class="flaticon-color-picker"></i> 
                            <img src="https://image.flaticon.com/icons/png/64/138/138339.png" >
                        </a>
                        <div ng-if="canvasObjTypeCng == 'group'" class="xs-block-el item-common-menu xs-menu-bar-tag circle-inner-text item-button-editing-options tblr-inner-data" ng-controller="toolTipController">
                            <label class="text-editor-type">Select Color Type</label>
                            <li style="width:500px;border-radius:0;box-shadow:none;border:medium none;height:100%;list-style:none;">
                                <div class="xs_canvas_color_opt">
                                    <label><input type="radio" name="button_choose_for" checked="checked" value="buttontext" > Button Text</label>                                                 
                                    <label><input type="radio" name="button_choose_for" value="button" > Button</label>

                                </div>
                            </li>
                            <label class="color-code"> Choose color code(hex code 000000) </label>
                            <input type="text" class="color--picker-input" value="000000" style="width:80px;" />
                            <button class="color-submit" type="button" name="color-submit" value="ok" >ok</button>
                        </div>
                    </div>    
                    
                    <div ng-if="canvasObjTypeCng == 'i-text'" class="xs-block-el item-common-menu xs-menu-bar-tag txt-spacing-slide-bx shadow-button text-options-circle">
                        <a class="text-shadow tbar-menu-icon has-tip tip-right" data-attr="text_shadow"  data-tooltip aria-haspopup="true" title="Shadow">
                            <i class="flaticon-square-shadow black-color"></i> 
                        </a>
                    </div>
                    <div ng-if="canvasObjTypeCng == 'i-text'" class="text-allignments text-options-circle ">
                        <a class=" tbar-menu-icon has-tip tip-right " data-class='item-alignments'  data-tooltip aria-haspopup="true"  title="Alignments">
                            <i class="flaticon-center-align"></i> 
                        </a>
                    </div>
                    <div ng-if="canvasObjTypeCng == 'i-text' || canvasObjTypeCng == 'group'" class="xs-block-el item-common-menu xs-menu-bar-tag txt-spacing-slide-bx font-style-button text-options-circle">
                        <a class="text-layer tbar-menu-icon has-tip tip-right" data-class='item-fonts' title="Font style"  data-tooltip aria-haspopup="true" >
                            <i class="flaticon-fontstyle"></i> 
                            <img src="https://image.flaticon.com/icons/png/512/364/364379.png">
                        </a>

                    </div>
                    <div  ng-if="canvasObjTypeCng == 'i-text' || canvasObjTypeCng == 'group'" class="xs-block-el item-common-menu xs-menu-bar-tag bold-button text-options-circle">
                        <a class="bold-layer-icon tbar-menu-icon has-tip tip-right" data-attr="bold"  data-tooltip aria-haspopup="true"  title="Bold">
                            <i class="flaticon-font-style-bold black-color"></i> 
                        </a>
                    </div>

                    <div ng-if="canvasObjTypeCng == 'i-text' || canvasObjTypeCng == 'group'" class="xs-block-el item-common-menu xs-menu-bar-tag italic-button text-options-circle">
                        <a class="italic-layer-icon tbar-menu-icon has-tip tip-right" data-attr="italic"  data-tooltip aria-haspopup="true"  title="Italic">
                            <i class="flaticon-italic-text-option-interface-symbol"></i> 

                        </a>
                    </div>-->
                    <!--                <div class="xs-block-el item-common-menu xs-menu-bar-tag transparent-slide-bx transparency-button text-options-circle">
                                        <a class="transparent-layer tbar-menu-icon has-tip tip-down" data-attr="transparancy" data-tooltip aria-haspopup="true"  title="Transparency">
                                            <i class="flaticon-diamond black-color"></i> 
                                        </a>
                                        <ul class="xs-popover-more-layer">
                                            <li>
                                                <ol class="transparancy-popover xs-popover">
                                                    <span class="set-transparency-label">Set Transparancy</span>
                                                    <div id="range-slider-filter" class="range-slider" data-slider data-options="display_selector: #transparancy-range-limit; start:1; initial: 100;">
                                                        <span class="range-slider-handle" role="slider" tabindex="0"></span>
                                                        <span class="range-slider-active-segment"></span>
                                                    </div>
                                                    <small id="transparancy-range-limit"></small>
                                                </ol>
                                            </li>
                                        </ul>
                                    </div>--> <!--
                    <div ng-if="canvasObjTypeCng == 'i-text'" ng-controller="toolTipController" class="xs-block-el item-common-menu xs-menu-bar-tag txt-spacing-slide-bx font-size-button text-options-circle">
                        <a class="text-layer tbar-menu-icon has-tip tip-right" data-class='item-font-sizes' title="Font size"  data-tooltip aria-haspopup="true" >
                            <i class="flaticon-fontsize"></i> 
                            <img src="https://image.flaticon.com/icons/png/64/186/186302.png">
                        </a>

                </div>
                </div>
                
                <div class ="circle-inner-content" style="display:none;">
                    <div ng-controller="leftNavBarController" ng-controller="toolTipController" class="xs-block-el mx-page-font-size font-sizes item-font-sizes tblr-inner-data" ng-if="canvasObjTypeCng == 'i-text'" >
                        <label>Select Font Size</label>
                        <select id="circle-font-size" onchange="plugin.setFontSize(this.value)" class="select-dropbox">
                            <option ng-repeat="fontSize in fontSizeLimit" value="{{fontSize}}">{{fontSize}}</option>
                        </select>
                    </div>
                    <div  class="xs-block-el mx-page-font-family custom-fonts item-fonts tblr-inner-data" ng-controller="toolTipController" ng-if="canvasObjTypeCng == 'i-text' || canvasObjTypeCng == 'group'">
                        <label>Select Font Family</label>
                        <select onchange="plugin.setFont(this.value)" id="circle-font-family">
                            <option value="arial" selected="">Arial</option>
                            <option value="Abril Fatface" selected="">Abril Fatface</option>
                            <option value="Asap">Asap</option>
                            <option value="Bree Serif">Bree Serif</option>
                            <option value="Courgette">Courgette</option>
                            <option value="Dancing Script">Dancing Script</option>
                            <option value="Gentium Basic">Gentium Basic</option>
                            <option value="Lobster">Lobster</option>
                            <option value="Lobster Two">Lobster Two</option>
                            <option value="Merriweather">Merriweather</option>
                            <option value="Oleo Script">Oleo Script</option>
                        </select>

                    </div> 
                    <div id='aligns' class="alignments item-alignments tblr-inner-data" ng-controller="toolTipController">
                        <label>Align Text</label>
                        <div ng-if="canvasObjTypeCng == 'i-text'" class="xs-block-el item-common-menu xs-menu-bar-tag ">
                            <a class="left-layer-icon tbar-menu-icon  has-tip tip-down"  data-attr="left" data-tooltip aria-haspopup="true"  title="Left">
                                <span>Left <i class="flaticon-left-alignment-2"></i> </span>
                            </a>
                        </div>
                        <div ng-if="canvasObjTypeCng == 'i-text'" class="xs-block-el item-common-menu xs-menu-bar-tag ">
                            <a class="center-layer-icon tbar-menu-icon has-tip tip-down"  data-attr="center" data-tooltip aria-haspopup="true"  title="Center">
                                <span>Center <i class="flaticon-center-align"></i> </span>

                            </a>
                        </div>
                        <div ng-if="canvasObjTypeCng == 'i-text'" class="xs-block-el item-common-menu xs-menu-bar-tag " >
                            <a class="right-layer-icon tbar-menu-icon has-tip tip-down" data-attr="right" data-tooltip aria-haspopup="true"  title="Right">
                                <span>Right <i class="flaticon-right-alignment-1"></i> </span>
                            </a>
                        </div>
                    </div>
<!--                    <div ng-if="canvasObjTypeCng == 'i-text'" class="xs-block-el item-common-menu xs-menu-bar-tag item-text-editing-options tblr-inner-data" ng-controller="toolTipController">
                        <label class="text-editor-type">Select Color Type</label>
                        <li style="width:500px;border-radius:0;box-shadow:none;border:medium none;height:100%;list-style:none;">
                            <div class="xs_canvas_color_opt circle_col_options">
                                <label><input type="radio" name="color_choose_for" value="text" checked="checked"> Text</label>
                                <label><input type="radio" name="color_choose_for" value="stroke"> Stroke</label>
                                <label><input type="radio" name="color_choose_for" value="gradient"> Gradient</label>
                            </div>
                        </li>
                        <label class="color-code"> Choose color code(hex code eg:000000) </label>
                        <input type="text" class="color--picker-input" value="000000" style="width:80px;" />
                        <button class="color-submit" type="button" name="color-submit" value="ok" >ok</button>
                    </div>-->
<!--                    <div ng-if="canvasObjTypeCng == 'group'" class="xs-block-el item-common-menu xs-menu-bar-tag item-button-editing-options tblr-inner-data" ng-controller="toolTipController">
                        <label class="text-editor-type">Select Color Type</label>
                        <li style="width:500px;border-radius:0;box-shadow:none;border:medium none;height:100%;list-style:none;">
                            <div class="xs_canvas_color_opt">
                                <label><input type="radio" name="button_choose_for" checked="checked" value="buttontext" > Button Text</label>                                                 
                                <label><input type="radio" name="button_choose_for" value="button" > Button</label>

                            </div>
                        </li>
                        <label class="color-code"> Choose color code(hex code 000000) </label>
                        <input type="text" class="color--picker-input" value="000000" style="width:80px;" />
                        <button class="color-submit" type="button" name="color-submit" value="ok" >ok</button>
                    </div>
                </div>
            </div> circle div close-->
            <?php if(!empty($designFromTemplateData->frames[0])) { ?>
                <input type="hidden" id="canvas_data" value='<?php echo $designFromTemplateData->frames[0]->frame_data; ?>'>
            <?php } ?>
            <!--<input type="hidden" id="_canvas_data" value='<?php echo Session::get('_canvas_data'); ?>'>-->
        <?php endif ?>
        <!--Add Fabric Js -->
        <script src="<?php echo url(); ?>/js/fabric.min.js"></script>
        <!--<script src="<?php echo url(); ?>/js/fabric1.7/fabric.min.js"></script>-->
        <!--<script src="<?php echo url(); ?>/js/customiseControls.js"></script>-->
        <script src="<?php echo url(); ?>/js/customization.canvas.js"></script>
        <script src="<?php echo url(); ?>/js/foundation.min.js"></script>
        <script src="<?php echo url(); ?>/js/spectrum.js"></script>
        <script src="<?php echo url(); ?>/js/script.js"></script>
        <!--        <script src="js/perfect-scrollbar.min.js"></script>-->
        <!--End of fabric JS-->
        <script>

                                            var intervalAccord;
                                            function intervalAccordian() {
                                            if ($("#menu_3d_container>section ").length > 0) {
                                            $(document).foundation();
                                                    $(document).foundation('accordion', 'reflow');
                                                    clearInterval(intervalAccord);
                                            }

                                            }
                                    setInterval(function () {
                                    intervalAccordian()
                                    }, 1000);

        </script>
        <?php // echo Session::set('_canvas_data', ''); ?>
       
    </body>
</html>
