@include('includes.header-blank')
<div class="menu-toggler sidebar-toggler"></div>
<div class="logo">
    <a href="{{ URL::previous() }}">
        <i class="fa fa-backward"></i>
        <span class="title">Go Back</span>
        <span class="selected"></span>
    </a>
</div>

<div class="content">
    <form class="register" action="<?php echo url(); ?>/auth/register" method="post">
        {!! csrf_field() !!}
        <h3 class="font-green">Sign Up</h3>
        <div>
            <ul style="list-style: none;">
                @foreach ($errors->all() as $error)
                <li style="color:red;">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Full Name <span class="xs-astric">*</span></label>
            <input required="" class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="first_name" /> </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Last Name <span class="xs-astric">*</span></label>
            <input required="" class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="last_name" /> </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email <span class="xs-astric">*</span></label>
            <input required="" required="" class="form-control placeholder-no-fix" type="email" placeholder="Email" name="email" /> </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password <span class="xs-astric">*</span></label>
            <input required="" class="form-control placeholder-no-fix" type="password" placeholder="Password" name="password" /> </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Confirm Password <span class="xs-astric">*</span></label>
            <input required="" class="form-control placeholder-no-fix" type="password" placeholder="Confirm Password" name="password_confirmation" /> </div>
        <button class="btn btn-lg btn-success" name="submit" type="submit">Register</button>

    </form>
    <!-- END REGISTRATION FORM -->
</div>

@include('includes.footer-blank')