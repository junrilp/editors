@include('includes.header-blank')

<div class="menu-toggler sidebar-toggler"></div>
<div class="logo">
    <!--<a href="index.html">-->
        <!--<img src="<?php //echo url();   ?>/assets/pages/img/logo-big.png" alt="" /> </a>-->
</div>
<div class="content">
    <form class="login-form" action="<?php echo url(); ?>/auth/login" method="post"> 
        {!! csrf_field() !!}
        <h3 class="form-title font-green">Sign In</h3>
        @if($errors->has('email'))
            {!! $errors->first('email', '<div class="alert alert-danger error-block "><button class="close" style="margin-top:6px;" data-close="alert"></button><span>:message</span></div>') !!}
        @else
        <div class="alert alert-danger display-hide">
            <button class="close" style="margin-top:6px;" data-close="alert"></button>
            <span> Enter your Username and Password. </span>
            
        </div>
        @endif
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="email" /> 
            
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" />
        </div>
<!--        <div style="display:none;">
            <ul style="list-style: none;">
                <li style="color:red;">These credentials do not match our records.</li>
            </ul>
        </div>-->
        
        <div class="form-actions">
            <button type="submit" class="btn green uppercase">Login</button>
            <label class="rememberme check">
                <input style="margin: 4px 5px 0 !important;" type="checkbox" name="remember" value="1" />Remember 
            </label>

        </div>
        <div style="display: block;width: 100%;text-align: center;top: -10px;position: relative;">
            {!! link_to('password/email', 'Forgot Your Password?') !!}
        </div>
        <!--                <div class="login-options">
                            <h4>Or login with</h4>
                            <ul class="social-icons">
                                <li>
                                    <a class="social-icon-color facebook" data-original-title="facebook" href="javascript:;"></a>
                                </li>
                                <li>
                                    <a class="social-icon-color twitter" data-original-title="Twitter" href="javascript:;"></a>
                                </li>
                                <li>
                                    <a class="social-icon-color googleplus" data-original-title="Goole Plus" href="javascript:;"></a>
                                </li>
                                <li>
                                    <a class="social-icon-color linkedin" data-original-title="Linkedin" href="javascript:;"></a>
                                </li>
                            </ul>
                        </div>-->
        <div class="create-account">
            <p>
                {!! link_to('auth/register', 'Create an Account') !!}
            </p>
        </div>
    </form>
</div>

@include('includes.footer-blank')