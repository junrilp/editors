<!DOCTYPE html>
@include('includes.header-blank')
<div class="menu-toggler sidebar-toggler"></div>
<div class="logo">
    <!--<a href="index.html">-->
        <!--<img src="<?php //echo url();    ?>/assets/pages/img/logo-big.png" alt="" /> </a>-->
</div>
<div class="content">
    <div class="panel-body">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif

        {!! Form::open(['to' => 'password/email', 'class' => 'form-horizontal', 'role' => 'form']) !!}
        <h3 class="form-title font-green">Reset Password</h3>
        <div class="form-group">
            <div class="col-md-12 col-sm-12 col-lg-12">
                {!! Form::input('email', 'email', old('email'), ['class' => 'form-control','placeholder'=>'Please enter you Email']) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12 col-sm-12 col-lg-12" style="text-align: center;"> 
                {!! Form::submit('Send Password Reset Link', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>    
</div>

@include('includes.footer-blank')