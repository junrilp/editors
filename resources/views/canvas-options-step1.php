<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Online Editor</title>
        <link href="<?php echo url(); ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="<?php echo url(); ?>/css/canvas-options-step.css" type="text/css">
        <script src="<?php echo url(); ?>/js/jquery.js"></script>
        <script src="<?php echo url(); ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

        <script src="<?php echo url(); ?>/js/modernizr.js"></script>
        <script src="<?php echo url(); ?>/js/jquery-ui.js"></script>
        <script src="<?php echo url(); ?>/js/angular.min.js"></script>
        <script src="<?php echo url(); ?>/js/bootstrap-colorpicker-module.js"></script>
        <script src=" <?php echo url(); ?>/js/angular-animate.min.js"></script>
        <script src="<?php echo url(); ?>/js/app-controller.js"></script> 

        <!--<link rel="stylesheet" href="css/perfect-scrollbar.css" />-->

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo url(); ?>/css/foundation.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/colorpicker.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/app.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/app.animation.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo url(); ?>/css/spectrum.css" type="text/css" />
    </head>
    <body ng-app="editorApp">
        <!-- Nav tabs -->

        <?php if (empty($templateSizeLayouts)) { ?>
            <div class="tabs-content" ng-controller="leftNavBarController">
                <section role="tabpanel" aria-hidden="true" class="content active" id="tab1">
                    <div class="text-center">
                        <div class="started-editor">
                            <h4>No Sizes Choosen </h4>
                            <h5>Please Contact to administrator</h5>
                            <a href="/mockup3d">Create your own canvas size</a>
                        </div>
                    </div>
                </section>
            </div>

        <?php } else { ?>
            <div class="card">
                <ul class="nav nav-tabs tabs-navbar" role="tablist">
                    <li><img alt="logo" src="<?php echo url(); ?>/assets/layouts/layout/img/banner-logo.jpg"></li>
                    <li class="active get-custom-canvas" role="presentation">
                        <a aria-controls="home" data-toggle="tab" href="#home" role="tab">CUSTOM CANVASES</a>
                    </li>
                    <li role="presentation" class="get-init-canvas">
                        <a aria-controls="profile" data-toggle="tab" href="#profile" role="tab">PRE DESIGNED TEMPLATES</a>
                    </li>
                    
                        <li class="dropdown dropdown-user pull-right">
                            <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                @if(Auth::user()->profile_pic !='')
                                {!! Html::image('users_images/'.Auth::user()->profile_pic, 'image', array(  'height' => 80 ))  !!}
                                @else
                                {!! Html::image('images/avatar.png', 'image') !!}
                                @endif
                                <span class="username">
                                    {{  ucfirst(Auth::user()->first_name) }} </span>
                                <i class="fa fa-angle-down"></i>
                            </a>-->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <span class="username"><?=  ucfirst(Auth::user()->first_name)?> </span>
                                <span class="username-icon"><i class="fa fa-user" style="color: #FE048A !important;"></i></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo url() . '/users/' . (Auth::user()->id) . '/edit'; ?>"><i class="icon-user"></i> View Profile</a>
                                </li>
                                <li>
                                    <a href="<?php echo url() . '/auth/logout'; ?>"><i class="icon-lock"></i> Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                <div class="tab-content content-section">
                    <div class="tab-pane active" id="home" role="tabpanel">
                        <div class="col-xs-12 col-sm-3 col-md-2 sidebar-tabs">
                            <button class="btn btn-primary primary-button" type="button" data-toggle="modal" data-target="#canvasSizeModal" >+ New Custom Size</button>
                            <ul class="sidebar-text">
                                <?php if (count($categories) > 0) { ?>
                                    <?php foreach ($categories as $key => $category) { ?>
                                        <li class="get-category-data">
                                            <a data-id="<?php echo $key; ?>" href="javascript:void(0);"><?php echo $category; ?></a>
                                        </li>
                                    <?php } ?> 
                                <?php } ?> 
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-10 right-section">
                            
                           
                            <div class="no-category-data-msg"> 
                            
                                <div class="loadingpage" id="loadingToolPage">
                                    <div class="loading-gif">
                                        <a class="loadingAnchorTag"></a>
                                        <p>Loading Please wait....</p>
                                    </div>
                                </div>
                                <div class="box-section category-data-ajax " id=""><h3>My banners</h3></div>
                                <div class="box-section category-trending-data-ajax " id=""><div class="size-heading">
                                <h3><span class="title-head">banner</span> - Select a size to get started</h3>
                            </div>
                            </div>
                            </div>
                        </div>
                    </div> 
                    <div class="tab-pane" id="profile" role="tabpanel">
                        <div class="col-xs-12 col-sm-3 col-md-2 sidebar-tabs">
                            <button class="btn btn-primary primary-button" type="button" data-toggle="modal" data-target="#canvasSizeModal">+ New Custom Size</button>
                            <ul class="sidebar-text">
                                <?php if (count($templateSizeLayouts) > 0) { ?>
                                    <?php foreach ($templateSizeLayouts['dimensions'] as $dimension) { ?>
                                        <li class="get-canvas-data">
                                            <a data-id="<?php echo $dimension->width . '|' . $dimension->height; ?>" href="javascript:void(0);"><?php echo $dimension->width . 'X' . $dimension->height; ?></a>
                                        </li>
                                    <?php } ?> 
                                <?php } ?> 
                            </ul>
                        </div>

                        <div class="col-xs-12 col-sm-9 col-md-10 right-section">
                            <div class="size-heading">
                                <h1>Select a size to get started</h1>
                                <p class="dim-title">1000X1000</p> 
                            </div>
                            <div class="no-canvas-data-msg">
                                <div class="loadingpage" id="canvasloadingToolPage">
                                    <div class="loading-gif">
                                        <a class="loadingAnchorTag"></a>
                                        <p>Loading Please wait....</p>
                                    </div>
                                </div>
                                <div class="box-section canvas-data-ajax " id=""><h3>My banners</h3></div>
                                <div class="box-section canvas-trending-data-ajax " id=""><h3>Templates</h3></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Choose canvas size Modal -->
            <div id="canvasSizeModal" class="modal fade" role="dialog"> 
                <div class="modal-dialog">
                    <!-- Modal content--> 
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center">Choose Size</h4>
                        </div>
                        <div class="modal-body tabs-content"  ng-controller="leftNavBarController">

                            <div class="form-group">
                                <?php if (count($categories) > 0) { ?>
                                    <select name="category_id" class="custom-category-select"> 
                                        <option value="">Select Category</option>
                                        <?php foreach ($categories as $key => $category) { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $category; ?></option>
                                        <?php } ?>  
                                    <?php } ?> 

                                </select>
                            </div>
                            <div class="text-center">
                                <div class="started-editor">
                                    <h4>Click on a size to get started</h4>
                                    <div class="large-12 columns button-section">
                                        <ul>
                                            <?php foreach ($templateSizeLayouts['dimensions'] as $template) { ?>
                                                <li>
                                                    <a class="button radius" source-type="1" data-height="<?php echo $template->height; ?>" data-width="<?php echo $template->width; ?>"><?php echo $template->width . ' &#10005; ' . $template->height; ?>
                                                        <input style="visibility:hidden;" type="radio" name="dimensions" value="<?php echo $template->width . '_' . $template->height; ?>" >
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="enter-editor-size ">

                                    <h4>OR</h4>
                                    <p>enter your own custom size</p>
                                    <form>
                                        <ul>
                                            <li><input class="width" type="text" placeholder="width" /></li>
                                            <li  style="margin: 11px;position: relative;">&#10005 </li>
                                            <li><input class="height" type="text" placeholder="height" /></li>
                                        </ul>
                                    </form>
                                    <div class="continue-button">
                                        <a href="#" class="button expand" ng-click="setCanvasDim()">CONTINUE</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer" >

                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div id="previewModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Preview</h4>
                    </div>
                    <div class="modal-body canvas-img">
                        <img src ="">
                        <div class="frame-data" id=""></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>    


        <form id="oeditorForm" style="">
            <input type="hidden" value="<?php echo csrf_token(); ?>" name="_token">
            <input type="hidden" name="_m_user_id" id="_m_user_id" value="<?php echo Auth::user()->id; ?>">
            <input type="hidden" name="_m_canvas_id" id="_m_canvas_id" value="0">
            <input type="hidden" name="title" id="_user_canvas_title" value=":Untitled Project">
            <input type="hidden" name="dimention" id="_user_canvas_dimention" value="">
            <input type="hidden" name="canvas_data" id="_canvas_data_json" value="">
            <input type="hidden" name="category_id" id="_category_id" value="">
            <input type="hidden" name="img_data_source" id="_canvas_img_souce" value="">
        </form>

        <!--Add Fabric Js -->
        <script src="<?php echo url(); ?>/js/fabric.min.js"></script>
        <script src="<?php echo url(); ?>/js/customiseControls.js"></script>
        <script src="<?php echo url(); ?>/js/customization.canvas.js"></script>
        <script src="<?php echo url(); ?>/js/foundation.min.js"></script>
        <script src="<?php echo url(); ?>/js/spectrum.js"></script>
        <script src="<?php echo url(); ?>/js/script.js"></script>
        <script src="<?php echo url(); ?>/js/isotope.pkgd.min.js"></script>

<!--     <script src="js/perfect-scrollbar.min.js"></script>-->
        <!--End of fabric JS-->
        <script>
                                        $(document).ready(function() {
                                            $('input[name="dimensions"]:checked').parent('a').addClass("is-selected");
                                            $("#designfromtemplate").on("click", function() {
                                                setTimeout(function() {
                                                    $("#canvasDimensions").find("ul li:first-child a").trigger("click");
                                                }, 50);
                                            });
                                            $(".button-section").on('click', 'ul li a', function() {
                                                var canvasDimensions = $(this).find('input').val().split('_');
                                                var val = $(this).find('input:radio').prop('checked') ? false : true;
                                                $(this).find('input:radio').prop('checked', val);
                                                $('.button-section').find('a').removeClass('is-selected')
                                                if (val) {
                                                    $(this).addClass('is-selected')
                                                    $('.width').val(canvasDimensions[0]);
                                                    $('.height').val(canvasDimensions[1]);
                                                }
                                                localStorage.setItem('sourceType', 1);
                                            });
                                        });


                                        var intervalAccord;
                                        function intervalAccordian() {
                                            if ($("#menu_3d_container>section ").length > 0) {
                                                $(document).foundation();
                                                $(document).foundation('accordion', 'reflow');
                                                clearInterval(intervalAccord);
                                            }

                                        }
                                        setInterval(function() {
                                            intervalAccordian()
                                        }, 1000);

                                        //Isotope for grid viewvar 
                                        $grid = $('.grid').isotope({
                                            itemSelector: '.grid-item-value',
                                            layoutMode: 'fitRows'
                                        });

                                        // store filter for each group
                                        var filters = {};

                                        $('.filter').on('click', function() {
                                            $('.NoLayouts').remove();
                                            filters[ 'sizes' ] = $(this).attr('data-filter');
                                            // combine filters
                                            var filterValue = concatValues(filters);
                                            // set filter for Isotope
                                            $grid.isotope({filter: filterValue});
                                            if (!$grid.data('isotope').filteredItems.length) {
                                                $('.NoLayouts').remove();
                                                $('.grid').append('<div class="NoLayouts">No Layouts</div>');
                                            }
                                        });

                                        // change is-checked class on buttons
                                        $('.button-grid').each(function(i, buttonGroup) {
                                            var $buttonGroup = $(buttonGroup);
                                            $buttonGroup.on('click', function() {
                                                $('.button-grid').removeClass('is-checked');
                                                $(this).addClass('is-checked');
                                            });
                                        });

                                        // flatten object by concatting values
                                        function concatValues(obj) {
                                            var value = '';
                                            for (var prop in obj) {
                                                value += obj[ prop ];
                                            }
                                            return value;
                                        }
        </script>
        <?php echo Session::set('_canvas_data', ''); ?>

    </body>
</html>
<script>
    $(document).ready(function() {
        $('.get-category-data:eq("0")').addClass('active');
        $('body').on('click', 'li.get-canvas-data, li.get-category-data', function() {
            $(this).addClass('active').siblings().removeClass('active');
        });

        $('.get-init-canvas').click(function() {
            $("#canvasloadingToolPage").show();
            $('.get-canvas-data:eq("0")').trigger("click");
            $('.get-canvas-data').removeClass('active')
            $('.get-canvas-data:eq("0")').addClass('active');
        });

        $('.get-custom-canvas').click(function() {
            $("#loadingToolPage").show();
            $('.get-category-data:eq("0")').trigger("click");
            $('.get-category-data').removeClass('active');
            $('.get-category-data:eq("0")').addClass('active');
        });

        //get the templates according to selected category
        $('.get-category-data').click(function() {
            var category = $(this).find('a').data("id");
            var catTitle = $(this).find('a').text();
            $("p.cat-title").text(catTitle);
            $("span.title-head").text("My " + catTitle);
            $(".category-trending-data-ajax").css("display", "none");
            $(".category-data-ajax").css("display", "none");
            $("#loadingToolPage").show();
            $(".no-category-data-msg ul").remove();
//            $(".category-trending-data-ajax ul").remove();
            var params = {
                canvas_params: JSON.stringify(category)
            };
            categoryAjaxResponse(params);
        });


        var canvasDetailData = {};
        //ajax call to get the categories
        function categoryAjaxResponse(params) {
            $.ajax({
                url: '/getCategoryAjax',
                type: 'get',
                dataType: 'JSON',
                data: params,
                success: function(res) {
                    $(".category-trending-data-ajax").css("display", "none");
                    $(".category-data-ajax").css("display", "none");
                    if (res.success == 1) {

                        var data = "", trending_data = "";
                        trending_data += '<ul class="fixed-box ">';
                        data += '<ul class="fixed-box ">';
                        $.each(res.data.layouts, function(index, element) {
                            canvasDetailData[element.id] = element;
                            if (element.user_id == 1) {
                                if ($(".category-trending-data-ajax").css("display", "none")) {
                                    $(".category-trending-data-ajax").css("display", "block");
                                }
                                var d  = "dummy.jpg";
                                //data +='<div id="gridloader" style="display:none; text-align: center;"><img src="<?php echo url(); ?>/images/ajax-loader.gif" ></div>'
                                trending_data += '<li class="square grid-item-value ' + element.width + '_' + element.height + '">';
                                trending_data += '<div class="card-block-wrapper">';
                                trending_data += '<a class="card-block-img" href="#" style="background-image:url( '+ element.frame_image[0] +')" source_id = '+ element.id +'></a>';
                                trending_data += '<div class="customize-preview-section"><a href="javascript:void(0);" class="customize-template" source_id = '+ element.id +'><i class="fa fa-paint-brush" aria-hidden="true"></i>Customize</a><a data-target="#previewModal" data-toggle="modal" href="javascript:void(0);" data-id="' + element.id + '" class="preview-template"><i class="fa fa-eye" aria-hidden="true"></i>Preview</a></div>';
                                trending_data += '</div>';
                                trending_data += '<div class="canvas-details">'
                                trending_data += ' <h4 class="project-title">' + element.project_title + '</h4>';
                                trending_data += ' <h5>' + element.width + 'x' + element.height + '</h5>';
//                                trending_data += ' <h5>' +'edited by:' + element.first_name + '</h5>';
                                trending_data += '</div>'
                                trending_data += '</li>';
                            } else {
                                if ($(".category-data-ajax").css("display", "none")) {
                                    $(".category-data-ajax").css("display", "block");
                                }
                                data += '<li class="square grid-item-value ' + element.width + '_' + element.height + '">';
                                data += '<div class="card-block-wrapper">';
                                data += '<a class="card-block-img" href="#" style="background-image:url('+ element.frame_image[0] +')" source_id = '+ element.id +'></a>';
                                data += '<div class="customize-preview-section"><a href="javascript:void(0);" class="customize-template" source_id = '+ element.id +'><i class="fa fa-paint-brush" aria-hidden="true"></i>Customize</a><a data-target="#previewModal" data-toggle="modal" href="javascript:void(0);" data-id="' + element.id + '" class="preview-template"><i class="fa fa-eye" aria-hidden="true"></i>Preview</a></div>';
                                data += '</div>';
                                data += '<div class="canvas-details">'
                                data += ' <h4 class="project-title">' + element.project_title + '</h4>';
                                data += ' <h5>' + element.width + 'x' + element.height + '</h5>';
//                                data += ' <h5>' +'edited by:' + element.first_name + '</h5>';
                                data += '</div>'
                                data += '</li>';
                            }
                        });
                        data += '</ul>';
                        $(".category-trending-data-ajax").append(trending_data);
                        $(".category-data-ajax").append(data);
                        $("#loadingToolPage").hide();
                    } else {
                        $("#loadingToolPage").hide();
                        $(".no-category-data-msg").append('<ul class="data-msg" ><li>' + res.msg + '</li></ul>');
                    }

                }

            });
            return false;
        }


        //get the templates according to selected size
        $('.get-canvas-data').click(function() {
            var dimension = $(this).find('a').data("id");
            var headingRender = $(this).find('a').data("id").split('|');
            $("p.dim-title").text(headingRender[0] + 'x' + headingRender[1]);
            $(".canvas-trending-data-ajax").css("display", "none");
            $(".canvas-data-ajax").css("display", "none");
            $("#canvasloadingToolPage").show();
            $(".no-canvas-data-msg ul").remove();
//            $(".canvas-trending-data-ajax ul").remove();
            var params = {
                canvas_params: JSON.stringify(dimension)
            };
            canvasajaxResponse(params);
        });
        var ajaxResponse;

        //ajax call to get the canvas sizes
        function canvasajaxResponse(params) {
            $.ajax({
                url: '/getCanvasAjax',
                type: 'get',
                dataType: 'JSON',
                data: params,
                success: function(res) {
                    $(".canvas-trending-data-ajax").css("display", "none");
                    $(".canvas-data-ajax").css("display", "none");
                    ajaxResponse = res
                    if (ajaxResponse.success == 1) {

                        var data = "", trending_data = "";
                        trending_data += '<ul class="fixed-box ">';
                        data += '<ul class="fixed-box ">';
                        $.each(res.data.layouts, function(index, element) {
                            canvasDetailData[element.id] = element;
                            if (element.user_id == 1) {
                                if ($(".canvas-trending-data-ajax").css("display", "none")) {
                                    $(".canvas-trending-data-ajax").css("display", "block");
                                }
                                //data +='<div id="gridloader" style="display:none; text-align: center;"><img src="<?php echo url(); ?>/images/ajax-loader.gif" ></div>'
                                trending_data += '<li class="square grid-item-value ' + element.width + '_' + element.height + '">';
                                trending_data += '<div class="card-block-wrapper">';
                                trending_data += '<a class="card-block-img" href="#" style="background-image:url('+ element.frame_image[0] +')" source_id = '+ element.id +'></a>';
                                trending_data += '<div class="customize-preview-section"><a href="#" class="customize-template" source_id = '+ element.id +'><i class="fa fa-paint-brush" aria-hidden="true"></i>Customize</a><a data-target="#previewModal" data-toggle="modal" href="javascript:void(0);" data-id="' + element.id + '" class="preview-template"><i class="fa fa-eye" aria-hidden="true"></i>Preview</a></div>';
                                trending_data += '</div>';
                                trending_data += '<div class="canvas-details">'
                                trending_data += ' <h4 class="project-title">' + element.project_title + '</h4>';
                                trending_data += ' <h5>' + element.width + 'x' + element.height + '</h5>';
//                                trending_data += ' <h5>' +'edited by:' + element.first_name + '</h5>';
                                trending_data += '</div>';
                                trending_data += '</li>';
                            } else {
                                if ($(".canvas-data-ajax").css("display", "none")) {
                                    $(".canvas-data-ajax").css("display", "block");
                                }
                                data += '<li class="square grid-item-value ' + element.width + '_' + element.height + '">';
                                data += '<div class="card-block-wrapper">';
                                data += '<a class="card-block-img" href="#" style="background-image:url('+ element.frame_image[0] +')" source_id = '+ element.id +'></a>';
                                data += '<div class="customize-preview-section"><a href="javascript:void(0);" class="customize-template" source_id = '+ element.id +'><i class="fa fa-paint-brush" aria-hidden="true"></i>Customize</a><a data-target="#previewModal" data-toggle="modal" href="javascript:void(0);" data-id="' + element.id + '" class="preview-template"><i class="fa fa-eye" aria-hidden="true"></i>Preview</a></div>';
                                data += '</div>';
                                data += '<div class="canvas-details">'
                                data += ' <h4 class="project-title">' + element.project_title + '</h4>';
                                data += ' <h5>' + element.width + 'x' + element.height + '</h5>';
//                                data += ' <h5>' +'edited by:' + element.first_name + '</h5>';
                                data += '</div>'
                                data += '</li>'; 
                            }
                        });
                        data += '</ul>';
                        $(".canvas-trending-data-ajax").append(trending_data);
                        $(".canvas-data-ajax").append(data);
                        $("#canvasloadingToolPage").hide();
                    } else {
                        $("#canvasloadingToolPage").hide();
                        $(".no-canvas-data-msg").append('<ul class="data-msg"><li>' + res.msg + '</li></ul>');
                    }
                }

            });
            return false;
        }

        $('body').on("change", ".custom-category-select", function() {
            var categoryId = this.value;
            $("#_category_id").val(categoryId);
        });


        $("body").on("click", ".grid-item-value a.preview-template", function() {
            $(".frame-data ul").remove();
            $('.canvas-img img').attr("src", canvasDetailData[$(this).attr('data-id')].frame_image[0]);
            var data = "";
            data += '<ul class="frame-box">';
            $.each(canvasDetailData[$(this).attr('data-id')].frame_image, function(index, element) {
                data += '<li>'
                data += '<a href="#"><img src="' + element + '" width="' + 100 + '" height="' + 200 + '" /></a>';
                data += '</li>';

            });
            data += '</ul>';
            $(".frame-data").append(data);

        });
        $('body').on("click", "li.grid-item-value", function() {
            var source_id = $(this).find('img').attr('source_id');
            var width = $(this).find('img').attr('width');
            var height = $(this).find('img').attr('height');

            $('#grid-layouts .grid-item img').attr('source_id', source_id)
            $('#grid-layouts .grid-item img').attr('width', width)
            $('#grid-layouts .grid-item img').attr('height', height)
        });
        
        $('body').on("click", ".frame-box li a img", function() {
            var selectedFrame = $(this).attr('src');
            console.log(selectedFrame)
            $('.canvas-img').children("img").attr("src",selectedFrame);
        });
        


        $('#myModal').on('shown.bs.modal', function() {
            $("#_category_id").val("");
        });

        $('#myModal').on('hidden.bs.modal', function() {
            if ($("#_category_id").val() != '') {
                $("#grid-layouts .grid-item").trigger("click");
                return;
            }
            $('#grid-layouts .grid-item img').attr('source_id', "")
            $('#grid-layouts .grid-item img').attr('width', "")
            $('#grid-layouts .grid-item img').attr('height', "")
            alert("Please select category")
        });
    });
</script>
<style type="text/css">
    .size-heading h3{
        margin-top: 0px;
        color: #727071;
        font-size: 30px;
        font-weight: 200;
        padding-left: 8px;
    }
    span.title-head{
        color:black;
        font-weight: bold;
    }
</style>