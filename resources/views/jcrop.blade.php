@extends ('app')

@section('page-header')
@endsection
@section('content')

<div class="panel panel-default">
    <div class="panel-heading">Please crop the image</div>
    <div class="panel-body">
        <?= Form::open(['class' => 'form-horizontal', 'url' => '/users/jcrop', 'files' => true]) ?>
        <p class="xs-form-bar"><?= Form::submit('Crop it!', ['class' => 'btn btn-info jcropSubmit']) ?></p>
        <img src="<?php echo $image ?>" id="cropimage" class="crop_img">

        <div class="row">
            <?= Form::hidden('image', $image) ?>
            <?= Form::hidden('user_id', $user_id) ?>
            <?= Form::hidden('ext', $ext) ?>
            <?= Form::hidden('x', '', array('id' => 'x')) ?>
            <?= Form::hidden('y', '', array('id' => 'y')) ?>
            <?= Form::hidden('w', '', array('id' => 'w')) ?>
            <?= Form::hidden('h', '', array('id' => 'h')) ?>
        </div>
        <p class="xs-form-bar"><?= Form::submit('Crop it!', ['class' => 'btn btn-info jcropSubmit']) ?></p>
        <?= Form::close() ?>
    </div>

</div>

<script type="text/javascript">
    $(function() {
        $('#cropimage').Jcrop({
            onSelect: updateCoords,
            onChange: updateCoords,
            setSelect: [100, 100, 30, 30],
//            bgOpacity: .4,
            allowSelect: true,
            allowMove: true,
            allowResize: true,
            aspectRatio: 0, //If you want to keep aspectRatio
//            boxWidth: 1024, //Maximum width you want for your bigger images
//            boxHeight: 0,
            
        });
    });
    function updateCoords(c) {

        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    };
</script>
@stop