@include('includes.header-theme')
<div class="page-container">
    @include('includes.sidebar-theme')
    @include('includes.content-theme')
    @include('includes.quick-sidebar-theme')
</div>
<!--
<section class="content-header container">
    @yield('page-header-theme')
</section>-->
@include('includes.footer-theme')
