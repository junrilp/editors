@extends ('app')
@section('content')
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active">
        <!-- <div class ="description">
             List of saved templates by the users.
             
         </div>-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>My Designs</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body flip-scroll">
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content">
                        @if($canvas && count($canvas) >= 1)
                        <tr>
                            <!--<th>ID</th>-->
                            <th>User Name</th>
                            <th>Project Title</th>
                            <th>Created Date</th>
                            <th>Last Updated at</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($canvas as $can)
                        <tr>
                            <!--<td>{!! $can->id !!}</td>-->
                            <td>{!! $can->users['first_name'] !!} {!!$can->users['last_name']!!}</td>
                            <td>{!! $can->project_title !!}</td>
                            <td>{!! $can->created_at->format('d-M, Y  (H:i:s)') !!}</td>
                            <td>{!! $can->updated_at->diffForHumans() !!}</td>
                            <td> 
                                <a class="app-operation-left" target="_blank" href="/editor/{{ $can->id }}"> 
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                                {!! Form::open(['method' => 'DELETE', 'class'=>'del-css', 'route' => array('canvas.destroy', $can->id),'onsubmit' => 'return ConfirmDelete()']) !!}
                                <button type="submit" class="submit-btn-cls"> 
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                {!! Form::close() !!} 
                            </td>
                        </tr>

                        @endforeach
                        @else
                    <h3 align='center'>Nothing found!</h3>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="pull-left">
    {!! $canvas->total() !!} canvas(s) total
</div>

<div class="pull-right">
    {!! $canvas->render() !!}
</div>
<div class="clearfix"></div>
<script>
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete this Canvas ?. This will not be recoverable press OK to continue.");
        if (x)
            return true;
        else
            return false;
    }
</script>
@stop