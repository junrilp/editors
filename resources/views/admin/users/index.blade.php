@extends ('app')
@section('content')
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active">
        <div class ="description">
            <!-- List of all registered user shown here, you can manage users from here.-->
        </div>
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i> Users List</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body flip-scroll">
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content" >
                        <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>E-mail</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td>{!! $user->id !!}</td>
                            @if($user->id ==1)
                            <td>{!! $user->first_name !!}<span style="color:green"> ( Admin )</span></td>
                            @else
                            <td>{!! $user->first_name !!}</td>
                            @endif
                            <td>{!! $user->last_name !!}</td>
                            <td>{!!  $user->email !!}</td>
                            <td>
                                <a class="app-operation-left" href="{!!route('users.edit', $user->id)!!}">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                                @if($user->id !=1)
                                {!! Form::open(['method' => 'DELETE', 'class'=>'del-css app-operation-left', 'route' => array('users.destroy', $user->id), 'onsubmit' => 'return ConfirmDelete()']) !!}
                                {!! Form::hidden('id', $user->id) !!}
                                <button type="submit" class="submit-btn-cls"> 
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="pull-left">
    {!! $users->total() !!} user(s) total
</div>

<div class="pull-right">
    {!! $users->render() !!}
</div>
<div class="clearfix"></div>
<script>
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete this User ?. This will not be recoverable press OK to continue.");
        if (x)
            return true;
        else
            return false;
    }
</script>
@stop
@section('footer')
