@extends ('app')
@section('page-header')
<h2 align='center'>Edit User</h2>
@endsection
@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    @if($user_profile->profile_pic !='')
                    {!! Html::image('users_images/'.$user_profile->profile_pic, 'image', array(  'height' => 80 ))  !!}
                    @else
                    {!! Html::image('images/avatar.png', 'image', array( 'width' => 78, 'height' => 86 )) !!}
                    @endif </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> {{ ucfirst($user_profile->first_name)}} {{ ucfirst($user_profile->last_name)}} </div>
                    <div class="profile-usertitle-job"> {{ ucfirst($user_profile->department)}} </div>
                </div>

                <!-- END MENU -->
            </div>

            <!-- END PORTLET MAIN -->
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                            </div>
                            <div class="caption caption-md edit-css">

                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="tab_1_1">
                                    {!! Form::model($user_profile , ['class' => 'form-horizontal', 'role' => 'form', 'action'=>['Admin\UsersController@update' , $user_profile->id] , 'method' => 'PATCH','files' => true,]) !!}
                                    <div class="form-group">
                                        <label class="control-label">First Name <span class="xs-astric">*</span></label>
                                        {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}</div>
                                    <div class="form-group">
                                        <label class="control-label">Last Name <span class="xs-astric">*</span></label>
                                        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}</div>
                                    <div class="form-group">
                                        <label class="control-label">Email <span class="xs-astric">*</span></label>
                                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail Address']) !!}</div>
                                    <!--                                                       <div class="form-group">
                                                                                                    <label class="control-label">Department</label>
                                                                                                    <input type="text" readonly='readonly'  placeholder="" class="form-control" value="Editing"/> </div>-->
                                    <div class="form-group">
                                        <label class="control-label">Password <span class="xs-astric">*</span></label>
                                        <input type="checkbox" name="password" class="change_password" class="form-control"/>
                                        {!! Form::password('password', array('class' => 'form-control change_password_txt', 'disabled' => true)) !!}</div>
                                    
                                    <div class="form-group">
                                        <label class="control-label">Profile Pic</label>
                                        {!! Form::file('profileImage', ['class' => 'field', 'id'=>'target']) !!}
                                    </div>
                                    
                                    <div class="form-group toolbarSetting" >
                                        <label class="control-label"><h2>  Toolbar setting </h2></label><br/>
                                        <label><input class = "toolbarradio" type="radio" name="editor_tool"  <?php if($user_profile->editor_tool == 0){?> checked <?php } ?> value="0" class="form-control"  />Top toolbar (Default)</label>
                                        <label><input class = "toolbarradio" type="radio" name="editor_tool" <?php if($user_profile->editor_tool == 1){?> checked <?php } ?> value="1" class="form-control"/>Circle Toolbar</label>
                                    </div>
                                    
                                    <div class="margiv-top-10">
                                        <button class="btn btn-circle green btn-md" type="submit">Update</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>


<script>
    $(".change_password").change(function() {
        if ($(this).hasClass('checked')) {
            $(this).removeClass('checked').removeAttr('checked');
            $('.change_password_txt').attr('disabled', 'disabled').val('');
        } else {
            $(this).addClass('checked').attr('checked');
            $('.change_password_txt').removeAttr('disabled');
        }
    });
</script>
@stop
