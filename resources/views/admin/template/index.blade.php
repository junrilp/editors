@extends ('app')
@section('content')
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>List Templates</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body flip-scroll">
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content">
                        @if($template && count($template) >= 1)
                        <tr>
                            <th></th>
                            <th>Template Title</th>
                            <th>User Name</th>
                            <th>Price</th>
                            <th>Dimension</th>
                            <th>Created Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($template as $item)
                        <tr>
                            <td>
                                <a href="{!! $item->img_data !!}" target="_blank">
                                    <img src="{!! $item->img_data !!}" style="width: 50px;height:50px;">
                                </a>
                            </td>
                            <td>{!! $item->title !!}</td>
                            <td>{!! $item->users['first_name'] !!} {!!$item->users['last_name']!!}</td>
                            <td>{!! $item->price !!}</td>
                            <td>{!! $item->width !!}*{!! $item->height !!}</td>
                            <td>{!! $item->created_at->format('d-M, Y  (H:i:s)') !!}</td>
                            <td> 
                                <!--<a href="{!!route('template.edit', $item->id)!!}" class="btn btn-outline btn-circle btn-sm purple">Edit Information</a>-->
                                {!! Form::open(['method' => 'DELETE', 'class'=>'del-css', 'route' => array('template.destroy', $item->id),'onsubmit' => 'return ConfirmDelete()']) !!}
                                <button type="submit" class="submit-btn-cls"> 
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                {!! Form::close() !!} 
                            </td>
                        </tr>

                        @endforeach
                        @else
                    <h3 align='center'>Nothing found!</h3>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div>
    <a class="btn btn-success" href="<?php echo url(); ?>/template/create" style="margin-bottom:15px;">Create New</a>
</div>

<div class="pull-left">
    {!! $template->total() !!} template(s) total
</div>

<div class="pull-right">
    {!! $template->render() !!}
</div>
<div class="clearfix"></div>
<script>
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete this Template ?. This will not be recoverable press OK to continue.");
        if (x)
            return true;
        else
            return false;
    }
</script>
@stop