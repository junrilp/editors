<a type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal">Create Role</a>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create a New Role</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    {!! Form::open(['class' => 'form-horizontal','url'=>'roles','files'=>true]) !!}
                    <label class="col-lg-3 control-label">Role Name</label>
                    <div class="col-lg-8">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Role Name']) !!}
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-success" value="Save" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
