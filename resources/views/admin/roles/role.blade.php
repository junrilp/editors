@extends ('app')
@section('content')
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active">
           <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>List Roles </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                       
                                    </div>
                                </div>
                 <div class="portlet-body flip-scroll">
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class="flip-content">
                <tr>
                    <th>ID</th>
                    <th>Role Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roles as $role)
                @if($role->id !='1')
                <tr>
                    <td>{!! $role->id !!}</td>
                    <td>{!! $role->name !!}</td>
                    <td> 
                        <a href="{!!route('roles.edit', $role->id)!!}" class="btn btn-outline btn-circle btn-sm purple">Edit Information</a>
                        {!! Form::open(['method' => 'DELETE','class'=>'del-css', 'route' => array('roles.destroy', $role->id),'onsubmit' => 'return ConfirmDelete()']) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-outline btn-circle dark btn-sm red']) !!}
                        {!! Form::close() !!} 
                   </td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>
      </div>
</div>
          
    
@include('admin/roles/createrole')
<a href="<?php echo url();  ?>/roles" class="btn btn-success">Assigning Role Area</a>


<script>
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete this Role ?. This will not be recoverable press OK to continue.");
        if (x)
            return true;
        else
            return false;
    }

</script>
@stop
@section('footer')
