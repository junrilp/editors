@extends ('app')
@section('page-header')
<h2 align='center'>Edit Role</h2>
@endsection
@section('content')

{!! Form::model($roles , ['class' => 'form-horizontal', 'role' => 'form', 'action'=>['Admin\RolesController@update' , $roles->id] , 'method' => 'PATCH','files' => true]) !!}
<div class="panel panel-default">
    <div class="panel-heading">Update Role</div>
    <div class="panel-body">
        <div class="container col-lg-8 col-lg-push-2">   
            <div class="form-group">
                <label class="col-lg-2 control-label">First Name</label>
                <div class="col-lg-10">
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Role Name']) !!}
                </div>
            </div><!--form control-->
            <div class="pull-right">
                <input type="submit" class="btn btn-success" value="Save" />
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

</div>
{!! Form::close() !!}
@stop
