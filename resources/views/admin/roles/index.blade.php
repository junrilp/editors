@extends ('app')
@section('page-header')
@endsection
@section('content')

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Assign Role To User </div>
        <div class="tools">
            <a href="" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        {!! Form::open(array('method' => 'POST' ,'action'=> ['Admin\RolesController@assign'], 'class'=>'form-horizontal', 'role'=>'form')) !!}
        <div class="form-body">
            <div class="form-group"> 
                <label for="sel1" class="col-lg-2 control-label">Select User</label>
                <div class="col-lg-2" align="center">
                    <select class="form-group" id="userId" name="user_id">
                        <option value="">Select user</option>
                        @foreach($users_list as $user) 
                        @if($user->role_id!='1')
                        @continue;
                        <option value="{{ $user->id }}"> <a href="javascript:void(0);">{{ ucfirst($user->first_name) }} {{ ucfirst($user->last_name) }}</a></option>
                        @endif
                        @endforeach
                    </select>
                </div>
                <label for="sel1" class="col-lg-2">Assign Role</label>
                <div class="col-lg-2">
                    <select class="form-group" id="role_id" name="role_id" >
                        <option value="">Assign Role</option>
                        @foreach($roles as $role) 
                        @if($role->id!='1')
                        @continue;
                        <option value="{{ $role->id }}"> <a href="javascript:void(0);">{{ ucfirst($role->name) }}</a></option>
                        @endif
                        @endforeach
                    </select>
                </div>
                <div class="pull-right col-lg-4">
                    <button class="btn btn-success" type="submit">Change Role</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- Trigger the modal with a button -->


    <script type="text/javascript">
        $(document).ready(function() {
            $('#userId').change(function() {
                var user_id = $(this).val();
                 if($(this).val()!=''){
                $.ajax({
                    url: '<?php echo url();  ?>/roles/' + user_id,
                    type: 'get',
                    //       data: { '_token': $('input[name=_token]').val()},
                    success: function(response) {
                        $('#role_id').val($.trim(response.role_id));
                    }, error: function(e) {
                        alert(e.response);
                    }
                });
                }
            });
        });
    </script>
    @stop
