
<a style="margin-bottom:15px;"  class="btn btn-success" data-toggle="modal" data-target="#myModal">+ Add New</a>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create a New Category</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    {!! Form::open(['class' => 'form-horizontal','url'=>'/category','files'=>true]) !!}
                    <label class="col-lg-12 control-label">Category Name <span class="xs-astric">*</span></label>
                    <div class="col-lg-12">

                        <input required="" type="text" name="name" value="" class="form-control" placeholder="Category Name">
                    </div>
                    <div class="col-lg-8">
                        <input type="hidden" value="" name="" >
                    </div>
                </div>

            </div>
            <div class="modal-footer footer-btn">
                <input type="submit" class="btn btn-success" value="Save" />
                <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>



