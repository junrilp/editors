@extends ('app')
@section('content')
<div class="tab-content" id="List_Categories">
    <div role="tabpanel" class="tab-pane active">
        <div class="portlet box green">

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>List Categories</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body flip-scroll">
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content">
                        @if($category && count($category) >= 1)
                        <tr>
                            <th>ID</th>
                            <th>Category Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($category as $cat)
                        <tr>
                            <td>{!! $cat->id !!}</td>
                            <td>{!! $cat->name !!}</td>
                            <td> 
                                <a class="app-operation-left" href="{!!route('category.edit', $cat->id)!!}">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                                {!! Form::open(['method' => 'DELETE', 'class'=>'del-css', 'route' => array('category.destroy', $cat->id),'onsubmit' => 'return ConfirmDelete()']) !!}
                                <button type="submit" class="submit-btn-cls"> 
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                {!! Form::close() !!} 
                            </td>
                        </tr>
                        @endforeach
                        @else
                    <h3 align='center'>Nothing found!</h3>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('admin/category/create')
</div>
<div class="pull-left">
    {!! $category->total() !!} category(s) total
</div>

<div class="pull-right">
    {!! $category->render() !!}
</div>
<div class="clearfix"></div>
<script>
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to move this item to trash folder ?");
        if (x)
            return true;
        else
            return false;
    }

</script>
@stop