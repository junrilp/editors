@extends ('app')
@section('page-header')
<h2 align='center'>Edit Category</h2>
@endsection
@section('content')

{!! Form::model($category , ['class' => 'form-horizontal', 'role' => 'form', 'action'=>['Admin\CategoryController@update' , $category->id] , 'method' => 'PATCH','files' => true]) !!}
<div class="panel panel-default update-category">
    <div class="panel-heading">Update Category</div>
    <div class="panel-body">
        <div class="container col-lg-12">   
            <div class="form-group ">
                <label class="col-lg-12 control-label">Category Name <span class="xs-astric">*</span></label>
                <div class="col-lg-12">
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Category Name','required'=>'']) !!}
                </div>
            </div><!--form control-->
            <div class="pull-right">
                <input type="submit" class="btn btn-success" value="Save" />
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

</div>
{!! Form::close() !!}
@stop
