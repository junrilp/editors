@extends ('app')
@section('content')
<h2>List Roles</h2>
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active">
        <table class="table table-striped table-hover table-bordered dashboard-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Role Name</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roles as $role)
                @if($role->id !='1')
                <tr>
                    <td>{!! $role->id !!}</td>
                    <td>{!! $role->name !!}</td>
                    <td> 
                        <a href="{!!route('roles.edit', $role->id)!!}" class="btn btn-primary btn-xs">Edit Information</a>
                        {!! Form::open(['method' => 'DELETE', 'route' => array('roles.destroy', $role->id),'onsubmit' => 'return ConfirmDelete()']) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-warning btn-xs']) !!}
                        {!! Form::close() !!} 
                   </td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@include('admin/roles/createrole')
<a href="/roles/" class="btn btn-success">Assigning Role Area</a>


<script>
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete this Role ?");
        if (x)
            return true;
        else
            return false;
    }

</script>
@stop
@section('footer')
