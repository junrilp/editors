@extends ('app')
@section('content')
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active">
         <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Trashed Category</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                      </div>
                                </div>
                                <div class="portlet-body flip-scroll">
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class="flip-content">
                @if($category && count($category) >= 1)
                <tr>
                    <th>ID</th>
                    <th>Category Name</th>
                     <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($category as $cat)
                <tr>
                    <td>{!! $cat->id !!}</td>
                    <td>{!! $cat->name !!}</td>
                    <td> 
                      {!! Html::linkAction('Admin\CategoryController@restoreItems', 'Restore Category', array($cat->id ) ,array('class' => 'btn btn-outline btn-circle btn-sm purple')) !!}
                      
                      {!! Html::linkAction('Admin\CategoryController@forceDelete', 'Delete Permanently', array($cat->id ) ,array('class' => 'btn btn-outline btn-circle dark btn-sm red')) !!}

                   </td>
                </tr>
               
                @endforeach
                @else
                <h3 align='center'>There is no Item in Trash Folder</h3>
                @endif
                
            </tbody>
        </table>
    </div>
    </div>
</div>
    </div>
<div class="pull-left">
    {!! $category->total() !!} category(s) total
</div>

<div class="pull-right">
    {!! $category->render() !!}
</div>
<div class="clearfix"></div>
<script>
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete this Category ?. This will not be recoverable press OK to continue.");
        if (x)
            return true;
        else
            return false;
    }
</script>
@stop