@extends ('app')
@section('content')

{!! Form::open(['class' => 'form-horizontal','url'=>'/pattern','files'=>true]) !!}
<div class="panel panel-default">
    <div class="panel-heading">New Background</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="col-lg-2 control-label"> Select Type <span class="xs-astric">*</span></label>
			<span><i class="fa fa-sort-desc create-ican" aria-hidden="true"></i>
</span>
            <select class="image_type create" name="type" style="margin-left: 15px;">
                <option value="">Type </option>
                <option value="pattern" selected="selected">Pattern 
                </option>
                <option value="image">Image
                </option>
            </select>
        </div><!--form control-->
        <div class="form-group">   
            <label class="col-lg-2 control-label"></label>
            <div class="col-lg-10 background-text "></div>
            <div class="col-lg-10 image-text"></div>
            
        </div>
        <div class="color form-group">
            <label class="col-lg-2 control-label">Background Color</label>
            <div class="col-lg-10">
                {!! Form::text('color', '', ['class' => 'form-control color_picker', 'placeholder' => 'Color']) !!}
            </div>
        </div><!--form control-->
        <div class="form-group">
            <label class="col-lg-2 control-label">Title <span class="xs-astric">*</span></label>
            <div class="col-lg-10">
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
            </div>
        </div><!--form control-->
<!--        <div class="form-group">
            <label class="col-lg-2 control-label">Price</label>
            <div class="col-lg-10">
                {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Price']) !!}
            </div>
        </div>form control-->
        <div class="form-group">
            <label class="col-lg-2 control-label">Select Media <span class="xs-astric">*</span></label>
            <a type="button" class="btn btn-primary gallery" data-toggle="modal" data-target="#myUploads" data-media-id="media_id" data-media-type="radio" data-media-source="pattern" data-media-collect="media_pictures_collect">Gallery</a>
        </div>
        {!! Form::hidden('media_id', null, ['class' => 'form-control', 'placeholder' => 'Media', 'id'=>'media_id']) !!}
        <div class="row media_pictures_collect" id="media_pictures_collect" style="padding: 10px;text-align: right;"></div>
        <div class="pull-right">
            <input type="submit" class="btn btn-success" value="Save" />
        </div>
    </div>
</div>


{!! Form::close() !!}
@stop

