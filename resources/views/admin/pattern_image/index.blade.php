@extends ('app')
@section('content')
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active">
        <!-- <div class ="description">
             <div>Add images for the frontend(customization tool), There are two type of image you can save </div>
             <div>-Image : These type of images will be shown in the image tab of the frontend tool.</div>
             <div>-Background: These Images will be shown in the background tab of the frontend tool.</div>
             
         </div>-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>Backgrounds</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body flip-scroll">
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content">
                        <?php
                       
                        
                        ?>
                        @if($pattern_image && count($pattern_image) >= 1)
                        <tr>
                            <!--<th>ID</th>-->
                            <th>Title</th>
                            <th>Media</th>
                            <th>Background Color</th>
                            <th>Type</th>
<!--                            <th>Price</th>-->
                            <th>Created Date</th>
                            <th>Last Updated</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($pattern_image as $item)

                        <tr>
                             <!--<td>{!! $item->id !!}</td>-->
                            <td>{!! $item->title !!}</td>
                            <td><img src="{!! $item->media->image->url('thumb') !!}"></td>
                            <td>{!! $item->color !!}</td>
                            <td>{!! ucfirst($item->type) !!}</td>
                            <!--<td>{!! $item->price !!}</td>-->
                            <td>{!! $item->created_at->format('d-M, Y  (H:i:s)') !!}</td>
                            <td>{!! $item->updated_at->diffForHumans() !!}</td>
                            <td> 
                                <a class="app-operation-left" href="{!!route('pattern.edit', $item->id)!!}">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>						
                                </a>
                                {!! Form::open(['method' => 'DELETE', 'class'=>'del-css', 'route' => array('pattern.destroy', $item->id),'onsubmit' => 'return ConfirmDelete()']) !!}
                                  <button type="submit" class="submit-btn-cls"> 
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                {!! Form::close() !!} 

                            </td>


                        </tr>

                        @endforeach
                        @else
                    <h3 align='center'>Nothing found!</h3>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--<div>
    <a class="btn btn-success" href="<?php echo url(); ?>/pattern/create" style="margin-bottom:15px;">+ Add New</a>
</div>-->

<button type="button" data-target="#myModal" data-toggle="modal" data-tooltip="tooltip" class="btn btn-success" style="margin-bottom:15px;">+ Add New</button>

<div class="modal fade background-popup" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                <h4 class="modal-title panel-heading" id="myModalLabel">New background</h4>
            </div>
            <div class="modal-body " align="center">
                {!! Form::open(['class' => 'form-horizontal','url'=>'/pattern','files'=>true]) !!}
                <div class="panel panel-default">
                    <div class="panel-body panel-background-header">
                        <div class="form-group select-type">
                            <label class="col-lg-12 control-label" style="padding-left:0px;"> Select Type <span class="xs-astric">*</span></label>
                            <span><i class="fa fa-sort-desc create-ican" aria-hidden="true"></i>
                            </span>
                            <select class="image_type create" name="type" style="margin-left: 15px;">
                                <option value="">Type </option>
                                <option value="pattern" selected="selected">Background 
                                </option>
                                <option value="image">Image
                                </option>
                            </select>
                        </div><!--form control-->
                        <div class="form-group">   
                            <label class="col-lg-12 control-label"></label>
                            <div class="col-lg-12 background-text "></div>
                            <div class="col-lg-12 image-text"></div>
                        </div>
                        <div class="color form-group">
                            <label class="col-lg-12 control-label">Background Color</label>
                            <div class="col-lg-12">
                                {!! Form::text('color', '', ['class' => 'form-control color_picker', 'placeholder' => 'Color']) !!}
                            </div>
                        </div><!--form control-->
                        <div class="form-group">
                            <label class="col-lg-12 control-label">Title <span class="xs-astric">*</span></label>
                            <div class="col-lg-12">
                                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                            </div>
                        </div> 
<div class="form-group pull-left">
                              <div id="mulitplefileuploader2" style="display: none;">Browse ...</div>
                        </div>
						
                        <div class="form-group">
                            <label class="col-lg-12 control-label">Select Media <span class="xs-astric">*</span></label>
                            <div class="col-lg-12 media_pictures_collect" id="media_pictures_collect">                                
                                <!--<form id="upload_content_frm" method="post" action="#">-->
                                <input type="hidden" id="media_source" value="main">
                                <input type="hidden" id="media_limit" value="1">
                                <input type="hidden" id="renderMediaTotal" value="0">
                                <input type="hidden" name="media_id" id="media_id" value="">
                                <div class="row loading-media-container">
                                    <div id="LoadingImage">
                                        <img src="<?php echo url(); ?>/images/ajax-loader.gif" >
                                    </div> 

                                    <fieldset>
                                        <div class="media-inner-box">
                                            <div id="thumbs_store2" class="gallery"></div>
                                        </div>
                                    </fieldset>
                                </div>

                                <div class="load-more" style="text-align:center; padding: 15px;">
                                    <a class="more">Load more</a>
                                </div>

                                <!--div class="row" style="border-top : 1px solid #f1f1f1;text-align: right;padding: 15px;">
                                    <div class="col-lg-12">
                                        <input type="submit" value="Okay" class="btn btn-info">
                                        <button class="btn btn-default" data-dismiss="modal" class="close" type="button">
                                            Cancel
                                        </button>
                                    </div>

                                </div-->
                                <!--</form>-->
                            </div>
                        </div>
                        <!--                        <div class="col-md-3">
                                                    {!! Form::file('media_id', null, ['class' => 'form-control', 'placeholder' => 'Media', 'id'=>'media_id']) !!}
                                                </div>--> 
                        {!! Form::submit('SAVE', array('class' => 'btn btn-success pull-right')) !!}
                        {!! Form::close() !!}

                        {!! Form::open(['class' => 'form-horizontal','url'=>'/media/uploads','files'=>true]) !!}
                        

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pull-left">
    {!! $pattern_image->total() !!} Pattern(s) total
</div>

<div class="pull-right">
    {!! $pattern_image->render() !!}
</div>
<div class="clearfix"></div>
<script>
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete this Pattern or Image ?. This will not be recoverable press OK to continue.");
        if (x)
            return true;
        else
            return false;
    }
</script>
@stop