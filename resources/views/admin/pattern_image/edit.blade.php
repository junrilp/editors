@extends ('app')
@section('page-header')
<h2 align='center'>Edit</h2>
@endsection
@section('content')



{!! Form::model($pattern_image , ['class' => 'form-horizontal', 'role' => 'form', 'action'=>['Admin\PatternImageController@update' , $pattern_image->id] , 'method' => 'PATCH','files' => true]) !!}
<div class="panel panel-default" id="edit-panel-page">
    <div class="panel-heading">Edit Background</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="col-lg-12 control-label">Select Type <span class="xs-astric">*</span></label>
           <div class="col-lg-12">  <select name="type" style="" class="pattern-edit_section">
                <option value="">Select Type</option>
    
                <?php $data = array('pattern' => 'Pattern', 'image' => 'Image') ?>
                @foreach ($data as $k => $val)
                <?php $selected = ( $pattern_image->type == $k ) ? "selected" : "" ?>
                <option value="{{ $k }}" {{$selected}}>{{ $val }}</option>
                @endforeach
            </select></div>
        </div>

        <div class="form-group">
            <label class="col-lg-12 control-label">Color </label>
            <div class="col-lg-10">
                {!! Form::text('color', $pattern_image->color, ['class' => 'form-control color_picker', 'placeholder' => 'Color']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-12 control-label">Title <span class="xs-astric">*</span></label>
            <div class="col-lg-10">
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
            </div>
        </div><!--form control-->
<!--        <div class="form-group">
            <label class="col-lg-2 control-label">Price</label>
            <div class="col-lg-10">
                {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Price']) !!}
            </div>
        </div>form control-->
    </div><!--form control-->
    <div class="form-group media-form-page">
	 <a type="button" data-flag="edit" class="btn btn-primary gallery" data-toggle="modal" data-target="#myUploads" data-media-id="media_id" data-media-type="radio" data-media-source="pattern" data-media-collect="media_pictures_collect">Gallery</a>
      </div>
	  <div class="form-group media-form-page">
	  <label class="col-lg-12 control-label">Uploaded Media <span class="xs-astric">*</span></label>
       <img height="50" width="40" src="<?php echo url().$pattern_image->media->image->url('thumb'); ?>">
    </div>

    {!! Form::hidden('media_id', null, ['class' => 'form-control', 'placeholder' => 'Media', 'id'=>'media_id']) !!}
    <div class="row media_pictures_collect" id="media_pictures_collect" style="padding: 10px;text-align: right;"></div>

    <div class="pull-right">
        <p>
            <input type="submit" class="btn btn-success" value="Save" />
        </p>
    </div>
</div>
</div>
{!! Form::close() !!}
@stop
