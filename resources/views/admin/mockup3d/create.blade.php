@extends ('app')
@section('content')

{!! Form::open(['class' => 'form-horizontal','url'=>'/mockup3d','files'=>true]) !!}
<div class="panel panel-default custom-Canvas-size" id="create_canvas_size">
    <div class="panel-heading">Add Canvas Size</div>
    <div class="panel-body">
<!--        <div class="form-group">
        <label class="col-lg-2 control-label">Select Category</label>
        <select name="category_id" class="custom-Canvas-size-select">
		<option value="">Select Category</option>
            @foreach ($category as $cat)
            @if($cat->status == 1 || $cat->status == 0)
            <option value="{{$cat->id}}">{!! $cat->name !!}
                </option>
                @endif
                    @endforeach
         </select>
        </div>-->
        <div class="form-group">
            <label class="col-lg-12 control-label">Title</label>
            <div class="col-lg-12">
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
            </div>
        </div><!--form control-->
<!--        <div class="form-group">
            <label class="col-lg-2 control-label">Price</label>
            <div class="col-lg-10">
                {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Price']) !!}
            </div>
        </div>form control-->
        <div class="form-group">
            <label class="col-lg-12 control-label">Width<span class="xs-astric">*</span></label>
            <div class="col-lg-12">
                {!! Form::text('width', null, ['class' => 'form-control', 'placeholder' => 'Width']) !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            <label class="col-lg-12 control-label">Height<span class="xs-astric">*</span></label>
            <div class="col-lg-12">
               {!! Form::text('height', null, ['class' => 'form-control', 'placeholder' => 'height']) !!}
            </div>
        </div><!--form control-->
<!--        <div class="form-group">
            <label class="col-lg-2 control-label">Select Media</label>
            <a type="button" class="btn btn-primary gallery" data-toggle="modal" data-target="#myUploads" data-media-id="media_id" data-media-type="radio" data-media-collect="media_pictures_collect">Gallery</a>
        </div>-->
        {!! Form::hidden('media-id', null, ['class' => 'form-control', 'placeholder' => 'height', 'id'=>'media_id']) !!}
        <!--<div class="row media_pictures_collect" id="media_pictures_collect" style="padding: 10px;text-align: right;"></div>-->
        <div class="pull-left">
            <input type="submit" class="btn btn-success" value="Save" />
        </div>
    </div>
</div>

{!! Form::close() !!}
@stop
