@extends ('app')
@section('page-header')
<h2 align='center'>Edit Category</h2>
@endsection
@section('content')


{!! Form::model($mockup3d , ['class' => 'form-horizontal', 'role' => 'form', 'action'=>['Admin\Mockup3dController@update' , $mockup3d->id] , 'method' => 'PATCH','files' => true]) !!}
<div class="panel panel-default">
    <div class="panel-heading">Edit Canvas Size</div>
    <div class="panel-body" id="edit-canvas">
<!--        <div class="form-group">
            <label class="col-lg-2 control-label">Select Category</label>
            <select name="category_id">
                <option value="">Select Category</option>
                @foreach ($category as $cat)
                <option value=""</option>
                    
                        @endforeach
                </select>
            </div>-->
        <div class="form-group">
            <label class="col-lg-12 control-label">Title <span class="xs-astric">*</span></label>
            <div class="col-lg-12">
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
            </div>
        </div><!--form control-->
        <div class="form-group">
            <label class="col-lg-12 control-label">Price <span class="xs-astric">*</span></label>
            <div class="col-lg-12">
                {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Price']) !!}
            </div>
        </div><!--form control-->
        <div class="form-group">
            <label class="col-lg-12 control-label">Width <span class="xs-astric">*</span></label>
            <div class="col-lg-12">
                {!! Form::text('width', null, ['class' => 'form-control', 'placeholder' => 'Width']) !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            <label class="col-lg-12 control-label">Height <span class="xs-astric">*</span></label>
            <div class="col-lg-12">
               {!! Form::text('height', null, ['class' => 'form-control', 'placeholder' => 'height']) !!}
            </div>
        </div><!--form control-->
		<a type="button" class="btn btn-primary gallery" data-toggle="modal" data-target="#myUploads" data-media-id="media_id" data-media-type="radio"  data-media-source="mockup3d" data-media-collect="media_pictures_collect">Gallery</a>
        <div class="form-group">
            <label class="col-lg-3 control-label">Uploaded Media</label>
            <img height="50" width="40" src="<?php echo url(); ?>/uploads/thumbnails/<?php echo $mockup3d->media['file_name'] ; ?>">
        </div>
        
        {!! Form::hidden('media_id', null, ['class' => 'form-control', 'placeholder' => 'Media', 'id'=>'media_id']) !!}
        <div class="row media_pictures_collect" id="media_pictures_collect" style="padding: 10px;text-align: right;"></div>
        
        <div class="pull-right">
            <input type="submit" class="btn btn-success" value="Save" />
        </div>
    </div>
</div>
{!! Form::close() !!}
@stop
