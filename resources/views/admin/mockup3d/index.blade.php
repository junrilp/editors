@extends ('app')
@section('content')
<div class="tab-content" id="Canvas_Sizes">
    <div role="tabpanel" class="tab-pane active">
        <!-- <div class ="description">
            Create different size of banners for the frontend, once you are set they will be available in the frontend , later end user can choose desired size and customize them accordingly.
             
         </div>-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>List Canvas Sizes</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body flip-scroll">
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content">
                        @if($mockup3d && count($mockup3d) >= 1)
                        <tr>
        <!--                     <th>Type</th>-->
                            <!--<th>Category Name</th>-->
                            <th>Title</th>
                            <!--<th>Price</th>-->
                            <th>Width</th>
                            <th>Height</th>
       <!--                     <th>Media Image</th>-->
                            <th>Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($mockup3d as $data)
                        <tr>
                            <!--<td><?php //  echo ($data->is_3d=='yes')?"Mockup":"Size";       ?></td>-->
                            <!--<td><?php //echo ($data->category['name']) ? $data->category['name'] : "N/A"; ?></td>-->
                            <td><?php echo ($data->title) ? $data->title : "N/A"; ?></td>
                            <!--<td><?php //  echo ($data->price==0)?"Free":$data->price;       ?></td>-->
                            <td>{!! $data->width !!}</td>
                            <td>{!! $data->height !!}</td>
                            <!--<td><?php // if($data->media['file_name']){       ?><a target="_blank" href="<?php // echo url();       ?>/uploads/prolix/<?php // echo $data->media['file_name'] ;       ?>"><img  width="40" src="<?php // echo url();       ?>/uploads/thumbnails/<?php // echo $data->media['file_name'] ;       ?>" class="xs-mockup-img"></a><?php // }      ?> </td>-->
        <!--                    <td>{!! $data->media['file_name'] !!}</td>-->
                            <td> 
                                <a class="app-operation-left" href="{!!route('mockup3d.edit', $data->id)!!}">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                                {!! Form::open(['method' => 'DELETE', 'class'=>'del-css', 'route' => array('mockup3d.destroy', $data->id),'onsubmit' => 'return ConfirmDelete()']) !!}
                                <!--                        <a href="#" onclick="ConfirmDelete()">
                                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                        </a>-->
                                
                                <!--<a class="delete-form-button" type="submit"><i class="fa fa-trash-o" aria-hidden="true"></i></a>-->
                                <button type="submit" class="submit-btn-cls">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                {!! Form::close() !!} 
                            </td>

                        </tr>

                        @endforeach
                        @else
                    <h3 align='center'>Nothing found!</h3>
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<div><a style="margin-bottom:15px;" href="<?php echo url(); ?>/mockup3d/create" class="btn btn-success">+ Add New</a></div>

<div class="clearfix"></div>
<div class="pull-left">
    {!! $mockup3d->total() !!} Mock up(s) total
</div>

<div class="pull-right">
    {!! $mockup3d->render() !!}
</div>

<script>
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete this 3D Mock up ?. This will not be recoverable press OK to continue.");
        if (x)
            return true;
        else
            return false;
    }
</script>
@stop