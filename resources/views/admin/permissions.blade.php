@extends ('app')
@section('page-header')
<h2 align='center'>Permissions Area</h2>
@endsection
@section('content')

    {!! Form::open(array('method' => 'POST' ,'action'=> ['Admin\AclController@add_permission'], 'class'=>'form-horizontal abc')) !!}
    <div class="panel panel-default">
     <div class="panel-heading">Assign Permission to Different Roles</div>
        <div class="panel-body">
            <div class="container">
           <div class="row">
            <div class="col-md-12 col-lg-12">
                <select name="role_id" id="role_id">
                    <option value="">Select Role</option>
                    @foreach($roles as $role)
                    @if($role->id!='1')
                    @continue;
                    <?php $selected = ( $type == $role->id ) ? "selected" : "" ?>
                    <option value="{{ $role->id}}" {{$selected}}>{{$role->name}}</option>
                    @endif
                    @endforeach
                </select>

            </div>
               </div>
            @foreach($resourceData as $controller => $resource)
            <div class="col-md-6 col-lg-6 action-css">
                <div>
                    <label>Controller Name:</label>
                    <span class="label label-danger">{{ucfirst($controller)}}</span>
                </div>
                @foreach( $resource as $id => $action )
                <?php $defaultVal = 0;
                $checked = "";
                ?>
                @if( $permissions && (isset($permissions[$id]) && $permissions[$id] ) )
                <?php $defaultVal = 1;
                $checked = "checked='checked'";
                ?>
                @endif
                <div>
                    <span>Action Allowed:</span>
                    <input name="resource_id[{{$id}}]" type="hidden" class="resource_chkbox" value='{{$defaultVal}}'>
                    <input name="resource_id[{{$id}}]" type="checkbox" class="resource_chkbox" data-toggle="toggle" data-on="Enabled" data-off="Disabled" data-size='mini' {{$checked}}>
                    <span class="label label-success">{{ucfirst($action)}}</span>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
        <div class="pull-right">
            <input type="submit" class="btn btn-success" value="Save Permission" />
        </div>
    </div>
</div>
{!! Form::close() !!}
<script>
    $('form').submit(function() {
        if (!$("#role_id").val()) {
            alert("Please select User Role");
            return false;
        }
    });
    
    $("#role_id").change(function() {
        if( $("#role_id").val()!=''){
        window.location.href = "<?php echo url();  ?>/permissions/" + $(this).val();
        }
    });
    
</script>
@stop


