<!-- BEGIN SIDEBAR -->
<?php
//$path = explode('public/', $_SERVER['REQUEST_URI']);
//$controllerName = explode('/', $path[1]);
//$cname = $controllerName[0];
 $cname = request()->segment(count(request()->segments()));
?>
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu nav" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
           <!-- <li>
                <a href="{{ URL::previous() }}">
                    <i class="fa fa-backward"></i>
                    <span class="title">Go Back</span>
                    <span class="selected"></span>
                </a>
            </li>-->
			 <li class="custom-button">
                <a target="_blank" href="<?php echo url(); ?>/design">
                    <i class="fa fa-paint-brush"></i>
                    <span class="title">Start Customization</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>

            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <!--            <li class="sidebar-search-wrapper hidden-xs">
                             BEGIN RESPONSIVE QUICK SEARCH FORM 
                             DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box 
                             DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box 
                            <form class="sidebar-search" action="extra_search.html" method="POST">
                                <a href="javascript:;" class="remove">
                                    <i class="icon-close"></i>
                                </a>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                                    </span>
                                </div>
                            </form>
                             END RESPONSIVE QUICK SEARCH FORM 
                        </li>-->

           <li class="start <?php echo ($cname=='home') ? "active": ""; ?> ">
              <a href="<?php echo url(); ?>/">
              <i class="icon-home"></i>
              <span class="title">Home</span>
              <span class="selected"></span>
              </a>
              </li>

            @if(!Auth::guest() && Auth::user()->role_id=='1')

            <li class="start <?php echo ($cname == 'users') ? "active" : ""; ?>">
                <a href="<?php echo url(); ?>/users">
                    <i class="glyphicon glyphicon-user"></i>
                    <span class="title">Users</span>
                    <span class="selected"></span>
                </a>
            </li>

            

            <li class="<?php echo ($cname == 'role') ? "active" : ""; ?>">
              <a href="<?php echo url(); ?>/role">
              <i class="glyphicon glyphicon-cog"></i>
              <span class="title">Role Area</span>
              <span class="selected"></span>
              </a>
              </li>
            
            <li class="<?php echo ($cname == 'permissions') ? "active" : ""; ?>">
              <a href="<?php echo url(); ?>/permissions">
              <i class="glyphicon glyphicon-lock"></i>
              <span class="title">Permissions Area</span>
              <span class="selected"></span>
              </a>
            </li> 
          @endif
            <li class="<?php echo ($cname == 'category') ? "active" : ""; ?>">
                <a href="<?php echo url(); ?>/category">
                    <i class="glyphicon glyphicon-tasks"></i>
                    <span class="title">My Categories</span>
                    <span class="selected"></span>
                </a>
            </li>
          @if(!Auth::guest() && Auth::user()->role_id=='1')

            <li class="<?php echo ($cname == 'acl') ? "active" : ""; ?>">
              <a href="<?php echo url(); ?>/acl">
              <i class="glyphicon glyphicon-dashboard"></i>
              <span class="title">Update New Resources</span>
              <span class="selected"></span>
              </a>
              </li>
          @endif
            <li class="<?php echo ($cname == 'mockup3d') ? "active" : ""; ?>">
                <a href="<?php echo url(); ?>/mockup3d">
                    <i class="glyphicon glyphicon-th-list"></i>
                    <span class="title">Custom Canvas Sizes</span>
                    <span class="selected"></span>
                </a>
            </li>

            <li class="">
                <a href="" type="button" class="gallery" data-toggle="modal" data-target="#myUploads" data-flag="media" data-media-id="media_id" data-media-type="radio"  data-media-source="main" data-media-collect="media_pictures_collect">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span class="title">Media Library</span>
                </a>
            </li>

            <li class="<?php echo ($cname == 'pattern') ? "active" : ""; ?>">
                <a href="<?php echo url(); ?>/pattern">
                    <i class="glyphicon glyphicon-list-alt"></i>
                    <span class="title">Backgrounds</span>
                </a>
            </li>
          @if(!Auth::guest() && Auth::user()->role_id=='1')

           <li class="<?php echo ($cname == 'template') ? "active" : ""; ?>">
              <a href="<?php echo url(); ?>/template">
              <i class="glyphicon glyphicon-list-alt"></i>
              <span class="title">Create Template</span>
              </a>
              </li>

            @endif

            <li class="<?php echo ($cname == 'canvas') ? "active" : ""; ?>">
                <a href="<?php echo url(); ?>/canvas">
                    <i class="glyphicon glyphicon-folder-open"></i>
                    <span class="title">My Designs</span>
                    <span class="selected"></span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>


<!-- END SIDEBAR -->

<!-- Modal -->
<div id="myUploads" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="text-transform: uppercase;font-weight: bold;">Upload to Media Library</h3>
            </div>
            <div class="modal-body">
                {!! Form::open(['class' => 'form-horizontal','url'=>'/media/uploads','files'=>true]) !!}
                <div class="form-group">
                    <div id="mulitplefileuploader" style="display: none;">Browse ...</div>
                </div> 
                {!! Form::close() !!} 
                <form id="upload_content_frm" method="post" action="#">
                    <input type="hidden" id="media_source" value="main">
                    <input type="hidden" id="media_limit" value="1">
                    <input type="hidden" id="renderMediaTotal" value="0">
                    <div class="row loading-media-container">
                        <div id="LoadingImage">
                            <img src="<?php echo url(); ?>/images/ajax-loader.gif" >
                        </div>
                    
                        <fieldset>
                            <legend style="padding: 8px;">Select From Gallery</legend>
                            <div class="media-inner-box">
                                <div id="thumbs_store" class="gallery"></div>
                            </div>
                        </fieldset>
                        <div class="load-more" style="text-align:center;padding: 15px;display: none;">
                            <a class="more">Load more</a>
                        </div> 

                    </div>
                    <div class="row" style="text-align: right;padding: 15px;">
                        <div class="col-lg-12">
                            <input type="submit" value="Okay" class="btn btn-info media-okay">
                            <button class="btn btn-default" data-dismiss="modal" class="close" type="button">
                                Cancel
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


