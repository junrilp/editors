<div class="copyright"> <?php echo date('Y'); ?> @BannersPro. Admin Dashboard Template. </div>

<script src="<?php echo url(); ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo url(); ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo url(); ?>/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo url(); ?>/assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="<?php echo url(); ?>/assets/pages/scripts/login.min.js" type="text/javascript"></script>
</body>
</html>