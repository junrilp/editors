<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="_token" content="{!! csrf_token() !!}" />
        <title>Online Editors</title>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>

        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="{{ asset('plugins/jquery.gritter.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css"/>


        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        <link href="{{ asset('plugins/tasks.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />


        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="{{ asset('assets/global/css/components.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/layouts/layout/css/layout.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/layouts/layout/css/themes/default.css')}}" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="{{ asset('assets/layouts/layout/css/custom.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('fileupload/uploadfile.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('css/jquery.minicolors.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('css/admincss.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('plugins/jcrop.css')}}" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->

        <link href="{{ asset('css/bootstrap-toggle.min.css')}}" rel="stylesheet">
        <link href="{{ asset('css/uimcss.css')}}" rel="stylesheet">

        <link rel="shortcut icon" href="favicon.ico"/>
        <script src="{{ asset('assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
       

        <script> $baseUrl = '<?php echo url(); ?>';</script>
    </head>

    <body ng-app="editorApp">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<?php echo url(); ?>/users">
                        <img src="{{ asset('assets/layouts/layout/img/banner-logo.jpg')}}" alt="logo" class="logo-default"/>
                    </a>
                    <div class="menu-toggler sidebar-toggler hide"></div>
                </div>
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>
                
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown dropdown-user">
                            <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                @if(Auth::user()->profile_pic !='')
                                {!! Html::image('users_images/'.Auth::user()->profile_pic, 'image', array(  'height' => 80 ))  !!}
                                @else
                                {!! Html::image('images/avatar.png', 'image') !!}
                                @endif
                                <span class="username">
                                    {{  ucfirst(Auth::user()->first_name) }} </span>
                                <i class="fa fa-angle-down"></i>
                            </a>-->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <span class="username">{{  ucfirst(Auth::user()->first_name)}} </span>
                                <span class="username-icon"><i class="fa fa-user"></i></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo url() . '/users/' . (Auth::user()->id) . '/edit'; ?>"><i class="icon-user"></i> View Profile</a>
                                </li>
                                <li>
                                    <a href="<?php echo url() . '/auth/logout'; ?>"><i class="icon-lock"></i> Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
        <!-- END HEADER -->
