
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="">Online Editor</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>{!! link_to('/', 'Home') !!}</li>
                <li>{!! link_to('/editor', 'Online Editor') !!}</li>
                @if(!Auth::guest() && Auth::user()->role_id!='1')
                @endif
                @if(!Auth::guest() && Auth::user()->role_id=='1')

                <li>{!! link_to('users', 'Dashboard') !!}</li>
                <li>{!! link_to('/role', 'Role Area') !!}</li>
                <li>{!! link_to('permissions', 'Permission Area') !!}</li>
                <li>{!! link_to('/category', 'Category Area') !!}</li>
                <!--<li>{!! link_to('roles', 'Assign Role') !!}</li>-->
                <li>{!! link_to('acl', 'Update New Resources') !!}</li>
              

                @endif
            </ul>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())

                    <li>{!! link_to('auth/login', 'Login') !!}</li>
                    <li>{!! link_to('auth/register', 'Register') !!}</li>

                    @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hello, {{  ucfirst(Auth::user()->first_name) }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>{!! link_to_route('user.show', "View Profile", Auth::user()->id) !!}</li>
                             @if(!Auth::guest() && Auth::user()->role_id!='2')
                            @endif
                            <li>{!! link_to('auth/logout', 'Logout') !!}</li>
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
        </div>
</nav>