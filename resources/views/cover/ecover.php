<section>
    <?php $i=0; foreach ($detailsData as $details => $coverData) { ?>
        <ul class="accordion" data-accordion="myAccordionGroup">
            <li class="accordion-navigation">
                <a href="#<?php echo strtolower($details); ?>"><?php echo $details; ?></a>
                <div id="<?php echo strtolower($details); ?>" class="content <?php echo ($i==0) ? 'active':''; ?>">
                    <section class="gs-example">
                        <?php foreach ($coverData as $data) { ?>
                            <div class="medium-2 large-2 columns border-none xs-grid-items">
                                <?php if($data['price']<=0) { ?>
                                    <img class="xs-price-tag" src="<?php echo url() . '/images/free.png'; ?>"/>
                                <?php } ?>
                                <div class="xs-title-bar"><?php echo $data['title']; ?></div>
                                <img src="<?php echo url(); ?>/<?php echo Config::get('constants.upload_path_thumb'); ?>/<?php echo $data['file_location'] . '/' . $data['file_name'] ?>" class="xs-mockup-img">
                                <div class="itemOptions">
                                    <div id="comment"></div>
                                    <ul class="items">
                                        <li title="Create this cover" ng-click="designStepTwo('<?php echo $data['width']; ?>','<?php echo $data['height']; ?>','<?php echo str_replace("'", '', $data['title'] )?>');" class="itemStart"></li>
                                        <li link-src="<?php echo url() . '/' . Config::get('constants.upload_path_prolix') . '/' . $data['file_name'] ?>" title="Preview final render" class="itemPreview"></li>
                                        <li tg-width="<?php echo $data['width']; ?>" tg-height="<?php echo $data['height']; ?>" title="Size info" class="itemInfo"></li>
                                        <li link-src="<?php echo url() . '/' . Config::get('constants.upload_path_prolix') . '/' . $data['file_name'] ?>" title="Download Offline Guide" class="itemGuide"></li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    </section>
                </div>
            </li>
        </ul>
    <?php $i++; } ?>
</section>

