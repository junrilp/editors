@extends ('app')
@section('content')

 <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            <div class="profile-sidebar">
                                <!-- PORTLET MAIN -->
                                <div class="portlet light profile-sidebar-portlet ">
                                    <!-- SIDEBAR USERPIC -->
                                    <div class="profile-userpic">
                                        @if($user_profile->profile_pic !='')
        {!! Html::image('users_images/'.$user_profile->profile_pic, 'image', array(  'height' => 80 ))  !!}
        @else
        {!! Html::image('images/avatar.png', 'image', array( 'width' => 78, 'height' => 86 )) !!}
        @endif </div>
                                    <!-- END SIDEBAR USERPIC -->
                                    <!-- SIDEBAR USER TITLE -->
                                    <div class="profile-usertitle">
                                        <div class="profile-usertitle-name"> {{ ucfirst($user_profile->first_name)}} {{ ucfirst($user_profile->last_name)}} </div>
                                        <div class="profile-usertitle-job"> {{ ucfirst($user_profile->department)}} </div>
                                    </div>
                                    <!-- END SIDEBAR USER TITLE -->
                                    <!-- SIDEBAR BUTTONS -->
                                    <div class="profile-userbuttons">
                                        <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                                        <button type="button" class="btn btn-circle red btn-sm">Message</button>
                                    </div>
                                    <!-- END SIDEBAR BUTTONS -->
                                    <!-- SIDEBAR MENU -->
                                   
                                    <!-- END MENU -->
                                </div>
                                <!-- END PORTLET MAIN -->
                                <!-- PORTLET MAIN -->
                                <div class="portlet light ">
                                    <!-- STAT -->
                                
                                    <!-- END STAT -->
                                    <div>
                                        <h4 class="profile-desc-title">About {{ ucfirst($user_profile->first_name)}} {{ ucfirst($user_profile->last_name)}}</h4>
                                        <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span>
                                        <div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-globe"></i>
                                            <a href="http://www.keenthemes.com">www.keenthemes.com</a>
                                        </div>
                                        <div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-twitter"></i>
                                            <a href="http://www.twitter.com/keenthemes/">@keenthemes</a>
                                        </div>
                                        <div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-facebook"></i>
                                            <a href="http://www.facebook.com/keenthemes/">keenthemes</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PORTLET MAIN -->
                            </div>
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light ">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption caption-md">
                                                    <i class="icon-globe theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                </div>
                                                 <div class="caption caption-md edit-css">
                                                    <i class="icon-globe theme-font hide"></i>
                                                    @if(Auth::user()->id==$user_profile->id || Auth::user()->role_id=='1' )
                                   {!! link_to_route('users.edit', "Edit Your Profile", $user_profile->id,['class'=>'btn btn-primary btn-xs']) !!}                               
                                    @endif  
                                                </div>
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="tab-content">
                                                    <!-- PERSONAL INFO TAB -->
                                                    <div class="tab-pane active" id="tab_1_1">
                                                        <form role="form" action="#">
                                                            <div class="form-group">
                                                                <label class="control-label">First Name</label>
                                                                <input type="text" readonly='readonly' placeholder="" class="form-control" value='{{ ucfirst($user_profile->first_name)}}'/> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Last Name</label>
                                                                <input type="text" readonly='readonly'  placeholder="" class="form-control" value='{{ ucfirst($user_profile->last_name)}}' /> </div>
                                                                 <div class="form-group">
                                                                <label class="control-label">Email</label>
                                                                <input type="text" readonly='readonly'  placeholder="" class="form-control" value='{{ ucfirst($user_profile->email)}}' /> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Department</label>
                                                                <input type="text" readonly='readonly'  placeholder="" class="form-control" value="PHP"/> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Created at</label>
                                                                <input type="text" readonly='readonly'  placeholder="Design, Web etc." class="form-control" value='{!! $user_profile->created_at !!} ({!! $user_profile->created_at->diffForHumans() !!})' /> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Updated at</label>
                                                                <input type="text" readonly='readonly'  placeholder="Web Developer" class="form-control" value='{!! $user_profile->updated_at !!} ({!! $user_profile->updated_at->diffForHumans() !!})' /> </div>
                                                            
                                                            <div class="margiv-top-10">
                                                             
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
               
@endsection
@stop
@section('footer')
